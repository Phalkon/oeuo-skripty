dofile('../functions/journal.lua')
dofile('../functions/communicator.lua')

local rezerva = 50 -- kolik HP je mozno ztratit
local nalozeni = UO.Weight

local nYear,nMonth,nDay,nDayOfWeek = getdate ()
local nHour, nMinute, nSecond, nMillisec = gettime()
if nMinute<10 then nMinute = (0)..nMinute end
local log_start_msg = (nYear..('/')..nMonth..('/')..nDay..(' ')..nHour..(':')..nMinute..(' ')..'STARTing log file')
if create then
create:write(log_start_msg..'\n')
create:close() end
function log(log_this_msg) cm_antiafk(log_this_msg) print(log_this_msg) end
--function log(log_this_msg) local file = openfile(communicator,"a") if file then file:write('ANTIAFK!'..'\n') file:close() print(log_this_msg) end end


ignore = {
--temp
--magery
'|67|Awa: ',
'|946|Awa: ',
'|946|Awa: Lapis Arcanum|',
'|946|Awa: Fomentum|',
'|237|Kouzlo selhalo|',
'|88|Your skill in Magie has increased by ',
'|93|Specializace oboru ',
--nekromancie
'|946|Nedok�zala ses soust�edit|',
' pYivol�vat pomocn�ka|',
'|1905|kostlivec ',
'|946|You see: kostlivec|',
'|52|Awa: kostlivec release|',
'|52|Awa: all release|',
'|946|Uklid�uje� svou mysl a vstupuje� do tranzu.|',
'|946|C�t� klid a m�r.|',
'|88|Your skill in Meditace has increased by ',
'|88|Your skill in Nekromancie has increased by ',
--alchemka
'|68|naplnila jsi misicku lektvaru z nadoby|',
'|946|Vyber prvni lektvar nebo nadobu s nim|',
'|946|Vyber druhy lektvar nebo nadobu s nim|',
'|946|Awa: naplnila jsi misicku lektvaru z nadoby|',
'|34|Awa: *za��n� v hmo�d��i drtit trochu ',
'|54|Za�ala jsi vyr�b�t ',
'za��n� v hmo�d��i drtit trochu ',
'sype podivnou sm�s na zem',
'|88|Your skill in Alchymie has increased by ',
--pitvani
'|946|Toto je t�lo T�lo sk�et�ho velitele a bylo vyvr�eno.Nedok�e� ur�it kdo tuto bytost zabil|',
'|54|Na jak� t',
'|946|Je to T�lo od',
'|88|Your skill in Pitvani has increased by ',
--spirit speak
'|946|Spojen� s on�m sv�tem je ji� nav�z�no.|',
'|946|Ztratil jsi spojen� s on�m sv�tem.|',
'|946|Pokou�� se nav�zat spojen� s on�m sv�tem.|',
'|946|Uskute��uje� spojen� s on�m sv�tem.|',
'|88|Your skill in Rec mrtvych has increased by ',
--panaci
'|946|You see: cvicna figurina|',
'|88|Your skill in Boj drtivymi zbranemi has decreased by ',
'|88|Your skill in Boj drtivymi zbranemi has increased by ',
'|88|Your skill in Boj bodnymi zbranemi has increased by',
'|88|Your skill in Taktika has increased by',
'|88|Your skill in Kradeni has increased by',
'|946|Tento p�edm�t se pr�v� pou��v�|',
'|946|You see: figurina|',
'|946|S takov�m trikem jsi',
-- makroskilly
'|946|Magotar je nadlidsky inteligentn�.|',
'|946|Paracelsus je nadlidsky inteligentn�.|',
'|946|Niklaus Manuel je velmi inteligentn�.|',
'|946|Duch tva�� se hloup�.|',
'|946|nem� o magii ani zd�n� a m� many na rozd�v�n�.|',
'|946|Awa se tv��� v�jime�n� inteligentn�.|',
'|946|m� povrchn� znalost magie a m� many na rozd�v�n�.|',
'|54|Vyber si, koho chce� odhadnout|',
'|88|Your skill in Odhad inteligence has increased by ',
'|38|You see: bezhlavy|',
'|54|Kter�ho tvora m',
'|946|Awa: O tomto stvo�en� nedok�e� nic ��ci.|',
'|5555|Pauza ',
'|1925|Awa: Awa je pr�m�rn� siln� a vypad� velmi obratn�.|',
'|88|Your skill in Anatomie has increased by ',
'|946|Unexpected target info|',
-- galeje
'|5555|Jdu si pro prut typu',
'|5555|Prut typu ',
'|1904|Vyber c�l: ',
'|1268|V�born, toto je spr�vn� prut.|',
'|5555|Vracim se do skladu|',
'|946|You see: uzovka|',
-- hid test
--'5555|',
--'|946|You see: Bryan|',
-- lumberjack
'|946|Lilien: Chvilku sek� do stromu, ale ��dn� pou�iteln� d�evo nez�sk�v�|',
'|946|Nezbylo tu nic k pok�cen�.|',
'|946|You see: lan|',
'|946|You see: krava|',
-- lahvicky
'|1904|Vyber c�l: Kde chce',
'|1268|Nasypala jsi p�sek do pytle|',
'|1356|Nem�a u sebe ~�dn� vhodn� pytel, nebo ho m�a zahraban� moc hluboko v batohu na to, abys do nj nasypala p�sek.|',
'|54|Za�ala jsi vyr�b�t prazdna lahev|',
'|1925|S t�m, co m� u sebe, nedovede� prazdna lahev vyrobit.|',
'|1925|Na v�robu je t�eba 1 ks od pisek.|',
'|1925|Sch�z� ti 1 ks od pisek.|',
-- orli
'Vid�, jak kralik prch�',
'Vid�, jak puma prch�',
'|946|Strk� obri ropucha pry� z cesty.|',
'Vid�, jak zajic prch�',
'Vid�, jak gorila zu�iv� za�to�ila',
'gorila na tebe uto��',
'|946|You see: nakladni kun|',
'|946|You see: taipan|',
'|946|You see: obri had|',
'|946|You see: papousek|',
'Vid�, jak obri ropucha prch�',
'|946|You see: krokodyl|',
'|946|You see: krokodyl|',
'|946|You see: horsky lev|',
'|946|You see: panter|',
'|946|You see: vrrk|',
'|946|You see: obri ropucha|',
'|946|You see: zajic|',
'|946|You see: Herka klisna|',
'|946|Strk� Herka klisna pry� z cesty.|',
'|946|Strk� Nenosi ponozky pry� z cesty.|',
'Vid� jak Nenosi ponozky uto�� na gorila',
'Vid� jak gorila uto�� na Nenosi ponozky',
'Vid�, jak gorila prch�',
'Vid� jak medved grizly uto�� na gorila',
'Vid� jak gorila uto�� na medved grizly',
'|946|Strk� medved grizly pry� z cesty.|',
'Vid� jak orel uto�� na medved grizly',
'Vid� jak havran uto�� na medved grizly',
'Vid� jak medved grizly uto�� na orel',
'Vid� jak medved grizly uto�� na havran',
'Vid�, jak pralesni papousek prch�',
'Vid�, jak uzovka prch�',
'|0|You can not pick that up.|',
"|0|Can't get there|",
'*orel na tebe ',
'Vid�, jak lan prch�',
'Vid�, jak orel prch�',
--'Vid�, jak orel zu�iv� za�to�il',
'|946|Strk� orel pry� z cesty.|',
'|946|Strk� jelen pry� z cesty.|',
'|946|Strk� gorila pry� z cesty.|',
'|946|Z�sk�v� kousek fame.|',
'|946|Kryje� �der �t�tem|',
', jak jelen prch',
'|38|You see: kostlivec|',
'|38|You see: gh�l|',
'|38|You see: spektra|',
'|38|You see: zombie|',
'|38|You see: zjeven�|',
'|38|You see: lich vyvol�va�|',
'|946|You see: puma|',
'|38|You see: kostlivec lu�i�tn�k|',
'|38|You see: Temn� kostlivec ryt��|',
'|38|You see: mumie|',
'|38|You see: kostlivec se sekerou|',
'|38|You see: Temn� Kostlivec se sekerou|',
'se mohla po�kodit|',
'|946|Strk� lan pry� z cesty.|',
'|946|Nar�� na skryt�ho(ou) pralesni papousek.|',
'|946|Strk� pralesni papousek pry� z cesty.|',
'|946|Strk� uzovka pry� z cesty.|',
'|88|Your skill in Kuchani has increased',
-- jedy
"|1925|P�eru�ila jsi nan�en� jedu.|",
"|54|Jaky jed chces pouzit?|",
"|946|Unexpected target info|",
"|946|Na co chce",
"|68|�sp�n� jsi pou�ila jed na",
"|0| |",
"|1925|Neusp�la jsi p�i pokusu otr�vit",
'|88|Your skill in Travicstvi has increased by ',
-- hid
--"|946|Lilien: Dob�e se tu ukr�v�|",
--"|946|Odhalili t�!|",
--"|946|Lilien: Zde se ukr",
-- pasti
--"|50|NALEZENA PAST!|",
--"|946|Zacala jsi zneskodnovat past.|",
--"|32|Past spustila|",
--"|32|Nepodarilo se ti zneskodnit past|",
--"|68|Uspesne jsi na chvili zneskodnila past|",
--"|38|Zprava: prerusila jsi zneskodnovani pasti.|",
--"|38|Tve nehmotne ruce prochazeji skrz past|",
--'|946|Ve sv�m stavu toho p��li� neud�l�.|',
--"|946|Nedos�hne� na to.",
--'|155|Logout countdown: ',
-- you see:
'|946|You see: Carrie|',
'|946|You see: hnedy medved|',
'|946|You see: obri zaba|',
'|946|You see: Nakladac|',
'|946|You see: Duch|',
'|946|You see: Paracelsus|',
'|946|You see: Herka hrebec|',
'|946|You see: Elinor|',
'|99|You see: Manfred|',
'|946|You see: ovce|',
'|946|You see: Valecna klisna|',
'|99|You see: Clifton|',
'|946|You see: ptak|',
'|946|You see: kralik|',
'|946|You see: medved grizly|',
'|946|You see: cerny medved|',
'|946|You see: delfin|',
'|946|You see: straka|',
'|2882|You see: Sestra Jeanne|',
'|2882|You see: Otec Marat|',
'|2882|You see: Otec Leonardo|',
'|2882|You see: Magotar|',
'|946|You see: pralesni papousek|',
'|946|You see: gorila|',
'|946|You see: lan|',
'|946|You see: jelen|',
'|946|You see: havran|',
'|946|You see: Vlocka|',
'|946|You see: Rybi Hlava|',
'|946|You see: Svobodna|',
'|946|You see: orel|',
'|946|You see: vrana|',
'|946|You see: Sanga|',
"|99|You see: Burl|",
"|99|You see: Adrian|",
"|99|You see: Kenan|",
"|2882|You see: Jagar Tharn|",
"|38|You see: sliz|",
"|38|You see: obri krysa|",
"|946|You see: krysa|",
"|995|You see: stena sklepu|",
"|995|You see: kamenne dlazdice|",
"|995|You see: klenba|",
-- heal
"|1904|Vyber c�l: Na koho ",
"|1259|za",
"|93|�spch pYi l�",
"|93|Vyl�",
"|1356|Obvaz jsi musela zahodit - ",
"|946|Pokl�d� krvave obvazy do ba�ohu.|",
-- zraneni
--"|1269|�ste",
--"|40|�ste",
--"|1266|�ste",
--"|1378|�ste",
--"|946|Dostala jsi zran�n� ohn�m|",
--"|946|Dostala jsi zran�n� mrazem|",
--"|946|Dostala jsi zran�n� v�buchem|",
--"|946|Dostala jsi zran�n� energi�|",
-- save
"|54|Save sv�ta za 10 vte�in|",
"|54|Sv�t se na chv�li zastavil...|",
"|54|Sv�t se zase za�al to�it|",
'|946|Server byl POZASTAVEN kvuli Resyncu|',
'|946|Resync dokoncen!|',
-- SkriptMsg
--"|35|SkriptMsg! �ivoty: ",
--"|155|SkriptMsg! Spou�t�m heal|",
--"|155|SkriptMsg! Band�e: ",
--'|54|SkriptMsg! PAST - ',
-- prezbrojovani
--'|17|Selh�n� kouzla:',
--'|946|Nosenim zbroje z medi si ubiras na respektu|',
--'|68|Ochrana ',
--'|1925|Ochrana ',
--'|17|Ochrana p�ed zran�n�m zv���:',
--'|946|Po sundani medene zbroje se na tebe lide hned divaji jinak|',
--'|1268|Z�sk�v�a setov� efekt ',
--'|1356|Ztr�c�a setov� efekt ',
-- login
'|55|Login confirm on Andaria|',
'|0|Welcome to Ultima Online!|',
'|946|Ztrac� pocit naprost�ho bezpe��|',
'|1259|Vstupujete na cvi',
'|1259|M�a nezaplacen� dluhy na dan�ch - ',
'|53|Sphere Version ',
'|946|Na Andarii je je�t� ',
'|946|Last logged: ',
-- vyrobni
'|946|Jen s obt�emi vytv��� tento p�edm�t. Je velmi chatrn�|',
'|946|D�ky tv�m chab�m schopnostem v�robu p�edm�tu po��dn� odb�v�|',
'|946|Vytv��� zhruba podpr�m�rn� kvalitn� p�edm�t|',
'|946|V�robek je nadpr�m�rn� kvality|',
'|946|V�robek je v�born� kvality|',
'|68|D�ky sv�m v�jime�n�m schopnostem vyr�b� velice kvalitn� v�robek|',
-- ostatni
'|946|Nar�� na skryt�ho(ou) lan.|',
'|946|K odtla�en� medved grizly z cesty nem� dost s�ly.|',
'|946|Lilien: Dob�e se tu ukr�v�|',
'|1268|Odv�zala jsi sv� zv�Ye.|',
'|1906|______________|',
'|1906|Seznam pomocn�ko:|',
'|1905|Rybi Hlava ',
'|1905|Paracelsus ',
'|1905|Svobodna ',
'|1906|Zvl�dnea jeat ',
'|1905|medved grizly ',
'odvazuje ',
'uvazuje ',
'|1904|Vyber c�l: Uka~ na zv�Ye, je~ m� b�t uv�z�no.|',
'|1906|______________|',
'|1906|Seznam pomocn�ko:|',
'|1905|Neovlad�a ~�dn�ho pomocn�ka a zvl�dnea jeat ',
'|946|Na co chce',
'|0|Pathfinding!|',
'|946|Omlouvame se, bude lag',
'|946|Mus� po�kat na dokon�en� jin� akce.|',
'|0|It begins to rain.|',
'|0|It begins to snow.|',
'|946|Lilien: Nezda�ilo se.|',
"|946|Zam��ov�n� zru�eno.|",
"|946|Pokl�d� prazdne lahve do ba�ohu.|",
'|1925|Lahvi�ka se p�i manipulaci po�kodila, mus� ji vyhodit...|',
"|946|P�ka pas�a�ka: Cvak!|",
--"|946|Lilien: Vypad� �e",
": Hej, krok zpatky.|",
": Nechci problemy, ale: Krok zpet!|",
": Nestujte tak blizko.|",
' kousek ode ',
'|946|Pokl�d�',
'|543|C�t� se, �e ti to tr�nov�n� jde mnohem l�pe.|',
'|0||',
--'|946|Ty - m� hlad|',
--" truhlu*",
}
j = journal:new()
log(log_start_msg)
local CharPosX = UO.CharPosX
local CharPosY = UO.CharPosY
local timer=getticks()+70000

function movecheck()   if CharPosX~=UO.CharPosX or CharPosY~=UO.CharPosY then CharPosX=UO.CharPosX CharPosY=UO.CharPosY timer=getticks()+70000 end
                       if CharPosX==UO.CharPosX and CharPosY==UO.CharPosY and timer<getticks() then
                        local XY_alert = (nHour..(':')..nMinute..(' ').."NEHEJBES SE!!!")
                        if UO.CliLogged==true then log(XY_alert) wait(2000) end end
                       end

function weightcheck()   if UO.Weight~=nalozeni and UO.CliLogged == true then
                          local nHour, nMinute, nSecond, nMillisec = gettime() if nMinute<10 then nMinute = (0)..nMinute end
                          local nalozeni_alert = (nHour..(':')..nMinute..(' ').."KONTROLAAAAAAA!!!")
                          log(nalozeni_alert)
                          wait(2000)
                         end end

while true do
--  movecheck()
--  weightcheck()
  if UO.Hits<(UO.MaxHits-rezerva) and UO.CliLogged == true then 
   local nHour, nMinute, nSecond, nMillisec = gettime() if nMinute<10 then nMinute = (0)..nMinute end
   local hp_alert = (nHour..(':')..nMinute..(' ').."LOW HITPOINTS!!!" .."  ".. UO.Hits)
   log(hp_alert)
   wait(2000)
  end
  if j:next() ~= nil then
   local msg=j:last()
   for i=1,#ignore do if msg:find(ignore[i]) then msg=nil break end end
   if msg~=nil then
    local nHour, nMinute, nSecond, nMillisec = gettime() if nMinute<10 then nMinute = (0)..nMinute end
    local journal_msg = (nHour..(':')..nMinute..(' || ')..msg)
    log(journal_msg)
   end 
  end
  if getkey('num0') then UO.Macro(2,0,'protahla se') while getkey('num0') do wait(1) end end  
  wait(50)
end