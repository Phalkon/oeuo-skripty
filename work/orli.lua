dofile('../data/npcs.lua')
dofile('../data/items.lua')
dofile('../functions/FindItems.lua')

local minimal_pause = 500
local loot_bag = UO.BackpackID
local orlos=0
local vylooceno={}
                                   
local lokace_index=1
local lokaceX={1373,1421,1438,1407,1402,1365,1336,1284,1316,1334,1377}
local lokaceY={1420,1461,1471,1499,1527,1523,1527,1456,1417,1433,1430}

function use_object(UObjectID) UO.LObjectID = UObjectID UO.Macro(17,0) end
function target_object(TObjectID,TObjectKind) UO.LTargetID = TObjectID UO.LTargetKind = TObjectKind UO.Macro(22,0) end
function dragndrop(id,stack,drop) UO.Drag(id,stack) time_to_run=getticks()+200 repeat wait(1) wait(minimal_pause-1) until UO.LLiftedID==id or time_to_run<getticks() UO.DropC(drop) end
function warmode_cancel() while UO.CharStatus:find('G') and UO.Hits>0 do UO.Macro(6,0) wait(2000) end end

function WaitForTarCurs(timeout)
 time_to_run = getticks() + timeout
 repeat wait (1) until UO.TargCurs == true or time_to_run < getticks()
 if UO.TargCurs == true then return true else return false end
end

function vyloz()
UO.Move (1374,1433)
use_object(1074629882) wait(500) use_object(889440)
local get_pirka = ScanItems(true,{Type=pirko['type'],Kind=0,ContID=UO.BackpackID})
for i=1,#get_pirka do dragndrop(get_pirka[i].ID,get_pirka[i].Stack,1074630119) wait(minimal_pause) end
use_object(1074629882) WaitForTarCurs(2000) target_object(889440,1)
end


function smatrej()
warmode_cancel()
if lokace_index>#lokaceX then vyloz() lokace_index=1 end
UO.Move(lokaceX[lokace_index],lokaceY[lokace_index],3)
lokace_index=lokace_index+1
end

function get_new_target()
local getting_target = ScanItems(true,{Type={orel['type'],havran['type']},Kind=1})
if getting_target[1]~=nil then orlos=getting_target[1].ID end
if getting_target[1]==nil then smatrej() end
end

while true do
get_new_target()
repeat
local get_target = ScanItems(true,{ID=orlos})
if get_target[1]~=nil then
UO.LTargetID=get_target[1].ID UO.Macro(27,0)
if get_target[1].Dist>1 then UO.Pathfind(get_target[1].X,get_target[1].Y,get_target[1].Z) end
wait (minimal_pause) end
until get_target[1]==nil

repeat
local get_body = ScanItems(true,{Type=8198,Kind=1,Dist=2},{ID=vylooceno})
if get_body[1]~=nil then UO.Macro(1,0,'.usehand') WaitForTarCurs(2000) target_object(get_body[1].ID,get_body[1].Kind)
repeat UO.NextCPosX=0 UO.NextCPosY=0 use_object(get_body[1].ID) wait (minimal_pause) until UO.ContID==get_body[1].ID
local get_pirka = ScanItems(true,{Type=pirko['type'],Col={442,2269},Kind=0,ContID=get_body[1].ID})
for i=1,#get_pirka do dragndrop(get_pirka[i].ID,get_pirka[i].Stack,loot_bag) wait(minimal_pause) end table.insert(vylooceno,1,get_body[1].ID) end
wait (minimal_pause)
until get_body[1]==nil


end