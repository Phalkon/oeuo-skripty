dofile('../data/items.lua')
dofile('../functions/FindItems.lua')

function use_object(UObjectID) UO.LObjectID = UObjectID UO.Macro(17,0) end
function target_object(TObjectID,TObjectKind) UO.LTargetID = TObjectID UO.LTargetKind = TObjectKind UO.Macro(22,0) end

local bavlna = ScanItems(true,{Type=surova_bavlna['type'],Col=surova_bavlna['col'],Kind=0})
local kolovratek = ScanItems(true,{Type=kolovrat['type'],Col=kolovrat['col'],Dist=3})
if bavlna[1]~=nil and kolovratek[1]~=nil then
   repeat
   use_object(bavlna[1].ID) repeat wait(100) until UO.TargCurs==true
   target_object(kolovratek[1].ID,kolovratek[1].Kind)
   wait(6000)
   until false==true
end