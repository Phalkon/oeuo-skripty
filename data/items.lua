-- Items
-- by Phalkon



--- MINCE ----
zlatak = {['type']=3821,['col']=0}
stribrnak = {['type']=3824,['col']=0}
medak = {['type']=3821,['col']=2315}

--- SIPY ----
barvy = {['ohen']=40,['stribro']=1953,['zlato']=1944}
------------------
sip = {['type']=3903,['col']=0}
sipka = {['type']=7163,['col']=0}
elfi_sip = {['type']=3903,['col']=2209}
smrtici_sip = {['type']=3903,['col']=2118}
drtiva_sipka = {['type']=7163,['col']=1724}
ohnivy_sip = {['type']=3903,['col']=40}
ohniva_sipka = {['type']=7163,['col']=40}
ledovy_sip = {['type']=3903,['col']=6}
ledova_sipka = {['type']=7163,['col']=6}  
stribrny_sip = {['type']=3903,['col']=1953}
stribrna_sipka = {['type']=7163,['col']=1953}
zlaty_sip = {['type']=3903,['col']=1944}
zlata_sipka = {['type']=7163,['col']=1944}

--- REGY ----
--obycejne
mandragora = {['type']=3974,['col']=0}
zensen = {['type']=3973,['col']=0}
rulik = {['type']=3976,['col']=0}
cesnek = {['type']=3972,['col']=0}
sirny_prach = {['type']=3980,['col']=0}
krvavy_mech = {['type']=3963,['col']=0}
pavucina = {['type']=3981,['col']=0}
--neobycejne
kost = {['type']=3966,['col']=0}
krvava_kost = {['type']=3966,['col']=1171}
cerna_kost = {['type']=3966,['col']=1109}
urodna_prst = {['type']=3969,['col']=0}
obsidian = {['type']=3977,['col']=0}
surove_zelezo = {['type']=nil,['col']=nil}
pemza = {['type']=3979,['col']=0}
prach_z_hrobu = {['type']=3983,['col']=0}
suche_drevo = {['type']=3984,['col']=0}
cerny_vres = {['type']=3961,['col']=0}
katova_kukla = {['type']=3971,['col']=0}
cerna_perla = {['type']=3962,['col']=0}
krvave_jikry = {['type']=3964,['col']=0}
--vzacne
netopyri_kridlo = {['type']=3960,['col']=0}
krev_demona = {['type']=3965,['col']=0}
draci_krev = {['type']=3970,['col']=0}
draci_srdce = {['type']=3985,['col']=0}
krystal_nox = {['type']=3982,['col']=0}
kost_demona = {['type']=3968,['col']=0}
slizke_oko = {['type']=3967,['col']=0}
--esence
esence_ohne = {['type']=15143,['col']=2994}
esence_vzduchu = {['type']=15143,['col']=21}
esence_slizu = {['type']=15143,['col']=63}
esence_zeme = {['type']=15143,['col']=2263}
esence_vody = {['type']=15143,['col']=6}

--- LEKTVARY ----
--slabe
slabsi_lektvar_hbitosti = {['type']=3848,['col']=0}
slabsi_lektvar_sily = {['type']=3849,['col']=0}
slabsi_lektvar_moudrosti = {['type']=3842,['col']=0}
slabsi_lecivy_lektvar = {['type']=3852,['col']=0}
slabsi_protijed = {['type']=3847,['col']=0}
slabsi_lektvar_osvezeni = {['type']=3851,['col']=0}
slabsi_lektvar_doplneni_many = {['type']=nil,['col']=nil}
slabsi_lektvar_obnovy = {['type']=3836,['col']=0}
slabsi_vybusny_lektvar = {['type']=3853,['col']=0}
slabsi_jed = {['type']=3850,['col']=0}
--silne
silny_lektvar_hbitosti = {['type']=15348,['col']=1361}
silny_lektvar_sily = {['type']=15348,['col']=0}
silny_lecivy_lektvar = {['type']=15348,['col']=2642}
silny_lektvar_obnovy = {['type']=15348,['col']=33}
silny_protijed = {['type']=15348,['col']=1259}
silny_lektvar_osvezeni = {['type']=15348,['col']=2648}
--dalsi
lektvar_svetla = {['type']=3846,['col']=0}
lektvar_magicke_zbroje = {['type']=3849,['col']=1994}
lektvar_tranzu = {['type']=3841,['col']=0}
lektvar_doplneni_many = {['type']=6215,['col']=0}
--ostatn�
lektvar_nasyceni = {['type']=3838,['col']=0}
lektvar_skryti_povesti = {['type']=3844,['col']=0}
lektvar_zmensovani = {['type']=3621,['col']=904}
lektvar_ocelove_kuze = {['type']=3623,['col']=2641}
lektvar_rozptyleni_magie = {['type']=3849,['col']=1151}
lektvar_neviditelnosti = {['type']=3627,['col']=0}
lektvar_nezranitelnosti = {['type']=3835,['col']=0}
lektvar_skryti_povesti = {['type']=3844,['col']=0}

--- SVITKY ----
svitek_prvni_stupen = {['type']=3637,['col']=0}
svitek_zivot = {['type']=3637,['col']=2705}
svitek_smrt = {['type']=3637,['col']=977}
svitek_rad = {['type']=3637,['col']=2934}
svitek_chaos = {['type']=3637,['col']=2878}
svitek_materie = {['type']=3637,['col']=2805}
svitek_priroda = {['type']=3637,['col']=1368}

--- HULKY ----
hulka = {['type']={3570,3571,3572,3573},['col']=0}
                       
--- PRUTY ----
prut_zeleza = {['type']=7151,['col']=0}
prut_medi = {['type']=7139,['col']=0}
prut_stribra = {['type']=7157,['col']=0}
prut_zlata = {['type']=7145,['col']=0}
prut_mitrilu = {['type']=7151,['col']=2057}

--- KUZE ----
hromada_kuzi = {['type']=4216,['col']=0}
hromada_vlcich_kuzi = {['type']=4216,['col']=978}
hromada_medvedich_kuzi = {['type']=4216,['col']=47}
rozstrihana_kuze = {['type']=4199,['col']=0}
rozstrihana_vlci_kuze = {['type']=4199,['col']=978}
rozstrihana_medvedi_kuze = {['type']=4199,['col']=47}

--- KREJCOVINA ----
surova_bavlna = {['type']=3577,['col']=0}


--- TROFEJE ----
vrrci_tlapka = {['type']=5640,['col']=2241}
skreti_tesak = {['type']=15170,['col']=1446}
zamotek_obriho_pavouka = {['type']=5926,['col']=1153}
sklipkani_zamotek = {['type']=5926,['col']=1664}
gorili_mozek = {['type']=7408,['col']=2718}  

--- HADRY ----
dublet = {['type']=8059,['col']='col'}
serpa = {['type']=5441,['col']='col'}
roba = {['type']=7939,['col']='col'}
kosile = {['type']=5399,['col']='col'}
zdobena_kosile = {['type']=7933,['col']='col'}
obycejne_saty = {['type']=7937,['col']='col'}
dlouhe_kalhoty = {['type']=5433,['col']='col'}
kratke_kalhoty = {['type']=5422,['col']='col'}
tunika = {['type']=8097,['col']='col'}
zdobene_saty = {['type']=7935,['col']='col'}
kilt = {['type']=5431,['col']='col'}
kape = {['type']=9716,['col']='col'}
--nepadaji
klobouk = {['type']=5907,['col']='col'}
kouzelnicky_klobouk_z_platna = {['type']=5912,['col']='col'}

--- KOVY ----
prut_zeleza = {['type']=7151,['col']=0}

--- NARADI ----
nuzky = {['type']=3998,['col']=0}
umyvadlo = {['type']=4104,['col']=0}
nuz_na_stahovani = {['type']=3780,['col']=0}
dratenicke_nacini = {['type']=7868,['col']=0}
paklic = {['type']=5371,['col']=0}
lopata = {['type']=3897,['col']=0}
vyhen = {['type']=4017,['col']=0}
kolovrat = {['type']=4124,['col']=0}

--- DALSI ----
prazdna_lahev = {['type']=3854,['col']=0}
strevo = {['type']=7407,['col']=443}
pirko = {['type']=7121,['col']=0}
orli_pirko = {['type']=7121,['col']=442}
havrani_pirko = {['type']=7121,['col']=2269}
obvaz = {['type']=3617,['col']=0}
krvavy_obvaz = {['type']=3616,['col']=0}
lahev_vina = {['type']=2503,['col']=0}
kus_masa = {['type']=2545,['col']=0}
sadlo = {['type']=7818,['col']=0}
zavrene_dvere = {788,1653,1654,1655,1657,1659,1661,1663,1667,1669,1673,1677,1679,1683,1701,1703,1705,1709,1711,1717,1721,1733,1735,1737,1739,1741,1743,1749,1753,1755,1757,1765,1767,1775,1773,1779,1782,2084,2088,2090,2092,2094,2132,2134,2150,2154,2160,8173,8174,9135,9137,9138,9141,9142,9143,9144,11940}
otevrene_dvere = {789,1654,1656,1660,1662,1664,1674,1678,1680,1684,1702,1704,1706,1710,1712,1718,1722,1734,1736,1738,1740,1742,1744,1750,1754,1756,1758,1766,1768,1774,1776,1780,2085,2089,2091,2093,2095,2133,2135,2151,2155,2161,9135,9138,9139,9144,9145,9141,9146}


--- ZBRANE ----
dyka = {['type']={3921,3922},['col']=0}
kopi = {['type']={3938,3939},['col']=0}
pozlacene_kopi = {['type']=3939,['col']=1944}
kratke_kopi = {['type']={5122,5123},['col']=0}
kris = {['type']={5120,5121},['col']=0}
-- strelne
tezka_kuse = {['type']={5116,5117},['col']=0}
skladany_luk = {['type']={9922,9932},['col']=0}
elfi_luk = {['type']=11550,['col']=0}
-- specialni
vrhaci_nuz = {['type']=15455,['col']=2414}
bic = {['type']=5742,['col']=0}
-- stity
kruhovy_kovovy_stit = {['type']=7035,['col']=0}
dreveny_stit = {['type']=7034,['col']=0}

--- ZBROJE ----
dratena_kosile_ze_zlata = {['type']=5055,['col']=1944}
kozeny_limec = {['type']=5063,['col']=0}
--kostena
hrudni_plat_z_kosti = {['type']=5199,['col']=0}
nohavice_z_kosti = {['type']=5202,['col']=0}
rukavy_z_kosti = {['type']=5198,['col']=0}
rukavice_z_kosti = {['type']=5200,['col']=0}
prilba_z_kosti = {['type']=5201,['col']=0}

--- PETI MINI ----
obri_had = {['type']=8444,['col']=0}
medved_grizzly = {['type']=8478,['col']=0}
--- MOUNTI MINI ----
bila_herka = {['type']=8479,['col']=0}
bily_kun = {['type']=8480,['col']=1673}
hnedy_kun = {['type']={8480,8413},['col']=1709}
bezovy_kun = {['type']={8484},['col']=2308}
hneda_slechta = {['type']={8480,8413},['col']=1950}
cerna_slechta = {['type']={8479},['col']=1109}
bila_slechta = {['type']={8413,8480},['col']=1673}
cerny_valec = {['type']={8481},['col']=2064}
bahenni_drak = {['type']={9753},['col']=1442}

--- INVIS ITEMY ----

vlasy = {8197,8264,8251,8261,8262,8253,8252,8263,8265,8266,8260,12224,12237,12238,12239,12240,12241,1223,12225,12226,12236}
vousy = {8197}

--zadne_vlasy 8197
--ustupujici_vlasy 8264
--kratke_vlasy 8251
--pazeci_uces 8261
--drdol 8262
--ohon 8253
--dlouhe_vlasy 8252
--kratke_vlasy_s_patkou 8263
--dva_copy 8265
--uzlik 8266
--indiansky_uces 8260
--dlouhe_elfi_vlasy_s_pirkem 12224
--dlouhe_elfi_vlasy_2 12237
--elfi_vlasy 12238
--elfi_vlasy 12239
--elfi_vlasy 12240
--elfi_vlasy 12241
--stredne_dlouhe_elfi_vlasy 12223
--kratke_elfi_vlasy 12225
--elfi_vlasy_mullet 12226
--kytickove_elfi_vlasy 12236

--zadne_vousy 8197
