lootBagID = 1073907465
------------------------------------------------------------------
zlatky    = {['storeCont']=1074761352,zlatak,stribrnak,medak}
regy_obyc = {['storeCont']=1073755657,mandragora,zensen,rulik,sirny_prach,krvavy_mech,pavucina,kost,cerna_perla,katova_kukla,cerny_vres,cesnek}
regy_vzac = {['storeCont']=nil,krvava_kost,cerna_kost,kost_demona,netopyri_kridlo,krystal_nox,urodna_prst,pemza,suche_drevo,prach_z_hrobu,obsidian,krev_demona,draci_krev,draci_srdce,slizke_oko,krvave_jikry}
esence    = {['storeCont']=nil,esence_ohne,esence_vzduchu,esence_slizu,esence_zeme,esence_vody}
lektvary  = {['storeCont']=1074127435,slabsi_lektvar_hbitosti,slabsi_lektvar_sily,slabsi_lektvar_moudrosti,lektvar_svetla,slabsi_lecivy_lektvar,slabsi_protijed,slabsi_lektvar_osvezeni,slabsi_lektvar_obnovy,slabsi_vybusny_lektvar,slabsi_jed,lektvar_tranzu,lektvar_doplneni_many}
sipy      = {['storeCont']=nil,sip,elfi_sip,smrtici_sip,ohnivy_sip,stribrny_sip,zlaty_sip}
sipky     = {['storeCont']=nil,sipka,drtiva_sipka,ohniva_sipka,stribrna_sipka,zlata_sipka}
kuze      = {['storeCont']=1074059220,hromada_kuzi,hromada_vlcich_kuzi,hromada_medvedich_kuzi,rozstrihana_kuze,rozstrihana_vlci_kuze,rozstrihana_medvedi_kuze}
hadry     = {['storeCont']=nil,dublet,serpa,roba,kosile,zdobena_kosile,obycejne_saty,dlouhe_kalhoty,kratke_kalhoty,tunika,zdobene_saty,kilt,kape}
pirka     = {['storeCont']=nil,pirko,orli_pirko,havrani_pirko}
pruty     = {['storeCont']=nil,prut_zeleza,prut_medi,prut_stribra,prut_zlata}
trofeje   = {['storeCont']=nil,vrrci_tlapka,skreti_tesak,zamotek_obriho_pavouka,sklipkani_zamotek,gorili_mozek}
lahve     = {['storeCont']=1074438447,prazdna_lahev}
strivka   = {['storeCont']=1074059220,strevo,sadlo}
svitky    = {['storeCont']=1074059316,svitek_prvni_stupen}
obvazy    = {['storeCont']=1074787744,obvaz,krvavy_obvaz} 
hulky     = {['storeCont']=1074059614,hulka} 
ostatni   = {['storeCont']=nil,}  --hulka
------------------------------------------------------------------------------------------------
lootit = {zlatky,regy_obyc,regy_vzac,esence,lektvary,kuze,hadry,pirka,pruty,trofeje,lahve,strivka,svitky,obvazy,hulky,ostatni}