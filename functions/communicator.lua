-- base fce
function communicator(tag,msg) local file = openfile(getinstalldir()..'/.communicator/communicator_file','a') if file then file:write(tag..' '..msg..'\n') file:close() end end
                                                        
-- cm_fce
function cm_antiafk(msg) communicator('ANTIAFK!',msg) end
function cm_screen() communicator('SCREEN!',0) end
--function cm_restart() communicator('RESTART!',0) end
function cm_gamma(vypinac) if vypinac=='on' then communicator('GAMMASET!',0) elseif vypinac=='off' then communicator('GAMMARESET!',0) end end
function cm_mapa(vypinac) if vypinac=='on' then communicator('MAPA!',0) elseif vypinac=='off' then communicator('kill_MAPA!',0) end end
