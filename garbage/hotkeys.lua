dofile('data/looter.lua')
dofile('functions/hotkey_funcs.lua')

jezditko_name = "Pani Noci"
jezditko_id = 951865
jezditko_mini = 8480
uvaz_kul_id = 1074475103
bagl_id = 1074337139

pet_id = 787462 -- type 21
pet_mini_type = obri_had["type"]

weapon_1H = {kratke_kopi["type"][1],kris["type"][1],dyka["type"][1],kratke_kopi["type"][2],kris["type"][2],dyka["type"][2]}
strelne = {tezka_kuse["type"][1],skladany_luk["type"][1],elfi_luk["type"],tezka_kuse["type"][2],skladany_luk["type"][2]}

variable = 1
ClearUOMsg()
wait (100)
keys_enabled()
j = journal:new()
warmode_alert=getticks()-10000
while true do
-----vyta�en� statusu-------
if UO.MaxHits == 0 or UO.MaxStam == 0 then
local cpox=UO.ContPosX UO.Macro(10,2) repeat wait(1) until UO.ContPosX~=cposx
UO.Macro(8,2) wait_for_gump(33981)
local cpox=UO.ContPosX UO.Macro(10,2) repeat wait(1) until UO.ContPosX~=cposx
UO.ContPosX=824 UO.ContPosY=343 end
------------------------
repeat wait (1)
  while UO.CliLogged~=true do wait(100) if UO.CliCnt<1 then print("Klient spadl!") stop() end end
---TALKing
  if modifier_key("none") and getkey("m") then keys_disabled("message") end
  if modifier_key("none") and getkey("n") then keys_disabled("emote") end
  if modifier_key("none") and getkey("b") then keys_disabled("party") end
---HID
  if modifier_key("alt") and getkey("q") and hid_pocitadlo==nil then warmode_cancel() UO.Macro(13,21) hid_pocitadlo=0 hidtimer=getticks() hid_pocitani=true until_key_lifted("q") end
  if modifier_key("alt") and getkey("q") and hid_pocitadlo~=nil then warmode_cancel() UO.Macro(13,21) wait(100)
     if UO.SysMsg~="Mus� po�kat na dokon�en� jin� akce." and UO.SysMsg~="Jsi p��li� zam�stn�n(a) bojem." then hid_pocitadlo=0 hidtimer=getticks() hid_pocitani=true end
     until_key_lifted("q") end
---ZBRAN�
  if modifier_key("ctrl") and getkey("a") then nahod_kopi() tarcurs_remover=1 until_key_lifted("a") end
  if modifier_key("ctrl") and getkey("s") then zbran(weapon_1H) zbran(kruhovy_kovovy_stit["type"]) tarcurs_remover=1 until_key_lifted("s") end
  if modifier_key("ctrl") and getkey("z") then nahod_ostep() tarcurs_remover=1 until_key_lifted("z") end
  if modifier_key("ctrl") and getkey("x") then zbran(strelne) until_key_lifted("x") end  
  if modifier_key("shift") and getkey("s") then zbran(vrhaci_nuz["type"]) until_key_lifted("s") end
  if modifier_key("alt") and getkey("s") then zbran(bic["type"]) UO.Macro(1,0,".startability 17") tarcurs_remover=1 until_key_lifted("s") end
  if modifier_key("ctrl") and getkey("c") then UO.Macro(24,1) until_key_lifted("c") end -- disarm
---TARGETY 
  if modifier_key("alt") and getkey("1") then UO.Macro(50,5) until_key_lifted("1") end -- target next
  if modifier_key("alt") and getkey("2") then UO.Macro(53,0) until_key_lifted("2") end -- attack target
  if modifier_key("alt") and getkey("3") then UO.Macro(51,5) until_key_lifted("3") end -- target previous
---LEKTVARY
  if modifier_key("shift") and getkey("v") then lektvar(lektvar_skryti_povesti["type"],lektvar_skryti_povesti["col"]) until_key_lifted("v") end
  if modifier_key("none") and getkey("a") then UO.Macro(1,0,".obnova") if lektvar_pocitadlo==nil then lektvar_timer=getticks()+12000 lektvar_pocitadlo=0 lektvar_pocitani=true end until_key_lifted("a") end
  if modifier_key("none") and getkey("s") then UO.Macro(1,0,".lecit") if lektvar_pocitadlo==nil then lektvar_timer=getticks()+9000 lektvar_pocitadlo=0 lektvar_pocitani=true end until_key_lifted("s") end
  if modifier_key("none") and getkey("e") then UO.Macro(1,0,".protijed") until_key_lifted("e") end
  if modifier_key("none") and getkey("t") then UO.Macro(1,0,".lektvar ocel") until_key_lifted("t") end
  if modifier_key("none") and getkey("w") then UO.Macro(1,0,".lektvar vybuch") WaitForTarCurs(100) target_object(UO.CharID,1) until_key_lifted("w") end
  if modifier_key("none") and getkey("x") then UO.Macro(1,0,".lektvar hbitost") until_key_lifted("x") end
  if modifier_key("none") and getkey("z") then UO.Macro(1,0,".lektvar osvez") until_key_lifted("z") end
  if modifier_key("ctrl") and getkey("q") then UO.Macro(1,0,".lektvar neviditelnost") until_key_lifted("q") end
---ABILITKY
  if modifier_key("alt") and getkey("w") then UO.Macro(1,0,".startability 25") until_key_lifted("w") end
  if modifier_key("alt") and getkey("x") then UO.Macro(1,0,".nabij") until_key_lifted("x") end
  if modifier_key("alt") and getkey("z") then UO.Macro(1,0,".startability 4") until_key_lifted("z") end
  if modifier_key("none") and getkey("space") then UO.Macro(1,0,".startability 20") until_key_lifted("space") end
---SKILLY
  if modifier_key("none") and getkey("v") then UO.Macro(13,14) until_key_lifted("v") end -- detect hiding
  if modifier_key("none") and getkey("p") then UO.Macro(13,19) until_key_lifted("p") end -- detect forensics
  if modifier_key("none") and getkey("i") then UO.Macro(13,3) until_key_lifted("i") end -- item identification
  if modifier_key("none") and getkey("k") then UO.Macro(13,33) until_key_lifted("k") end -- stealing
  if modifier_key("none") and getkey("h") then UO.Macro(13,21) until_key_lifted("h") end -- detect hiding
  if modifier_key("none") and getkey("g") then UO.TargCurs=true UO.SysMessage("Vyber past k zne�kodn�n�",54) pasticka=1 until_key_lifted("g") end -- bomb disarming
  if modifier_key("none") and getkey("l") then UO.Macro(13,38) until_key_lifted("l") end -- tracking
  if modifier_key("none") and getkey("f") then useskill_tracking("hraci") until_key_lifted("f") end -- tracking players
  if modifier_key("alt") and getkey("l") then useskill_tracking("zvirata") until_key_lifted("l") end -- tracking animals
  if modifier_key("shift") and getkey("1") then UO.Macro(13,22) until_key_lifted("1") end -- provocation
  if modifier_key("shift") and getkey("2") then UO.Macro(13,9) until_key_lifted("2") end -- peacemaking
  if modifier_key("shift") and getkey("3") then UO.Macro(13,15) until_key_lifted("3") end -- lakani
---PET CONTROL
  if modifier_key("shift") and getkey("e") then lektvar(pet_mini_type,0) until_key_lifted("e") end
  if modifier_key("shift") and getkey("d") then pet_do_baglu(pet_id,pet_mini_type) until_key_lifted("d") end
  if modifier_key("alt") and getkey("d") then UO.Macro(3,0,"all stay") until_key_lifted("d") end
  if modifier_key("alt") and getkey("e") then UO.Macro(3,0,"all come") until_key_lifted("e") end
  if modifier_key("alt") and getkey("f") then UO.Macro(3,0,"all stop") until_key_lifted("f") end
  if modifier_key("alt") and getkey("r") then UO.Macro(3,0,"all go") until_key_lifted("r") end
---DAL��
  if modifier_key("alt") and getkey("a") then naskoc() until_key_lifted("a") end
  if modifier_key("shift") and getkey("a") then seskoc() until_key_lifted("a") end
  if modifier_key("shift") and getkey("z") then uvaz() until_key_lifted("z") end
  if modifier_key("none") and getkey("q") then dvere(zavrene_dvere) until_key_lifted("q") end
  if modifier_key("shift") and getkey("q") then dvere(otevrene_dvere) until_key_lifted("q") end
  if modifier_key("none") and getkey("d") then UO.Macro(1,0,".obvaz") WaitForTarCurs(100) target_object(UO.CharID,1) until_key_lifted("d") end
  if modifier_key("none") and getkey("j") then UO.Macro(1,0,".kopat_poklad") until_key_lifted("j") end
  if getkey("ctrl") and getkey("shift") and getkey("z") then UO.Macro(1,0,".usehand") until_key_lifted("z") end
  if modifier_key("alt") and getkey("o") then toulec("napln") until_key_lifted("o") end
  if modifier_key("alt") and getkey("p") then toulec("vyprazdni") until_key_lifted("p") end
---MISCELLANEOUS
  if getkey("alt") and getkey("shift") and getkey("q") then UO.Macro(20,0) stop() end   ------ exit UO
  if modifier_key("alt") and getkey("j") then UO.Macro(8,3) until_key_lifted("q") end  ------ open journal
  if getkey("F10") then open_paperdoll() until_key_lifted("F10") end
  if getkey("F5") then UO.NextCPosX=786 UO.NextCPosY=374 use_object(UO.BackpackID) repeat wait(1) until UO.ContID==UO.BackpackID
      UO.NextCPosX=0 UO.NextCPosY=0 use_object(bagl_id) repeat wait(1) until UO.ContID==bagl_id UO.ContPosX=786 UO.ContPosY=545
      UO.NextCPosX=0 UO.NextCPosY=0 use_object(1074118350) repeat wait(1) until UO.ContID==1074118350 UO.ContPosX=609 UO.ContPosY=590
      until_key_lifted("F5") end
  if modifier_key("none") and getkey("f9") then UO.Macro(1,0,".buff_lista") until_key_lifted("f9") end
  if modifier_key("none") and getkey("f11") then UO.Macro(1,0,".dekorater") until_key_lifted("f11") end
  if modifier_key("none") and getkey("f12") then UO.Macro(1,0,".schoptext") until_key_lifted("f12") end
---P�EZBROJEN�
--  if getkey("F8") then prezbrojeni("nahod") end
--  if getkey("F9") then prezbrojeni("shod") end
---LOOTER
  if modifier_key("none") and getkey("F1") then lootuj("mrtvoly",loot,lootBagID) until_key_lifted("F1") end
  if modifier_key("none") and getkey("F2") then lootuj("target",loot,lootBagID) until_key_lifted("F2") end
  if modifier_key("none") and getkey("F3") then lootuj("target", "vse",UO.CharID) until_key_lifted("F3") end
  if modifier_key("none") and getkey("F4") then roztrid(lootit,lootBagID) until_key_lifted("F4") end
---------------- pasti ---------------------------   
  if pasticka==1 and UO.TargCurs==false then pasticka=nil pasti() end
--------------------------------------------------
------------ journal scan -----------------------  
  if j:next() ~= nil then
   local msg=j:last()
    if msg~=nil then
      if msg:find("*vrh� d�ku*") then dyka_timer=getticks()+4000 dyka_pocitadlo=0 dyka_pocitani=true end
      if msg:find("*se ") and msg:find(" na skok.*") then skok_timer=getticks()+4000 skok_pocitadlo=0 skok_pocitani=true end
    end
  end
---------------------------------------------------------------
  pocitadla()
---------------------------------------------------------------
  if UO.CharStatus:find("G") and warmode_alert<getticks()-10000 then warmode_alert=getticks() UO.SysMessage("Warning: Warmode ON") end
---------------------------------------------------------------
  if tarcurs_remover==1 and UO.TargCurs==true then tarcurs_remover=nil UO.TargCurs=false end
--------------------------------------------------
until (hits~=UO.Hits and UO.Hits~=nil) or (stamka~=UO.Stamina and UO.Stamina~=nil) or (DmgMin~=UO.MinDmg and UO.MinDmg~=nil) or (DmgMax~=UO.MaxDmg and UO.MaxDmg~=nil) or (armor~=UO.AR and UO.AR~=nil) or (ohen~=UO.FR and UO.FR~=nil) or (mraz~=UO.CR and UO.CR~=nil) or (kyselina~=UO.PR and UO.PR~=nil) or (elektrina~=UO.ER and UO.ER~=nil) or (nalozeni~=UO.Weight and UO.Weight~=nil) or (nalozeniMAX~=UO.MaxWeight and UO.MaxWeight~=nil) or (ehits~=UO.EnemyHits and UO.EnemyID~=nil)
---------------------------------------------------------------
----------------- S T A T S -----------------------------------
---------------------------------------------------------------
----CharHP+CharStam----------
if (hits~=UO.Hits and UO.Hits~=nil) or (stamka~=UO.Stamina and UO.Stamina~=nil) then
if UO.MaxHits~=0 and UO.MaxStam~=0 then
 UO.ExMsg (UO.CharID,3,35,tostring(UO.Hits)..(" / ")..(UO.Stamina)) end -- mozna zmena na barvu: 38
hits=UO.Hits
stamka=UO.Stamina end
----EnemyHP------------------
if ehits~=UO.EnemyHits and UO.EnemyID~=nil then
if UO.EnemyHits~=0 then
if UO.EnemyHits==4 and UO.EnemyID~=enemy then variable=50 end
local vypocet=(UO.EnemyHits/2)*variable if vypocet > 100 then vypocet = 100 end
UO.ExMsg (UO.EnemyID,3,57,tostring((vypocet)..("%"))) end
ehits=UO.EnemyHits
enemy=UO.EnemyID
variable=1 end
-----DMG-------------
if (DmgMin ~= UO.MinDmg or DmgMax ~= UO.MaxDmg) and (UO.MinDmg ~= nil or UO.MaxDmg ~= nil) then
if UO.MaxDmg~=0 then
 UO.ExMsg (UO.CharID,3,1259,tostring("dmg: ")..(UO.MinDmg)..(" - ")..(UO.MaxDmg)) end
DmgMin = UO.MinDmg
DmgMax = UO.MaxDmg end
-----Armor-------------
if (nalozeni~=UO.Weight or nalozeniMAX~=UO.MaxWeight) and (UO.Weight~=nil or UO.MaxWeight~=nil) then
if UO.MaxWeight~=0 then
 UO.ExMsg (UO.CharID,3,1378,tostring("nalozeni: ")..(UO.Weight)..(" / ")..(UO.MaxWeight)) end
nalozeni = UO.Weight
nalozeniMAX = UO.MaxWeight end
-----Nalozeni-------------
if armor~=UO.AR and UO.AR~=nil then
UO.ExMsg (UO.CharID,3,1673,tostring("armor: ")..(UO.AR))
armor=UO.AR end
-----Resisty-------------
if ohen~=UO.FR and UO.FR~=nil then
if UO.FR<500 or UO.FR>(-500) then
UO.ExMsg (UO.CharID,3,40,tostring("ohen: ")..(UO.FR)) end
ohen=UO.FR end
if mraz~=UO.CR and UO.CR~=nil then
if UO.CR<500 or UO.CR>(-500) then
UO.ExMsg (UO.CharID,3,1266,tostring("mraz: ")..(UO.CR)) end
mraz=UO.CR end
if kyselina~=UO.PR and UO.PR~=nil then
if UO.PR<500 or UO.PR>(-500) then
UO.ExMsg (UO.CharID,3,1269,tostring("kyselina: ")..(UO.PR)) end
kyselina=UO.PR end
if elektrina~=UO.ER and UO.ER~=nil then
if UO.ER<500 or UO.ER>(-500) then
UO.ExMsg (UO.CharID,3,54,tostring("elektrina: ")..(UO.ER)) end
elektrina=UO.ER end
end