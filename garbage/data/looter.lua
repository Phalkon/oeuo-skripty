-- Looter
-- by Phalkon

dofile('../functions/looter_funcs.lua')
dofile('items.lua')

--VARIABLES
local zlatky = {["bankaBag"]=nil,zlatak,stribrnak,medak}
local regy_obyc = {["bankaBag"]=1074225859,mandragora,zensen,rulik,sirny_prach,krvavy_mech,pavucina,kost,cerna_perla,katova_kukla,cerny_vres}
local regy_vzac = {["bankaBag"]=1073832617,krvava_kost,cerna_kost,kost_demona,netopyri_kridlo,krystal_nox,urodna_prst,pemza,suche_drevo,prach_z_hrobu,obsidian}
local esence = {["bankaBag"]=1074697417,esence_ohne,esence_vzduchu,esence_slizu,esence_zeme,esence_vody}
local lektvary = {["bankaBag"]=1074583195,slabsi_lektvar_hbitosti,slabsi_lektvar_sily,slabsi_lektvar_moudrosti,lektvar_svetla,slabsi_lecivy_lektvar,slabsi_protijed,slabsi_lektvar_osvezeni,slabsi_lektvar_obnovy,slabsi_vybusny_lektvar,slabsi_jed}
local obyc_sipy_a_sipky = {["bankaBag"]=1074600389,sip,sipka}
local sipy_barevne = {["bankaBag"]=1074600389,elfi_sip,smrtici_sip,ohnivy_sip,stribrny_sip,zlaty_sip}
local sipky_barevne = {["bankaBag"]=1074600389,drtiva_sipka,ohniva_sipka,stribrna_sipka,zlata_sipka}
local kuze = {["bankaBag"]=1073746071,hromada_kuzi,hromada_vlcich_kuzi,hromada_medvedich_kuzi,rozstrihana_kuze,rozstrihana_vlci_kuze,rozstrihana_medvedi_kuze}
local hadry = {["bankaBag"]=nil,dublet,serpa,roba,kosile,zdobena_kosile,obycejne_saty,dlouhe_kalhoty,kratke_kalhoty,tunika,zdobene_saty}
local pirka = {["bankaBag"]=1074697417,pirko,orli_pirko,havrani_pirko}
local pruty = {["bankaBag"]=1073832997,prut_zeleza,prut_medi,prut_stribra,prut_zlata}
local trofeje = {["bankaBag"]=1073832997,vrrci_tlapka,skreti_tesak,zamotek_obriho_pavouka}
local lahve = {["bankaBag"]=1074226166,prazdna_lahev}
local strivka = {["bankaBag"]=1073832997,strevo} 
local ostatni = {["bankaBag"]=nil,obvaz,krvavy_obvaz,hulka}

lootit = {zlatky,regy_obyc,regy_vzac,esence,lektvary,obyc_sipy_a_sipky,sipy_barevne,sipky_barevne,kuze,hadry,pirka,pruty,trofeje,lahve,strivka,ostatni}

lootBagID = 1074563275
ltr_lag_pause = 0
dontloot = {8197,8252}

loot = {}
local cntr = 1
local duplicite = {}
for i1=1,#lootit do for i2=1,#lootit[i1] do
 for y=1,#loot do if lootit[i1][i2]["type"]==loot[y] then
  duplicite[lootit[i1][i2]["type"]]=1 break end
 end
 if duplicite[lootit[i1][i2]["type"]]~=1 then
  loot[cntr]=lootit[i1][i2]["type"] cntr=cntr+1
 end
end end