-- Looter
-- by Phalkon

dofile('functions/looter_funcs.lua')
dofile('data/items.lua')

--VARIABLES
local zlatky = {["bankaBag"]=nil,zlatak,stribrnak,medak}
local regy_obyc = {["bankaBag"]=1074225859,mandragora,zensen,rulik,sirny_prach,krvavy_mech,pavucina,kost,cerna_perla,katova_kukla,cerny_vres}
local regy_vzac = {["bankaBag"]=1073832617,krvava_kost,cerna_kost,netopyri_kridlo,krystal_nox,urodna_prst,pemza,suche_drevo}
local esence = {["bankaBag"]=1074697417,esence_ohne,esence_vzduchu,esence_slizu,esence_zeme}
local lektvary = {["bankaBag"]=1074583195,slabsi_lektvar_hbitosti,slabsi_lektvar_sily,slabsi_lektvar_moudrosti,lektvar_svetla,slabsi_lecivy_lektvar,slabsi_protijed,slabsi_lektvar_osvezeni,slabsi_lektvar_obnovy,slabsi_vybusny_lektvar,slabsi_jed}
local obyc_sipy_a_sipky = {["bankaBag"]=1074600389,sip,sipka}
local sipy_barevne = {["bankaBag"]=1073936361,elfi_sip,smrtici_sip,ohnivy_sip,stribrny_sip,zlaty_sip}
local sipky_barevne = {["bankaBag"]=1073937738,ohniva_sipka,stribrna_sipka,zlata_sipka}
local kuze = {["bankaBag"]=1073746071,hromada_kuzi,hromada_vlcich_kuzi,hromada_medvedich_kuzi,rozstrihana_kuze,rozstrihana_vlci_kuze,rozstrihana_medvedi_kuze}
local hadry = {["bankaBag"]=nil,dublet,serpa,roba,kosile,zdobena_kosile,obycejne_saty,dlouhe_kalhoty,kratke_kalhoty,tunika}
local pirka = {["bankaBag"]=1074697417,pirko,orli_pirko}
local pruty = {["bankaBag"]=1073832997,prut_zeleza,prut_medi,prut_stribra,prut_zlata}
local trofeje = {["bankaBag"]=1073832997,vrrci_tlapka,skreti_tesak,zamotek_obriho_pavouka}
local lahve = {["bankaBag"]=1074226166,prazdna_lahev}
local ostatni = {["bankaBag"]=nil,obvaz,krvavy_obvaz}

local lootBagID = 1074601685
local loot = {zlatky,regy_obyc,regy_vzac,esence,lektvary,obyc_sipy_a_sipky,sipy_barevne,sipky_barevne,kuze,hadry,pirka,pruty,trofeje,lahve,ostatni}
ltr_lag_pause = 0


--MAIN LOOP
while true do wait (1)
if getkey("F1") then lootuj("mrtvoly",loot,lootBagID) until_key_lifted("F1") end
if getkey("F2") then lootuj("target",loot,lootBagID) until_key_lifted("F2") end
if getkey("F3") then lootuj("target", "vse",UO.CharID) until_key_lifted("F3") end
if getkey("F4") then roztrid(loot,lootBagID) until_key_lifted("F4") end
end