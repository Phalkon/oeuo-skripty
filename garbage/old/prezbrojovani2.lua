dofile('functions/findcol.lua')

local bagl_id = 1074354748
local zbroj_type = {{5399,0},{5054,0},{5055,0},{5062,961},{5076,0},{5078,0},{7947,0}}


function prezbrojeni(nahod_shod)
  if nahod_shod == 1 then
    UO.LObjectID = bagl_id
    UO.Macro(17,0)
    wait (200)
    local zbroj = 1
    repeat      
      local item = findcol(zbroj_type[zbroj][1],zbroj_type[zbroj][2])
      if item ~= nil then
        UO.LObjectID = item[1] 
        UO.Macro(17,0)
        wait (500)
      end
      zbroj = zbroj + 1
    until zbroj == 8
  end
  if nahod_shod == 2 then
  local zbroj = 7
  repeat      
    local item = findcol(zbroj_type[zbroj][1],zbroj_type[zbroj][2])
    if item ~= nil then
      UO.Drag(item[1])
      UO.DropC(bagl_id) 
      wait (500)
    end
    zbroj = zbroj - 1
    until zbroj == 0  
  end
end

while true do
  wait (1)
  if getkey("F8") then prezbrojeni(1) end
  if getkey("F9") then prezbrojeni(2) end
end