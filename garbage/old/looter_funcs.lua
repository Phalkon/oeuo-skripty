-- Functions for looter
-- by Phalkon

dofile('basic.lua')

--miniFunctions
function open_body(body) use_object(body) contas = body ignorelist[body] = 1 end
function loot_dragndrop(id,col,drop) UO.Drag(id,col) wait (100) UO.DropC(drop) wait (500) end 

--MAIN FUNCTION
function lootuj(kde,co,kam)
 if kde == "mrtvoly" then
  for i = -1,1 do 
   local telo = finditem("id",8198,"kind","contid",UO.CharPosX+i,UO.CharPosY,"z","stack","rep","col")
    if telo ~= nil and telo[7] < UO.CharPosZ + 10 and UO.CharPosZ -10 < telo[7] then open_body(telo[1]) break end
   local telo = finditem("id",8198,"kind","contid",UO.CharPosX,UO.CharPosY+i,"z","stack","rep","col")
    if telo ~= nil and telo[7] < UO.CharPosZ + 10 and UO.CharPosZ -10 < telo[7] then open_body(telo[1]) break end
   local telo = finditem("id",8198,"kind","contid",UO.CharPosX+i,UO.CharPosY+i,"z","stack","rep","col")
    if telo ~= nil and telo[7] < UO.CharPosZ + 10 and UO.CharPosZ -10 < telo[7] then open_body(telo[1]) break end
   local telo = finditem("id",8198,"kind","contid",UO.CharPosX+i,UO.CharPosY-i,"z","stack","rep","col")
    if telo ~= nil and telo[7] < UO.CharPosZ + 10 and UO.CharPosZ -10 < telo[7] then open_body(telo[1]) break end
  end
 end
 if kde == "target" then
  UO.TargCurs = true
  while UO.TargCurs == true do wait( 100) end
  use_object(UO.LTargetID)
  contas = UO.LTargetID
 end
 if co == "vse" then
  wait (200)
  repeat
    local for_loot = finditem("id","type","kind",contas,"x","y","z","stack","rep","col")
    if for_loot ~= nil then
      loot_dragndrop(for_loot[1],for_loot[8],kam)
    end 
  until for_loot == nil
 end
 if co ~= "vse" then
  wait (200)
  repeat
    for i1 = 1,#co do for i2 = 1,#co[i1] do
      local for_loot = finditem("id",co[i1][i2]["type"],"kind",contas,"x","y","z","stack","rep",co[i1][i2]["col"])
      if for_loot ~= nil then
        loot_dragndrop(for_loot[1],for_loot[8],kam)
      end
    end end
  until for_loot == nil
 end
end

function roztrid(co,odkud,otevri)
  use_object(odkud)
  if otevri ~= nil then for index_otvor = 1,#otevri do  wait(100) use_object(otevri[index_otvor]) end end
  wait (200)
  repeat
    for i1 = 1,#co do for i2 = 1,#co[i1] do
      local na_trideni = finditem("id",co[i1][i2]["type"],"kind",odkud,"x","y","z","stack","rep",co[i1][i2]["col"])
      if na_trideni ~= nil and co[i1]["bankaBag"] ~= nil then
        loot_dragndrop(na_trideni[1],na_trideni[8],co[i1]["bankaBag"])
      end
    end end
  until na_trideni == nil  
end