
    -- hotkeys_wait is being delcared here so it gets
    -- file "scope" instead of global scope.
     
local hotkeys_wait
local hotkeys -- function that checks key presses
local hid
local fn2
local fn3
     
hid = function() -- first hotkey function
  if UO.CharStatus == G then UO.Macro (6,0) end
  UO.Macro(13,21)
  UO.ExMsg(UO.CharID,3,0,"4")
  hotkeys_wait(500)
  UO.ExMsg(UO.CharID,3,0,"3")
  hotkeys_wait(500)
  UO.ExMsg(UO.CharID,3,0,"2")
  hotkeys_wait(500)
  UO.ExMsg(UO.CharID,3,0,"1")
  hotkeys_wait(500)
end
     
    fn2 = function() -- second hotkey function
            print("FN2 start")
            hotkeys_wait(500)
            print("FN2 end")
    end
     
    fn3 = function() -- etc...
            print("FN3 start")
            hotkeys_wait(500)
            print("FN3 end")
    end
     
    -- the timeout is just to keep it from "catching" hotkeys too
    -- fast IE: the TimeOUT before another hotkey will be checked
    -- is to give the human time to raise the finger
     
do
  local TimeOUT = 200
  local Timing = 100
  hotkeys = function()
    if getticks() < TimeOUT then return end
    if getkey("ALT") and getkey("q") then
      TimeOUT = getticks() + Timing
      hid()
    end
    if getkey(2) then
      TimeOUT = getticks() + Timing
      fn2()
    end
    if getkey(3) then
      TimeOUT = getticks() + Timing
      fn3()
    end
  end
end
     
hotkeys_wait = function(t)
  t = getticks() + t
  repeat
    wait(1)
    hotkeys()
  until getticks() >= t
end
     
repeat
  print("KEYLOOP")
  hotkeys_wait(1000)
until false
     
stop()