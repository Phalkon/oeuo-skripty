-- Pasti
-- by Phalkon

print("Loading....")
dofile('../functions/basic.lua')
dofile('../data/items.lua')
dofile('../functions/login.lua') if UO.CliLogged == false and UO.CliVer=="6.0.4.0" then login() end

local rezerva = 50 -- Kolk HP mus� chyb�t do pln�ho po�tu, aby za�al heal
local tOut1 = 10000 -- TimeOut pro pauzy (�eka�ky)
local tOut2 = 10000 -- TimeOut pro loop na zam��en� pasti
local tOut3 = 3000 -- TimeOut pro SysMsg skeny
local antilag = 20000 -- p�idan� pauza pro SysMsg sken po pohybu (zabra�uje smrti pro�lapov�n�m past� p�i laz�ch)
local buch = 10000 -- Pauza p�i spu�t�n� pasti
local lag = 100 -- Prodleva pro SysMsg skeny
-- pauza 1000 = pauza 1s

local norm = UO.GetSkill("remo") local skillnow = 0 local skill0 = norm
local BandsWaisted = 0
local SysMsgNil = UO.SysMsg
-- Strings
local string_buch = {"�","Dostala jsi zran�n� ohn�m","Dostala jsi zran�n� mrazem","Dostala jsi zran�n� energi�","Dostala jsi zran�n� v�buchem"}
local string_obvaz = {"Obvaz jsi musela zahodit - u~ se nedal pou~�t","Pokl�d� krvave obvazy do ba�ohu."}
-- Coords
local coords_to_north = {{6033,1392},{6032,1391},{6033,1390},{6032,1389},{6033,1388},{6030,1388},{6027,1388},{6028,1387},{6027,1386},{6028,1385},{6027,1384},{6028,1383},{6027,1382},{6027,1381},{6029,1381},{6031,1385},{6033,1385},{6035,1385},{6037,1385},{6037,1384},{6036,1383},{6037,1382},{6036,1381},{6039,1381},{6041,1381},{6043,1381},{6045,1381},{6047,1381},{6046,1390},{6044,1390},{6042,1390},{6042,1389},{6042,1388},{6042,1387},{6042,1386},{6040,1390}}
local coords_to_south = {{6031,1387},{6029,1387},{6028,1380},{6031,1380},{6030,1381},{6031,1382},{6030,1383},{6030,1384},{6032,1384},{6034,1384},{6036,1384},{6038,1380},{6040,1380},{6042,1380},{6044,1380},{6046,1380},{6048,1380},{6048,1381},{6047,1382},{6048,1383},{6047,1384},{6048,1385},{6047,1386},{6048,1387},{6047,1388},{6048,1389},{6045,1389},{6043,1389},{6041,1385},{6041,1386},{6041,1387},{6041,1388},{6041,1389},{6039,1389},{6039,1390}}
local coords_to_west = {{6033,1391},{6033,1389},{6033,1387},{6032,1387},{6031,1388},{6030,1387},{6029,1388},{6028,1388},{6028,1386},{6028,1384},{6028,1382},{6031,1381},{6031,1383},{6037,1383},{6037,1381},{6048,1382},{6048,1384},{6048,1386},{6048,1388},{6048,1390},{6047,1390},{6046,1389},{6045,1390},{6044,1389},{6043,1390},{6042,1385},{6041,1390},{6040,1389},{6039,1391}}
local coords_to_east = {{6032,1390},{6032,1388},{6027,1387},{6027,1385},{6027,1383},{6027,1380},{6028,1381},{6029,1380},{6030,1380},{6030,1382},{6030,1385},{6031,1384},{6032,1385},{6033,1384},{6034,1385},{6035,1384},{6036,1385},{6036,1382},{6036,1380},{6037,1380},{6038,1381},{6039,1380},{6040,1381},{6041,1380},{6042,1381},{6043,1380},{6044,1381},{6045,1380},{6046,1381},{6047,1380},{6047,1383},{6047,1385},{6047,1387},{6047,1389}}
-- miniFunctions
function check_SysMsg(string,brzda) local timesup = getticks() + brzda repeat wait(lag) if UO.SysMsg == string then return true end until timesup < getticks() return false end 
function checkIfLoggedIn() if UO.CliLogged == true then print("Skript nab�hl v po��dku") else print("CHYBA: Nepoda�ilo se nalogovat do hry!") stop() end end
function checkIfLoggedIn2() if UO.CliLogged == false then print("CHYBA: Clien spadl!") stop() end end
function SkriptMsg(string,col) UO.SysMessage("SkriptMsg! "..string,col) end
function zivoty() SkriptMsg("�ivoty: "..tostring(UO.Hits),35) end
function PrintStatus() print("Skill: "..tostring(norm/10).."%".." || Gain: "..tostring((norm-skill0)/10).."%".." || Sp�leno obvaz�: "..tostring(BandsWaisted)) end
function DejSiPauzu() logout_coutdown(5) if UO.CliLogged==false then print("Pauza") end repeat wait (100) until getkey("ctrl") and getkey("y") print("Konec Pauzy") login() checkIfLoggedIn() PrintStatus() end

-- FUNCTIONS
function jdi()
 for i = 1,#coords_to_north do if UO.CharPosX == coords_to_north[i][1] and UO.CharPosY == coords_to_north[i][2] then UO.Move (UO.CharPosX,UO.CharPosY - 1) return end end
 for i = 1,#coords_to_south do if UO.CharPosX == coords_to_south[i][1] and UO.CharPosY == coords_to_south[i][2] then UO.Move (UO.CharPosX,UO.CharPosY + 1) return end end
 for i = 1,#coords_to_west do if UO.CharPosX == coords_to_west[i][1] and UO.CharPosY == coords_to_west[i][2] then UO.Move (UO.CharPosX - 1,UO.CharPosY) return end end
 for i = 1,#coords_to_east do if UO.CharPosX == coords_to_east[i][1] and UO.CharPosY == coords_to_east[i][2] then UO.Move (UO.CharPosX + 1,UO.CharPosY) return end end
 if UO.CharPosX == 6038 and UO.CharPosY == 1391 then
   local pastacka = finditem(1074555364,"type","kind","contid",6036,1391,-38,"stack","rep","col")
   if pastacka ~= nil then
     use_object(pastacka[1])
   end 
 end
 UO.Move (6033,1394)
 UO.Move (6033,1392)
end

function zneskodni_past() 
  local timer = getticks() + tOut2    
  repeat
    local past = finditem("id",4113,"kind","contid",UO.CharPosX,UO.CharPosY,"z","stack","rep","col")
    if past ~= nil then
      UO.Macro(13,48) WaitForTarCurs()
      target_object(past[1],past[3])
      past_targeted = check_SysMsg("Za�ala jsi zne�kod�ovat past.",tOut3)
    end
    if past == nil then return end
  until past_targeted == true or timer < getticks()
  target_cancel("maybe") cekej(1,0)
end

function kdyz_bouchne()
  warmode_cancel() zivoty()
  local timer = getticks() + buch
  repeat heal() wait (100) until timer < getticks()
  zneskodni_past()
end

function heal()
 while UO.Hits < UO.MaxHits - rezerva do SkriptMsg("Spou�t�m heal",155)
   local bandage = finditem("id",obvaz["type"],"kind",UO.BackpackID,"x","y","z","stack","rep","col")
   if bandage == nil then
     while UO.Hits < UO.MaxHits - rezerva do
       for w = 1,10 do wait (500) if w==1 then SkriptMsg("Nem� band�e v batohu!",35) end end
     end
   end
   if bandage ~= nil then
     use_object(bandage[1]) WaitForTarCurs()      
     UO.Macro(23,0)
     local bebi_tOut = getticks() + tOut3
     local escape = 0
     repeat wait (lag)
       if UO.SysMsg:find("ses") then escape = 1 break end 
     until bebi_tOut < getticks()
     if escape == 1 then SkriptMsg("Band�e: "..tostring(bandage[8]-1),155) target_cancel("maybe") cekej(0,1) end
   end 
 end
end

function cekej(past,obvaz)
  local timer = getticks() + tOut1
  if past == 1 then
    while true do
      wait_until(getkey("ctrl") and getkey("y"),1,lag)
      if getkey("ctrl") and getkey("y") then DejSiPauzu() end
      if UO.SysMsg == "Uspesne jsi na chvili zneskodnila past" then return end
      if UO.SysMsg == "Nepodarilo se ti zneskodnit past" then zneskodni_past() end
      for i = 1,#string_buch do if UO.SysMsg == string_buch[i] then kdyz_bouchne() end end
      if timer < getticks() then return end
    end
  end
  if obvaz == 1 then
  healed = false
  repeat
   for cntdwn = 1,(2000/lag) do if cntdwn == 1 then zivoty() end wait (lag)
   for i = 1,#string_obvaz do if UO.SysMsg == string_obvaz[i] then if i==1 then BandsWaisted=BandsWaisted+1 PrintStatus() end healed = true end end
   if healed == true then break end end
  until healed == true or timer < getticks()
  zivoty() return
  end
end

-- MAIN LOOP
wait (5000)
checkIfLoggedIn() 
while norm ~= 1000 do
  checkIfLoggedIn2()
  norm = UO.GetSkill("remo") if norm ~= skillnow then skillnow = norm PrintStatus() end
  backpack_check()
  warmode_cancel()
  heal()
  jdi()
  local buba = getticks() + (tOut3 + antilag)
  local found_string_buba = 0
  repeat wait (lag)
   if UO.SysMsg == "NALEZENA PAST!" then found_string_buba = 1 break end
   for i = 1,#string_buch do if UO.SysMsg == string_buch[i] then found_string_buba = 2 break end end
   if (UO.CharPosX == 6033 and UO.CharPosY == 1392) or (UO.CharPosX == 6038 and UO.CharPosY == 1391) then break end 
  until found_string_buba == 2 or buba < getticks()
  if found_string_buba == 1 then zneskodni_past() end
  if found_string_buba == 2 then kdyz_bouchne() end
end
logout_coutdown(50)