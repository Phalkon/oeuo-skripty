    local ignorelist = {}
    finditem = function(typ, col)
      ncnt = UO.ScanItems(true)
      for nindex = 0,(ncnt-1) do
        local nid,ntype,nkind,ncontid,nx,ny,nz,nstack,nrep,ncol = UO.GetItem(nindex)
        if ntype == typ and ignorelist[nid] == nil then
          if col == nil or ( col ~= nil and col == ncol ) then
            return {nid,ntype,nkind,ncontid,nx,ny,nz,nstack,nrep,ncol}
          end
        end
      end
      return nil
    end