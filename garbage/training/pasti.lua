-- Pasti
-- by Phalkon

-- stiskni ctrl+y pro pauzu
-- UPOZORN�N�: pro vypnut� stiskni ctrl+u (znovu zapne zvuk)

print("Loading....")
dofile('../functions/basic.lua')
dofile('../functions/communicator.lua')
dofile('../data/items.lua')

-- initiate logging
local nYear,nMonth,nDay,nDayOfWeek = getdate ()
local nHour, nMinute, nSecond, nMillisec = gettime()
if nMinute<10 then nMinute = (0)..nMinute end
local log_start_msg = (nYear..('/')..nMonth..('/')..nDay..(' ')..nHour..(':')..nMinute..(' ')..'STARTing log')
function log(log_this_msg)
 local nHour, nMinute, nSecond, nMillisec = gettime() if nMinute<10 then nMinute = (0)..nMinute end
 local msg = (nHour..(':')..nMinute..(' || ')..log_this_msg)
 local file = openfile("pasti_log","a") if file then file:write(msg..'\n') file:close() print(msg) end
end
-- -------------------

if UO.CliCnt==0 then print("Neb�� ��dn� client! Startuju") cm_restart() cm_antiafk('CHYBA! RESTART!!!') stop() end
if UO.CliLogged == false then login() end

local rezerva = 50 -- Kolk HP mus� chyb�t do pln�ho po�tu, aby za�al heal
local tOut1 = 10000 -- TimeOut pro pauzy (�eka�ky)
local tOut2 = 10000 -- TimeOut pro loop na zam��en� pasti
local tOut3 = 3000 -- TimeOut pro SysMsg skeny
local antilag = 20000 -- p�idan� pauza pro SysMsg sken po pohybu (zabra�uje smrti pro�lapov�n�m past� p�i laz�ch)
local buch = 10000 -- Pauza p�i spu�t�n� pasti
local lag = 100 -- Prodleva pro SysMsg skeny
-- pauza 1000 = pauza 1s
local equip_fire = {1074504970,1074314729,1073949841,1073763036,1074824028,1074054804,1074752193}
local equip_cold = {1073946245,1074636771,1074745907,1074618312,1074686643,1074722207}
local equip_acid = {1074670390,1074643001,1074643867,1074594200,1074070747,1074069663,1074023909}
local equip_elec = {1074670303,1073949039,1074069540,1074626182,1074626207,1074624296,1074331744}

local norm = UO.GetSkill("remo") local skillnow = 0 local skill0 = norm
local BandsWaisted = 0
local cur_equip = 0
local SysMsgNil = UO.SysMsg
local kolecka = 0
-- Strings
local string_buch = {"�","Dostala jsi zran�n� ohn�m","Dostala jsi zran�n� mrazem","Dostala jsi zran�n� energi�","Dostala jsi zran�n� v�buchem"}
local string_obvaz = {"Obvaz jsi musela zahodit - u~ se nedal pou~�t","Pokl�d� krvave obvazy do ba�ohu."}
-- Coords
local coords_to_north = {{6032,1391},{6033,1390},{6032,1389},{6033,1388},{6030,1388},{6027,1388},{6028,1387},{6027,1386},{6028,1385},{6027,1384},{6028,1383},{6027,1382},{6027,1381},{6029,1381},{6031,1385},{6033,1385},{6035,1385},{6037,1385},{6037,1384},{6036,1383},{6037,1382},{6036,1381},{6039,1381},{6041,1381},{6043,1381},{6045,1381},{6047,1381},{6046,1390},{6044,1390},{6042,1390},{6042,1389},{6042,1388},{6042,1387},{6042,1386},{6040,1390}}
local coords_to_south = {{6031,1387},{6029,1387},{6028,1380},{6031,1380},{6030,1381},{6031,1382},{6030,1383},{6030,1384},{6032,1384},{6034,1384},{6036,1384},{6038,1380},{6040,1380},{6042,1380},{6044,1380},{6046,1380},{6048,1380},{6048,1381},{6047,1382},{6048,1383},{6047,1384},{6048,1385},{6047,1386},{6048,1387},{6047,1388},{6048,1389},{6045,1389},{6043,1389},{6041,1385},{6041,1386},{6041,1387},{6041,1388},{6041,1389},{6039,1389},{6039,1390}}
local coords_to_west = {{6033,1391},{6033,1389},{6033,1387},{6032,1387},{6031,1388},{6030,1387},{6029,1388},{6028,1388},{6028,1386},{6028,1384},{6028,1382},{6031,1381},{6031,1383},{6037,1383},{6037,1381},{6048,1382},{6048,1384},{6048,1386},{6048,1388},{6048,1390},{6047,1390},{6046,1389},{6045,1390},{6044,1389},{6043,1390},{6042,1385},{6041,1390},{6040,1389},{6039,1391}}
local coords_to_east = {{6032,1390},{6032,1388},{6027,1387},{6027,1385},{6027,1383},{6027,1380},{6028,1381},{6029,1380},{6030,1380},{6030,1382},{6030,1385},{6031,1384},{6032,1385},{6033,1384},{6034,1385},{6035,1384},{6036,1385},{6036,1382},{6036,1380},{6037,1380},{6038,1381},{6039,1380},{6040,1381},{6041,1380},{6042,1381},{6043,1380},{6044,1381},{6045,1380},{6046,1381},{6047,1380},{6047,1383},{6047,1385},{6047,1387},{6047,1389}}
local StarOrFinishCords = {["Start"]={6033,1392},["Finish"]={6038,1391}}

local next_past_ohen = {{6033,1392},{6033,1390},{6032,1388},{6030,1388},{6030,1387},{6029,1387},{6027,1388},{6027,1387},{6027,1385},{6028,1385},{6028,1384},{6027,1383},{6028,1383},{6027,1380},{6028,1380},{6028,1381},{6029,1381},{6031,1381},{6031,1383},{6030,1385},{6031,1385},{6033,1385},{6034,1384},{6035,1384},{6037,1384},{6036,1382},{6037,1380},{6039,1381},{6042,1380},{6044,1380},{6047,1381},{6047,1380},{6047,1383},{6047,1385},{6047,1387},{6048,1387},{6048,1389},{6045,1389},{6044,1389},{6041,1389},{6041,1390},{6039,1389}}
local next_past_mraz = {{6033,1387},{6032,1391},{6028,1388},{6028,1386},{6027,1386},{6027,1384},{6028,1382},{6027,1382},{6030,1380},{6030,1381},{6030,1382},{6030,1383},{6032,1384},{6033,1384},{6035,1385},{6037,1385},{6036,1383},{6037,1382},{6036,1380},{6038,1381},{6040,1380},{6041,1381},{6041,1380},{6042,1381},{6045,1381},{6046,1380},{6048,1381},{6048,1382},{6048,1383},{6047,1384},{6048,1385},{6047,1386},{6047,1388},{6046,1390},{6046,1389},{6045,1390},{6043,1390},{6042,1389},{6042,1388},{6042,1387},{6042,1386},{6042,1385},{6041,1385},{6041,1386},{6041,1387}}
local next_past_kyse = {{6033,1391},{6033,1389},{6033,1388},{6032,1387},{6031,1388},{6029,1388},{6027,1381},{6031,1380},{6031,1382},{6030,1384},{6031,1384},{6032,1385},{6034,1385},{6036,1384},{6036,1385},{6037,1383},{6037,1381},{6036,1381},{6038,1380},{6039,1380},{6040,1381},{6043,1381},{6043,1380},{6044,1381},{6045,1380},{6046,1381},{6048,1380},{6047,1382},{6048,1384},{6048,1386},{6048,1388},{6047,1389},{6048,1390},{6047,1390},{6044,1390},{6043,1389},{6042,1390},{6041,1388},{6040,1390},{6040,1389},{6039,1390}}
local next_past_elek = {{6032,1390},{6032,1389},{6031,1387},{6028,1387},{6029,1380}} 

-- miniFunctions
function check_SysMsg(string,brzda) local timesup = getticks() + brzda repeat wait(lag) if UO.SysMsg == string then return true end until timesup < getticks() return false end 
function checkIfLoggedIn() if UO.CliLogged == true then log('Skript nabehl v poradku') else log('CHYBA: Nepodarilo se nalogovat do hry! Pravdepodobne spadl klient') cm_restart() log('Klient restartovan, skript ceka na rucni restart') stop() end end
function checkIfLoggedIn2() if UO.CliCnt==0 then log('CHYBA: Clien spadl!') stop() elseif UO.CliLogged == false and UO.CliCnt>0 then log('Spadlo spojeni: RELOG') login() wait(5000) checkIfLoggedIn() PrintStatus() end end
function SkriptMsg(string,col) UO.SysMessage("SkriptMsg! "..string,col) end
function zivoty() SkriptMsg("�ivoty: "..tostring(UO.Hits),35) end
function PrintStatus() log("Skill: "..tostring(norm/10).."%".." || Gain: "..tostring((norm-skill0)/10).."%".." || Bands waisted: "..tostring(BandsWaisted).." || Rounds: "..tostring(kolecka)) end
function DejSiPauzu() log("Pauza") logout_coutdown(10) repeat wait (1000) until getkey("ctrl") and getkey("y") log("Konec Pauzy") login() checkIfLoggedIn() PrintStatus() end
function CheckStarOrFinishCords() if UO.CharPosX==StarOrFinishCords["Start"][1] and UO.CharPosY==StarOrFinishCords["Start"][2] then return("Start") elseif UO.CharPosX==StarOrFinishCords["Finish"][1] and UO.CharPosY==StarOrFinishCords["Finish"][2] then return("Finish") end return nil end
function prezbrojeni(zbroj) for i = 1,#zbroj do use_object(zbroj[i]) wait (1000) end end

-- FUNCTIONS
function equip(to_equip)
 if cur_equip ~= to_equip then
  if to_equip == 'ohen' then prezbrojeni(equip_fire) cur_equip='ohen' end
  if to_equip == 'mraz' then prezbrojeni(equip_cold) cur_equip='mraz' end
  if to_equip == 'kyse' then prezbrojeni(equip_acid) cur_equip='kyse' end
  if to_equip == 'elek' then prezbrojeni(equip_elec) cur_equip='elek' end
 end
end

function jdi()
-- co je dalsi past?
 for i = 1,#next_past_ohen do if UO.CharPosX == next_past_ohen[i][1] and UO.CharPosY == next_past_ohen[i][2] then SkriptMsg("PAST - OHEN",54) equip('ohen') end end
 for i = 1,#next_past_mraz do if UO.CharPosX == next_past_mraz[i][1] and UO.CharPosY == next_past_mraz[i][2] then SkriptMsg("PAST - MRAZ",54) equip('mraz') end end
 for i = 1,#next_past_kyse do if UO.CharPosX == next_past_kyse[i][1] and UO.CharPosY == next_past_kyse[i][2] then SkriptMsg("PAST - KYSELINA",54) equip('kyse') end end
 for i = 1,#next_past_elek do if UO.CharPosX == next_past_elek[i][1] and UO.CharPosY == next_past_elek[i][2] then SkriptMsg("PAST - ELEKTRINA",54) equip('elek') end end
-- -----------------
 for i = 1,#coords_to_north do if UO.CharPosX == coords_to_north[i][1] and UO.CharPosY == coords_to_north[i][2] then UO.Move (UO.CharPosX,UO.CharPosY - 1) return end end
 for i = 1,#coords_to_south do if UO.CharPosX == coords_to_south[i][1] and UO.CharPosY == coords_to_south[i][2] then UO.Move (UO.CharPosX,UO.CharPosY + 1) return end end
 for i = 1,#coords_to_west do if UO.CharPosX == coords_to_west[i][1] and UO.CharPosY == coords_to_west[i][2] then UO.Move (UO.CharPosX - 1,UO.CharPosY) return end end
 for i = 1,#coords_to_east do if UO.CharPosX == coords_to_east[i][1] and UO.CharPosY == coords_to_east[i][2] then UO.Move (UO.CharPosX + 1,UO.CharPosY) return end end
 local StarOrFinish = CheckStarOrFinishCords()
  if StarOrFinish == "Start" then hunger_check() UO.Move(6033,1391) return
  elseif StarOrFinish == "Finish" then kolecka=kolecka+1 PrintStatus()
   local pastacka = finditem(1074555364,"type","kind","contid",6036,1391,-38,"stack","rep","col")
   if pastacka ~= nil then
     local timer = getticks() + tOut2
     repeat use_object(pastacka[1]) wait (1000)
      local journal_check = scanjournal("P�ka pas�a�ka: Cvak!",10)
      if journal_check == true then break end
     until timer < getticks() or UO.CliLogged==false
   end 
  end
 UO.Move (6033,1394)
 UO.Move (6033,1392)
end

function zneskodni_past()
  UO.LObjectID = UO.BackpackID UO.Macro(17,0) -- resets ContID for Popup check
  local timer = getticks() + tOut2     
  local past_targeted = false
  repeat
    local past = finditem("id",4113,"kind","contid",UO.CharPosX,UO.CharPosY,"z","stack","rep","col")
    if past ~= nil then
      UO.Popup(past[1],0,0)
-- wait for popup ---
      local time_to_wait = getticks() + 1000
      repeat wait (10) until UO.ContID == 0 or time_to_wait < getticks()
---------------------
      UO.Click(50,40,true,true,true,false)
      past_targeted = check_SysMsg("Zacala jsi zneskodnovat past.",tOut3)
    end
    if past == nil then wait (500) end
--    if timer < getticks() then cm_restart() cm_antiafk('CHYBA! RESTART!!!') stop() end
  until past_targeted == true or timer < getticks() or UO.CliLogged==false
 cekej(1,0)
--   if past_targeted == true then cekej(1,0) else log('Spadlo spojeni: RELOG') logout_coutdown(10) wait(5000) login() wait(5000) checkIfLoggedIn() PrintStatus() end
end

--function zneskodni_past() 
--  local timer = getticks() + tOut2    
--  repeat
--    local past = finditem("id",4113,"kind","contid",UO.CharPosX,UO.CharPosY,"z","stack","rep","col")
--    if past ~= nil then
--      UO.Macro(13,48) WaitForTarCurs()
--      target_object(past[1],past[3])
--      past_targeted = check_SysMsg("Za�ala jsi zne�kod�ovat past.",tOut3)
--    end
--    if past == nil then wait (500) end
--  until past_targeted == true or timer < getticks()
--  target_cancel("maybe") cekej(1,0)
--end

function kdyz_bouchne()
  wait(100) zivoty() if not UO.CharStatus:find("H") then warmode_cancel() UO.Macro(13,21) end
  local timer = getticks() + buch
  repeat heal() wait (100) until timer < getticks() or UO.CliLogged==false
  zneskodni_past()
end

function heal()
 while UO.Hits < UO.MaxHits - rezerva and UO.CliLogged==true do SkriptMsg("Spou�t�m heal",155)
   local bandage = finditem("id",obvaz["type"],"kind",UO.BackpackID,"x","y","z","stack","rep","col")
   if bandage == nil then
     while UO.Hits < UO.MaxHits - rezerva do
       for w = 1,10 do wait (500) if w==1 then SkriptMsg("Nem� band�e v batohu!",35) end end
     end
   end
   if bandage ~= nil then
     use_object(bandage[1]) WaitForTarCurs()      
     UO.Macro(23,0)
     local bebi_tOut = getticks() + tOut3
     local escape = 0
     repeat wait (lag)
       if UO.SysMsg:find("ses") then escape = 1 break end 
     until bebi_tOut < getticks()
     if escape == 1 then SkriptMsg("Band�e: "..tostring(bandage[8]-1),155) target_cancel("maybe") cekej(0,1) end
   end 
 end
end

function cekej(past,obvaz)
  local timer = getticks() + tOut1
  if past == 1 then
    if not UO.CharStatus:find("H") then warmode_cancel() UO.Macro(13,21) end
    while UO.CliLogged==true do
      wait_until(getkey("ctrl") and (getkey("y") or getkey("u")),1,lag)
      if getkey("ctrl") and getkey("y") then DejSiPauzu() end
      if getkey("ctrl") and getkey("u") then toggle_sound() UO.SysMessage("SKRIPT VYPNUT!",155) log("SKRIPT VYPNUT!") stop() end
      if UO.SysMsg == "Uspesne jsi na chvili zneskodnila past" then return end
      if UO.SysMsg == "Nepodarilo se ti zneskodnit past" then zneskodni_past() end
      for i = 1,#string_buch do if UO.SysMsg == string_buch[i] then kdyz_bouchne() end end
      if timer < getticks() then return end
    end
  end
  if obvaz == 1 then
  healed = false
  repeat
   for cntdwn = 1,(2000/lag) do if cntdwn == 1 then zivoty() end wait (lag)
   for i = 1,#string_obvaz do if UO.SysMsg == string_obvaz[i] then if i==1 then BandsWaisted=BandsWaisted+1 PrintStatus() end healed = true end end
   if healed == true then break end end
  until healed == true or timer < getticks() or UO.CliLogged==false
  zivoty() return
  end
end

function hunger_check()
  UO.Macro(1,0,".hungry")
  wait (1000)
  local journal_check = scanjournal(UO.CharName..": Vypad� �e m� hlad",10)
   if journal_check == true then
    local bumbani = finditem("id",lektvar_nasyceni["type"],"kind",UO.BackpackID,"x","y","z","stack","rep","col")
    if bumbani == nil then SkriptMsg("Nem� lektvary nasycen� v batohu!",35) end
    if bumbani ~= nil then use_object(bumbani[1]) end 
   end
end

-- MAIN LOOP
local file = openfile("pasti_log","a")
if file then
 file:write('\n'..log_start_msg..'\n') file:close() print(log_start_msg)
end
wait (5000)
checkIfLoggedIn()
toggle_sound()
print ('ctrl+u - vypnout   ||   ctrl+y - pauza') 
while norm ~= 1000 do
  checkIfLoggedIn2()
  norm = UO.GetSkill("remo") if norm ~= skillnow then skillnow = norm PrintStatus() end
  backpack_check()
  warmode_cancel()
  heal()
  jdi()
  local buba = getticks() + (tOut3 + antilag)
  local found_string_buba = 0
  repeat wait (lag)
   if UO.SysMsg == "NALEZENA PAST!" then found_string_buba = 1 break end
   for i = 1,#string_buch do if UO.SysMsg == string_buch[i] then found_string_buba = 2 break end end
   local StarOrFinish = CheckStarOrFinishCords()
   if StarOrFinish ~= nil then break end 
  until found_string_buba == 2 or buba < getticks()
  if found_string_buba == 1 then zneskodni_past() end
  if found_string_buba == 2 then kdyz_bouchne() end
end
logout_coutdown(50)