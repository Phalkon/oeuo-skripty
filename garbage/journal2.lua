dofile('../functions/journal.lua')
local create = openfile("../data/journal","w+")
if create then create:write('STARTing log file\n')
create:close() end
local file = openfile("../data/journal","a")

ignore = {
-- hid
"|946|Lilien: Dob�e se tu ukr�v�|",
"|946|Odhalili t�!|",
-- pasti
"|50|NALEZENA PAST!|",
--"|946|Zacala jsi zneskodnovat past.|",
"|32|Past spustila|",
"|32|Nepodarilo se ti zneskodnit past|",
"|68|Uspesne jsi na chvili zneskodnila past|",
"|38|Zprava: prerusila jsi zneskodnovani pasti.|",
-- you see:
"|99|You see: Burl|",
"|99|You see: Adrian|",
"|99|You see: Kenan|",
"|2882|You see: Jagar Tharn|",
"|38|You see: sliz|",
"|38|You see: obri krysa|",
"|946|You see: krysa|",
"|995|You see: stena sklepu|",
"|995|You see: kamenne dlazdice|",
-- heal
"|1904|Vyber c�l: Na koho ",
"|1259|za",
"|93|�spch pYi l�",
"|93|Vyl�",
"|1356|Obvaz jsi musela zahodit - ",
"|946|Pokl�d� krvave obvazy do ba�ohu.|",
-- zraneni
"|1269|�ste",
"|40|�ste",
"|1266|�ste",
"|1378|�ste",
"|946|Dostala jsi zran�n� ohn�m|",
"|946|Dostala jsi zran�n� mrazem|",
"|946|Dostala jsi zran�n� v�buchem|",
"|946|Dostala jsi zran�n� energi�|",
-- save
"|54|Save sv�ta za 10 vte�in|",
"|54|Sv�t se na chv�li zastavil...|",
"|54|Sv�t se zase za�al to�it|",
-- SkriptMsg
"|35|SkriptMsg! �ivoty: ",
"|155|SkriptMsg! Spou�t�m heal|",
"|155|SkriptMsg! Band�e: ",
-- ostatni
"|946|P�ka pas�a�ka: Cvak!|",
"|946|Lilien: Vypad� �e"
--"SkriptMsg!",
--"Dostala jsi zran�n� ",
--" truhlu*",
--" odolnost: "
}
if file then
j = journal:new()
while true do
  if j:next() ~= nil then
   local msg=j:last()
   for i=1,#ignore do if msg:find(ignore[i]) then msg=nil break end end
    if msg~=nil then local file = openfile("journal","a") file:write(msg..'\n') file:close() end
  end
  wait(50)
end
file:close()
end