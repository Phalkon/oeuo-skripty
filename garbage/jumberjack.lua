dofile('../functions/FindItems.lua')
dofile('../functions/journal.lua')

local minimal_pause=500
local konik=1079143
local konik_bag=1074820674

function use_object(UObjectID) UO.LObjectID = UObjectID UO.Macro(17,0) end
function WaitForTarCurs(for_how_long) if for_how_long == nil then for_how_long = 500 end
local time_to_run = getticks() + for_how_long
repeat wait (100) until UO.TargCurs==true or time_to_run < getticks() end
function dragndrop(id,stack,drop) UO.Drag(id,stack) time_to_run=getticks()+200 repeat wait(1) wait(minimal_pause-1) until UO.LLiftedID==id or time_to_run<getticks() UO.DropC(drop) end

function novej_strom() 
 local sekyrka = ScanItems(true,{Type=3913,ContID=UO.CharID})
 if sekyrka[1]~=nil then use_object(sekyrka[1].ID) end
 WaitForTarCurs(3000)
 UO.SysMessage('Naklikni novej strom',5555)
 repeat wait (6000) until UO.TargCurs==false
end

function polena()
 if UO.ContID~=konik_bag then use_object(konik) repeat wait(500) until UO.ContID==konik_bag end
 local drivi = ScanItems(true,{Type=7133,ContID=UO.BackpackID})
 if drivi[1]~=nil then dragndrop(drivi[1].ID,drivi[1].Stack,konik_bag) end
end

function rubej()
 while true do
 local sekyrka = ScanItems(true,{Type=3913,ContID=UO.CharID})
 if sekyrka[1]~=nil then use_object(sekyrka[1].ID) end
 WaitForTarCurs(3000)
 UO.Macro(22,0)
 j = journal:new()
 local time_to_run = getticks() + 8000
 repeat wait(50)
 if UO.SysMsg=='Nezbylo tu nic k pok�cen�.' then return end
 if UO.SysMsg=='Pokl�d� polena do ba�ohu.' then polena() break end
 until j:next()=='|946|Lilien: Chvilku sek� do stromu, ale ��dn� pou�iteln� d�evo nez�sk�v�|' or time_to_run < getticks()
 end
end

while true do
novej_strom()
rubej()
end
