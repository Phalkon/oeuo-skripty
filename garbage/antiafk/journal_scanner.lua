dofile('../../functions/journal.lua')
dofile('../../functions/communicator.lua')
--local create = openfile("journal_log","w+")
--local communicator = "../.communicator/communicator"

local nYear,nMonth,nDay,nDayOfWeek = getdate ()
local nHour, nMinute, nSecond, nMillisec = gettime()
if nMinute<10 then nMinute = (0)..nMinute end
local log_start_msg = (nYear..('/')..nMonth..('/')..nDay..(' ')..nHour..(':')..nMinute..(' ')..'STARTing log file')
if create then
create:write(log_start_msg..'\n')
create:close() end
function log(log_this_msg) cm_antiafk(log_this_msg) print(log_this_msg) end
--function log(log_this_msg) local file = openfile(communicator,"a") if file then file:write('ANTIAFK!'..'\n') file:close() print(log_this_msg) end end


ignore = {
'LOW HITPOINTS!!!  0', -- logout 
'|54|Kter�ho tvora m',
'|1925|You see: Paracelsus je neskute�n� siln� a vypad� trochu neohraban�.|',
'|1925|Awa: Awa je pr�m�rn� siln� a vypad� velmi obratn�.|',
'|1925|You see: Atheron je neskute�n� siln� a vypad� bleskurychle.|',
'|88|Your skill in Anatomie has increased',
'|946|Mus� po�kat na dokon�en� jin� akce.|',
'|5555|Pauza nav��ena|',
'|5555|Pauza sn',
'|38|You see: bezhlavy|',
'|0|It begins to rain.|',
-- provokacka
"|946|Targetted item is gone?|",
"|946||",
"Targetted item moved?",
"|0|Peacemode activated|",
"|0|Peacemode deactivated|",
"|34|You see: *kr",
"|34|You see: *ob",
"|34|obri zaba: *",
"|0|Skript spu�t�n|",
"|0|Vyber target 1|",
"|0|Vyber target 2|",
"|1904|Vyber c�l: Koho chce",
"|946|You see: kralik|",
"|1268|Povedlo se ti vyprovokovat kralik na kralik|",
"|1268|Povedlo se ti vyprovokovat obri zaba na obri zaba|",
"|237|kralik: *",
"|237|obri zaba: *",
"|1356|Jeat nemo~ea hr�t...|",
"|1356|Hrajea apatn|",
"|1356|Nepovedlo se ti vyprovokovat kralik|",
"|1356|Nepovedlo se ti vyprovokovat obri zaba|",
"|1356|PYeruaila jsi provokaci.|",
"|946|Jsi",
-- jedy
"|1925|P�eru�ila jsi nan�en� jedu.|",
"|54|Jaky jed chces pouzit?|",
"|946|Unexpected target info|",
"|946|Na co chce",
"|68|�sp�n� jsi pou�ila jed na",
"|0| |",
"|1925|Neusp�la jsi p�i pokusu otr�vit",
-- hid
"|946|Lilien: Dob�e se tu ukr�v�|",
"|946|Odhalili t�!|",
"|946|Lilien: Zde se ukr",
-- pasti
"|50|NALEZENA PAST!|",
"|946|Zacala jsi zneskodnovat past.|",
"|32|Past spustila|",
"|32|Nepodarilo se ti zneskodnit past|",
"|68|Uspesne jsi na chvili zneskodnila past|",
"|38|Zprava: prerusila jsi zneskodnovani pasti.|",
"|38|Tve nehmotne ruce prochazeji skrz past|",
"|946|Nedos�hne� na to.",
'|155|Logout countdown: ',
-- you see:
"|99|You see: Burl|",
"|99|You see: Adrian|",
"|99|You see: Kenan|",
"|2882|You see: Jagar Tharn|",
"|38|You see: sliz|",
"|38|You see: obri krysa|",
"|946|You see: krysa|",
"|995|You see: stena sklepu|",
"|995|You see: kamenne dlazdice|",
"|995|You see: klenba|",
-- heal
"|1904|Vyber c�l: Na koho ",
"|1259|za",
"|93|�spch pYi l�",
"|93|Vyl�",
"|1356|Obvaz jsi musela zahodit - ",
"|946|Pokl�d� krvave obvazy do ba�ohu.|",
-- zraneni
"|1269|�ste",
"|40|�ste",
"|1266|�ste",
"|1378|�ste",
"|946|Dostala jsi zran�n� ohn�m|",
"|946|Dostala jsi zran�n� mrazem|",
"|946|Dostala jsi zran�n� v�buchem|",
"|946|Dostala jsi zran�n� energi�|",
-- save
"|54|Save sv�ta za 10 vte�in|",
"|54|Sv�t se na chv�li zastavil...|",
"|54|Sv�t se zase za�al to�it|",
'|946|Server byl POZASTAVEN kvuli Resyncu|',
'|946|Resync dokoncen!|',
-- SkriptMsg
"|35|SkriptMsg! �ivoty: ",
"|155|SkriptMsg! Spou�t�m heal|",
"|155|SkriptMsg! Band�e: ",
'|54|SkriptMsg! PAST - ',
-- prezbrojovani
'|17|Selh�n� kouzla:',
'|946|Nosenim zbroje z medi si ubiras na respektu|',
'|68|Ochrana ',
'|1925|Ochrana ',
'|17|Ochrana p�ed zran�n�m zv���:',
'|946|Po sundani medene zbroje se na tebe lide hned divaji jinak|',
-- login
'|55|Login confirm on Andaria|',
'|0|Welcome to Ultima Online!|',
'|946|Ztrac� pocit naprost�ho bezpe��|',
'|1259|Vstupujete na cvi',
'|1259|M�a nezaplacen� dluhy na dan�ch - ',
'|53|Sphere Version ',
'|946|Na Andarii je je�t� ',
'|946|Last logged: ',
-- ostatni
"|946|Zam��ov�n� zru�eno.|",
"|946|Pokl�d� prazdne lahve do ba�ohu.|",
'|1925|Lahvi�ka se p�i manipulaci po�kodila, mus� ji vyhodit...|',
"|946|P�ka pas�a�ka: Cvak!|",
"|946|Lilien: Vypad� �e",
": Hej, krok zpatky.|",
": Nechci problemy, ale: Krok zpet!|",
": Nestujte tak blizko.|",
' kousek ode ',
'|946|Pokl�d�',
'|946|Ty - m� hlad|',
" truhlu*",
}
j = journal:new()
log(log_start_msg)
while true do
  if UO.Hits<40 and UO.CliLogged == true then 
   local nHour, nMinute, nSecond, nMillisec = gettime() if nMinute<10 then nMinute = (0)..nMinute end
   local hp_alert = (nHour..(':')..nMinute..(' ').."LOW HITPOINTS!!!" .."  ".. UO.Hits)
   log(hp_alert)
  wait(2000)
  end
  if j:next() ~= nil then
   local msg=j:last()
   for i=1,#ignore do if msg:find(ignore[i]) then msg=nil break end end
   if msg~=nil then
    local nHour, nMinute, nSecond, nMillisec = gettime() if nMinute<10 then nMinute = (0)..nMinute end
    local journal_msg = (nHour..(':')..nMinute..(' || ')..msg)
    log(journal_msg)
   end
  end
  wait(50)
end