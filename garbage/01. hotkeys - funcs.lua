----- planovani fci
-- zautomatizovat otevirani paperdollu a backpacku (otevrit automaticky, kdyz nejsou otevreny) // [blbe se kontroluje otevreni, zatim neprakticky]
-- p�epsat fci ze staryho lootera na trideni
-- presouvani buff listy // [mozna nepujde]
-- novy targetovaci system // [bude potreba databaze NPC]
-- p�ed�lat looter... hledat itemy na b�zi NAME nam�sto TYPE (every item 2 values, singular+plural)


dofile('../functions/communicator.lua')
dofile('../functions/FindItems.lua')
dofile('../functions/FluentUO.lua')
dofile('../functions/journal.lua')
---------------
---- VARS -----
---------------
local minimal_pause = 500
local loop_wait=50

local bagl_id = 1074563268 -- druhy bagl na otevreni, krome BACKPACK
local lootBagID = 1073907465 
local paradyka_blessed = 1074719762
--local jezditko = {['name']='Snehova Boure',['id_velky']=1804,['typ']=bila_slechta,['kul_id']=1074796822}
--local jezditko = {['name']='bahenni drak',['id_velky']=776591,['typ']=bahenni_drak,['kul_id']=1074825074}
local jezditko = {['name']='Duch',['id_velky']=985329,['typ']=bila_slechta,['kul_id']=1074783463}
--local jezditko = {['name']='Stinova vrazedkyne',['id_velky']=529273,['typ']=cerny_valec,['kul_id']=1073965798}
--local jezditko = {['name']='Elinor',['id_velky']=687518,['typ']=bila_slechta,['kul_id']=1074209795}
--local pet = {['name']='medved grizzly',['id_velky']=414129,['typ']=medved_grizzly}
local pet = {['name']='Niklaus Manuel',['id_velky']=1046285,['typ']=medved_grizzly}
--local pet = {['name']='Adelaida',['id_velky']=898875,['typ']=obri_had}
--local pet = {['name']='Sycak',['id_velky']=902190,['typ']=obri_had}
local weapon_1H = {kratke_kopi['type'][1],kratke_kopi['type'][2],kris['type'][1],kris['type'][2],dyka['type'][1],dyka['type'][2]}
local kuse = {tezka_kuse['type'][1],tezka_kuse['type'][2],}
local luk = {skladany_luk['type'][1],elfi_luk['type'],skladany_luk['type'][2]}
local stit = {kruhovy_kovovy_stit['type'],dreveny_stit['type']}
local zbrane_poison = {kopi['type'][1],kopi['type'][2],kratke_kopi['type'][1],kratke_kopi['type'][2],kris['type'][1],kris['type'][2],dyka['type'][1],dyka['type'][2]}
local zbrane_kuchani = {kopi['type'][1],kopi['type'][2],kratke_kopi['type'][1],kratke_kopi['type'][2],kris['type'][1],kris['type'][2],dyka['type'][1],dyka['type'][2]}
local equip_types = {5397,5907,5067,7174,5905,5062,15193,7947,15018}
-- looter ----------------------------------------------------------------------------------------------
local zlatky    = {['storeCont']=nil,zlatak,stribrnak,medak}
local regy_obyc = {['storeCont']=1074225859,mandragora,zensen,rulik,sirny_prach,krvavy_mech,pavucina,kost,cerna_perla,katova_kukla,cerny_vres,cesnek}
local regy_vzac = {['storeCont']=1073832617,krvava_kost,cerna_kost,kost_demona,netopyri_kridlo,krystal_nox,urodna_prst,pemza,suche_drevo,prach_z_hrobu,obsidian,krev_demona,draci_krev,draci_srdce,slizke_oko,krvave_jikry}
local esence    = {['storeCont']=1074697417,esence_ohne,esence_vzduchu,esence_slizu,esence_zeme,esence_vody}
local lektvary  = {['storeCont']=1074366556,slabsi_lektvar_hbitosti,slabsi_lektvar_sily,slabsi_lektvar_moudrosti,lektvar_svetla,slabsi_lecivy_lektvar,slabsi_protijed,slabsi_lektvar_osvezeni,slabsi_lektvar_obnovy,slabsi_vybusny_lektvar,slabsi_jed,lektvar_tranzu,lektvar_doplneni_many}
local sipy      = {['storeCont']=1074563299,sip,elfi_sip,smrtici_sip,ohnivy_sip,stribrny_sip,zlaty_sip}
local sipky     = {['storeCont']=1074563250,sipka,drtiva_sipka,ohniva_sipka,stribrna_sipka,zlata_sipka}
local kuze      = {['storeCont']=1073746071,hromada_kuzi,hromada_vlcich_kuzi,hromada_medvedich_kuzi,rozstrihana_kuze,rozstrihana_vlci_kuze,rozstrihana_medvedi_kuze}
local hadry     = {['storeCont']=nil,dublet,serpa,roba,kosile,zdobena_kosile,obycejne_saty,dlouhe_kalhoty,kratke_kalhoty,tunika,zdobene_saty,kilt}
local pirka     = {['storeCont']=1074697417,pirko,orli_pirko,havrani_pirko}
local pruty     = {['storeCont']=1073832997,prut_zeleza,prut_medi,prut_stribra,prut_zlata}
local trofeje   = {['storeCont']=nil,vrrci_tlapka,skreti_tesak,zamotek_obriho_pavouka,sklipkani_zamotek,gorili_mozek}
local lahve     = {['storeCont']=1074226166,prazdna_lahev}
local strivka   = {['storeCont']=1073832997,strevo} 
local ostatni   = {['storeCont']=nil,obvaz,krvavy_obvaz,svitek_prvni_stupen,sadlo}  --hulka

local lootit = {zlatky,regy_obyc,regy_vzac,esence,lektvary,hadry,pirka,pruty,trofeje,kuze,strivka,ostatni,prazdna_lahev}
ltr_lag_pause = 0

local looter_items = {
['zlatky']    = {['storeCont']=nil,'Zla��k ','Zla��ky ','St��br��k ','St��br��ky ','M���k ','M���ky '},
['regy_obyc'] = {['storeCont']=1074225859,},
['regy_vzac'] = {['storeCont']=1073832617,},
['esence']    = {['storeCont']=1074697417,},
['lektvary']  = {['storeCont']=1074366556,},
['sipy']      = {['storeCont']=1074563299,},
['sipky']     = {['storeCont']=1074563250,},
['kuze']      = {['storeCont']=1073746071,},
['hadry']     = {['storeCont']=nil,},
['pirka']     = {['storeCont']=1074697417,},
['pruty']     = {['storeCont']=1073832997,},
['trofeje']   = {['storeCont']=nil,},
['lahve']     = {['storeCont']=1074226166,},
['strivka']   = {['storeCont']=1073832997,},
['ostatni']   = {['storeCont']=nil,},
}
--------------------------------------------------------------------------------------------------------
local lastequipedID=0
local lastequipedTYPE=0
 local mounti={16031,16032,16033,16034,16061}
local krvko='deactivated'
local autorun='deactivated' UO.Macro(32,0) wait(100) if UO.SysMsg=='Always Run is now on.' then UO.Macro(32,0) end
-- timery --
local potka_timer=getticks()
local krvko_timer=getticks()-30000
local srazeni_timer=getticks()-3000
local port_timer=getticks()-3000
local volani_kone=getticks()-3000
local wine_timer=getticks()-3000
------------
local warmode_alert=getticks()-10000
local status_prodleva=getticks()-1000 -- zabranuje nekolikanasobnemu vytazeni statusu
local tarcurs_remover={}
local gamma_prepinac={}
local uoam_prepinac={}

local potion_timer = {
['GH']       =  8000,
['obnova']   = 11000,
['protijed'] =  6500, -- 6600??
['ocelka']   =  8500,
['sila']     =  4000,
['hbitost']  =  4000, -- 4100??
['osvezko']  =  7000,
['dispell']  = 10000,
['invisko']  = 10000,
['nezranko'] = 30000,
['inko']     = 15000,
} 

local status = {['Hits']=UO.Hits,['Stamina']=UO.Stamina,['EnemyHits']=UO.EnemyHits,['EnemyID']=UO.EnemyID,
                ['MinDmg']=UO.MinDmg,['MaxDmg']=UO.MaxDmg,['Weight']=UO.Weight,['MaxWeight']=UO.MaxWeight,
                ['AR']=UO.AR,['FR']=UO.FR,['CR']=UO.CR,['PR']=UO.PR,['ER']=UO.ER}
local status_msg_wait = getticks()
local status_msg_pauza=100 -- prodleva mezi status zpravami

-- looter ----------------------------------------------------------------------------------------------
local loot_pause=getticks()-500
local dontloot = {8197,8252}
local vylooceno={}
local loot_table={}
local lootovani=0
local typlootu=0  
--drop all values from nested tables in 'lootit' in one table 'lootit2'
local lootit2={}
for i1=1,#lootit do for i2=1,#lootit[i1] do
 if type(lootit[i1][i2]['type']) == 'table' then
  for i3=1,#lootit[i1][i2]['type'] do table.insert(lootit2,lootit[i1][i2]['type'][i3]) end
 else table.insert(lootit2,lootit[i1][i2]['type']) end
end end

--remove duplicate values from 'lootit2' and create table 'loot'
local hash = {}
local loot = {}
for _,v in ipairs(lootit2) do
   if (not hash[v]) then
       loot[#loot+1] = v
       hash[v] = true
   end
end
--------------------------------------------------------------------------------------------------------
local string = {
['potka1']='Pokl�d� prazdne lahve do ba�ohu.',
['potka2']='Lahvi�ka se p�i manipulaci po�kodila, mus� ji vyhodit...',
['lenost']='Nemas ani trosku chut delat neco takoveho',
['cannot']='Nen� mo�n�, abys to dok�zala pr�v� te�.',
['krvko']='Tv� �toky jsou te mnohem bolestivja�',
['srazeni']='PYipravujea se na sra~en� z kon',
['mount too far']='Nedos�hne� na tu bytost.',
['mount timer']='Takhle rychle po sesednut� nasednout nem��e�',
}
--------------------------------------------------------------------------------------------------------
local skilly={
  ['Bojov�']={
    {['name']='Boj beze zbran�',        ['value']={UO.GetSkill('Wres')}}, -- Wrestling
    {['name']='Boj bodn�mi zbran�mi',   ['value']={UO.GetSkill('Fenc')}}, -- Fencing
    {['name']='Boj drtiv�mi zbran�mi',  ['value']={UO.GetSkill('Mace')}}, -- Mace Fighting
    {['name']='Boj se�n�mi zbran�mi',   ['value']={UO.GetSkill('Swor')}}, -- Swordsmanship
    {['name']='Kryt �t�tem',            ['value']={UO.GetSkill('Parr')}}, -- Parrying
    {['name']='Lukost�elba',            ['value']={UO.GetSkill('Arch')}}, -- Archery
    {['name']='Taktika',                ['value']={UO.GetSkill('Tact')}}, -- Tactics
    {['name']='Znalost zbran�',         ['value']={UO.GetSkill('Arms')}}, -- Arms Lore
  },
  ['V�robn�']={
    {['name']='D�evorubectv�',          ['value']={UO.GetSkill('Lumb')}}, -- Lumberjacking
    {['name']='Hornictv�',              ['value']={UO.GetSkill('Mini')}}, -- Mining
    {['name']='Jemn� mechanika',        ['value']={UO.GetSkill('Tink')}}, -- Tinkering
    {['name']='Kov��stv�',              ['value']={UO.GetSkill('Blac')}}, -- Blacksmithy
    {['name']='K�ej�ovstv�',            ['value']={UO.GetSkill('Tail')}}, -- Tailoring
    {['name']='�ezb��stv�',             ['value']={UO.GetSkill('Bowc')}}, -- Bowcraft Fletching
    {['name']='Tesa�stv�',              ['value']={UO.GetSkill('Carp')}}, -- Carpentry
  },
  ['Magick�']={
    {['name']='Alchymie',               ['value']={UO.GetSkill('Alch')}}, -- Alchemy
    {['name']='Antimagie',              ['value']={UO.GetSkill('Resi')}}, -- Resisting Spells
    {['name']='Kartografie',            ['value']={UO.GetSkill('Cart')}}, -- Cartography
    {['name']='Magie',                  ['value']={UO.GetSkill('Mage')}}, -- Magery
    {['name']='Meditace',               ['value']={UO.GetSkill('Medi')}}, -- Meditation
    {['name']='Nekromancie',            ['value']={UO.GetSkill('Necr')}}, -- Necromancy
    {['name']='Odhad inteligence',      ['value']={UO.GetSkill('Eval')}}, -- Evaluating Intelligence
    {['name']='Opisov�n�',              ['value']={UO.GetSkill('Insc')}}, -- Inscription
    {['name']='�e� mrtv�ch',            ['value']={UO.GetSkill('Spir')}}, -- Spirit Speak
  },
  ['Hrani���sk�']={
    {['name']='Anatomie',               ['value']={UO.GetSkill('Anat')}}, -- Anatomy
    {['name']='Krocen� zv��at',         ['value']={UO.GetSkill('Anim')}}, -- Animal Taming
    {['name']='Kuch�n�',                ['value']={UO.GetSkill('Bush')}}, -- Bushido
    {['name']='L��en�',                 ['value']={UO.GetSkill('Heal')}}, -- Healing
    {['name']='L��en� zv��at',          ['value']={UO.GetSkill('Vete')}}, -- Veterinary
    {['name']='Ochutn�v�n�',            ['value']={UO.GetSkill('Tast')}}, -- Taste Identification
    {['name']='Pastevectv�',            ['value']={UO.GetSkill('Herd')}}, -- Herding
    {['name']='Pitv�n�',                ['value']={UO.GetSkill('Fore')}}, -- Forensic Evaluation
    {['name']='Ryba�en�',               ['value']={UO.GetSkill('Fish')}}, -- Fishing
    {['name']='Stopov�n�',              ['value']={UO.GetSkill('Trac')}}, -- Tracking
    {['name']='T�bo�en�',               ['value']={UO.GetSkill('Camp')}}, -- Camping
    {['name']='Va�en�',                 ['value']={UO.GetSkill('Cook')}}, -- Cooking
    {['name']='Znalost zv��e',          ['value']={UO.GetSkill('Anil')}}, -- Animal Lore
  },
  ['Zlod�jsk�']={
    {['name']='�much�n�',               ['value']={UO.GetSkill('Snoo')}}, -- Snooping
    {['name']='Kraden�',                ['value']={UO.GetSkill('Stea')}}, -- Stealing
    {['name']='Odhad ceny',             ['value']={UO.GetSkill('Item')}}, -- Item Identification
    {['name']='Odhalen� skryt�ho',      ['value']={UO.GetSkill('Dete')}}, -- Detecting Hidden
    {['name']='Odstran�n� pasti',       ['value']={UO.GetSkill('Remo')}}, -- Remove Trap
    {['name']='Otev�r�n� z�mku',        ['value']={UO.GetSkill('Lock')}}, -- Lockpicking
    {['name']='Pohyb ve st�nu',         ['value']={UO.GetSkill('Stlt')}}, -- Stealth
    {['name']='Skryt� se',              ['value']={UO.GetSkill('Hidi')}}, -- Hiding
    {['name']='Travi�stv�',             ['value']={UO.GetSkill('Pois')}}, -- Poisoning
  },
  ['Bardsk�']={
    {['name']='Hudba',                  ['value']={UO.GetSkill('Musi')}}, -- Musicianship
    {['name']='L�k�n�',                 ['value']={UO.GetSkill('Disc')}}, -- Discordance
    {['name']='Provokace',              ['value']={UO.GetSkill('Prov')}}, -- Provocation
    {['name']='Usmi�ov�n�',             ['value']={UO.GetSkill('Peac')}}, -- Peacemaking
  },
}
---------------
---- FUNCS ----
---------------
-- sysmsg queue --
local msg_queue={}
function ClientMessage(typ,zprava,barva,kde)
  if typ=='ExMsg' then local msg = function() UO.ExMsg(kde,3,barva,zprava) end table.insert(msg_queue,msg) end
  if typ=='SysMsg' then local msg = function() UO.SysMessage(zprava,barva) end table.insert(msg_queue,msg) end
end
------------------
--function warmode_cancel() while UO.CharStatus:find('G') and UO.Hits>0 do UO.Macro(6,0) wait(100) end end
function warmode_cancel() if UO.CharStatus:find('G') and UO.Hits>0 then UO.Macro(6,0) end end
function hid_cancel() if hid_pocitani==true then hid_pocitani=false ClientMessage('ExMsg','hid: canceled',0,UO.CharID) end end
function use_object(UObjectID) UO.LObjectID = UObjectID UO.Macro(17,0) end
function target_object(TObjectID,TObjectKind) UO.LTargetID = TObjectID UO.LTargetKind = TObjectKind UO.Macro(22,0) end
function dragndrop(id,stack,drop) UO.Drag(id,stack) time_to_run=getticks()+200 repeat wait(1) wait(minimal_pause-1) until UO.LLiftedID==id or time_to_run<getticks() UO.DropC(drop) end
--function dragndrop(id,stack,drop) UO.CliDrag(id) wait(100) UO.Msg('\013\010') time_to_run=getticks()+200 repeat wait(1) wait(minimal_pause-1) until UO.LLiftedID==id or time_to_run<getticks() UO.Click(932,488,true,true,true,false) end
function string_to_number(string) local number_str = string.match(string,'%d+') local number = tonumber(number_str) return number end
--function mounted() local chleba_s_maslem=World().Visible().Equipped().WithType(mounti).Items if chleba_s_maslem[1]~=nil then return true else return false end end 
function mounted() local chleba_s_maslem=ScanItems(false,{Type=mounti,ContID=UO.CharID}) if chleba_s_maslem[1]~=nil then return true else return false end end 


function check_for_sysmsg(sysmsg,lag,timeout)
 local time_to_run=getticks()+timeout
 repeat wait(lag) for i=1,#sysmsg do if sysmsg[i]==UO.SysMsg then return true end end until time_to_run<getticks()
 return false 
end

function wait_for_gump(lag,timeout)
 time_to_run = getticks() + timeout
 local vstupX=UO.ContPosX local vstupY=UO.ContPosY
 repeat wait (lag) until UO.ContPosX~=vstupX or UO.ContPosY~=vstupY or time_to_run < getticks()
 if UO.ContPosX~=vstupX or UO.ContPosY~=vstupY then return true else return false end
end

function wait_for_cont(cont,lag,timeout)
 time_to_run = getticks() + timeout
 repeat wait (lag) until UO.ContID==cont or time_to_run < getticks()
 if UO.ContID==cont then return true else return false end
end

function WaitForTarCurs(timeout)
 time_to_run = getticks() + timeout
 repeat wait (1) until UO.TargCurs == true or time_to_run < getticks()
 if UO.TargCurs == true then return true else return false end
end

function FindNUse(type,col)
local item = ScanItems(true,{Type=type,Col=col,Kind=0},{ContID=UO.CharID})
if item[1] ~= nil then use_object(item[1].ID) end
end

function always_run(prepinac)
  if prepinac=='on'  and autorun=='activated' then return end
  if prepinac=='off' and autorun=='deactivated' then return end
  UO.Macro(32,0) if autorun=='activated' then autorun='deactivated' elseif autorun=='deactivated' then autorun='activated' end
end

local hid_cntr = {}
function hid_pocitadlo()
--if UO.CharStatus:find('G') and hidtimer+200 < getticks() then hid_cancel() --[[ always_run('off') ]] return end
  if UO.CharStatus:find('G') and hidtimer+300 < getticks() then hid_cancel() --[[ always_run('off') ]] return end
    if hid_cntr[1]~=1 and (UO.SysMsg==string['lenost'] or UO.SysMsg==string['cannot']) then
    hid_pocitani=false always_run('off') return end
  for f=1,4 do
    if hid_cntr[f]~=1 and hidtimer+500*f < getticks() then
      if f==4 then always_run('off') hid_pocitani=false break end
      hid_cntr[f]=1 ClientMessage('ExMsg','hid: '..4-f,946,UO.CharID) break  
    end
  end 
end

local potka_timer_odeslano={}
function potka_pocitadlo()
    local time=(potka_timer-getticks())/1000
    if (math.ceil(time)-time)<0.3 then time=math.ceil(time) elseif (time-math.floor(time))<0.3 then time=math.floor(time) else time=nil end
--  if time~=nil and potka_timer_odeslano[time]==nil then ClientMessage('ExMsg','potka: '..time,0,UO.CharID) potka_timer_odeslano[time]=1 end
    if time~=nil and potka_timer_odeslano[time]==nil then ClientMessage('SysMsg','potka: '..time,0) potka_timer_odeslano[time]=1 end
    if time==1 then potka_timer=getticks() potka_timer_odeslano={} end
end


function potka_pocitadlo2()
  if potka_timer<=getticks() then return false
  else local time=(potka_timer-getticks())/1000
       if (math.ceil(time)-time)<=0.5 then time=math.ceil(time) elseif (time-math.floor(time))<0.5 then time=math.floor(time) else time=nil end
       return time end
end

local dyka_cntr = {}
function dyka_pocitadlo()
  for f=1,5 do
    if dyka_cntr[f]~=1 and dyka_timer+1000*f < getticks() then
      if f==5 then dyka_pocitani=false break end
      dyka_cntr[f]=1 ClientMessage('ExMsg','dyka: '..4-f,0,UO.CharID) break
    end
  end 
end

local skok_cntr = {}
function skok_pocitadlo()
  for f=1,5 do
    if skok_cntr[f]~=1 and skok_timer+1000*f < getticks() then
      if f==5 then skok_pocitani=false break end
      skok_cntr[f]=1 ClientMessage('ExMsg','skok: '..4-f,0,UO.CharID) break
    end
  end 
end

local stats_func = {
['HP']       = function() if UO.Hits~=0 then if potka_pocitadlo2()~=false then ClientMessage('ExMsg',tostring(UO.Hits)..(' / ')..(UO.Stamina)..('   (')..potka_pocitadlo2()..(')'),35,UO.CharID)
                                             else ClientMessage('ExMsg',tostring(UO.Hits)..(' / ')..(UO.Stamina),35,UO.CharID) end
                          else cm_screen() end end,
--['HP']     = function() if UO.Hits~=0 then ClientMessage('ExMsg',tostring(UO.Hits)..(' / ')..(UO.Stamina),35,UO.CharID)
--                        else cm_screen() end end,
['enemy HP'] = function() local variable=1
                          if UO.EnemyHits==0 then if vypocet~=100 then ClientMessage('SysMsg','VRAZDAAAAA!',38) cm_screen() end end
                          if UO.EnemyHits==4 and UO.EnemyID~=enemy then variable=50 end   
                          vypocet=(UO.EnemyHits/2)*variable if vypocet > 100 then vypocet = 100 end
                          if UO.EnemyHits~=0 then ClientMessage('ExMsg',tostring((vypocet)..('%')),57,UO.EnemyID) end
                          local enemy=UO.EnemyID end,
['DMG']      = function() ClientMessage('ExMsg',tostring('dmg: ')..(UO.MinDmg)..(' - ')..(UO.MaxDmg),1259,UO.CharID) end,                      
['weight']   = function() ClientMessage('ExMsg',tostring("nalozeni: ")..(UO.Weight)..(' / ')..(UO.MaxWeight),1378,UO.CharID) end,
['AR']       = function() ClientMessage('ExMsg',tostring('armor: ')..(UO.AR),1673,UO.CharID) end,
['FR']       = function() ClientMessage('ExMsg',tostring('ohen: ')..(UO.FR),40,UO.CharID) end,
['CR']       = function() ClientMessage('ExMsg',tostring('mraz: ')..(UO.CR),1266,UO.CharID) end,
['PR']       = function() ClientMessage('ExMsg',tostring('kyselina: ')..(UO.PR),1269,UO.CharID) end,
['ER']       = function() ClientMessage('ExMsg',tostring('elektrina: ')..(UO.ER),54,UO.CharID) end,
}
-- looter ----------------------------------------------------------------------------------------------
local drag=0
local storcont=0
local looter_func = {
['open_body']  = function(body)       if UO.ContID~=body then if UO.CharStatus:find('H') then ClientMessage('SysMsg','Looter: otevreni tela by te odhidlo ... otevri telo rucne',60) else UO.NextCPosX=0 UO.NextCPosY=0 use_object(body) end end end,
['main_fce']   = function(contas,typ) if loot_table[contas]==nil then
                                         if typ=='loot' then loot_table[contas] = ScanItems(true,{Type=loot,ContID=contas})
                                         elseif typ=='vse' then loot_table[contas] = ScanItems(true,{ContID=contas},{Type=dontloot}) end end
                                      if loot_table[contas][1]~=nil then
                                         if drag==1 then if typ=='loot' then UO.DropC(lootBagID) elseif typ=='vse' then UO.DropC(UO.CharID) end drag=0
                                         else UO.Drag(loot_table[contas][1].ID,loot_table[contas][1].Stack) ClientMessage('SysMsg','Looter: '..loot_table[contas][1].Stack..'x '..loot_table[contas][1].Name,60) table.remove(loot_table[contas],1) drag=1 end
                                      else lootovani=0 loot_table[contas]=nil table.insert(vylooceno,1,contas) if drag==1 then if typ=='loot' then UO.DropC(lootBagID) elseif typ=='vse' then UO.DropC(UO.CharID) end drag=0 end ClientMessage('SysMsg','Looter: vylooceno',60) end end,
['roztrid']    = function(contas)     if loot_table[contas]==nil then loot_table[contas] = ScanItems(true,{Type=loot,ContID=contas}) end
                                      if loot_table[contas]~=nil then
                                        if drag==1 then UO.DropC(storcont) drag=0 storcont=0
                                        else
                                         for i1=1,#loot_table[contas] do
                                          for i2=1,#lootit do for i3=1,#lootit[i2] do
                                          if loot_table[contas][i1].Type==lootit[i2][i3]["type"] and lootit[i2]["storeCont"]~=nil then
                                           UO.Drag(loot_table[contas][1].ID,loot_table[contas][1].Stack) drag=1 storcont=lootit[i2]["storeCont"] break end
                                          end if drag==1 then break end end
                                          table.remove(loot_table[contas],i1) if drag==1 then break end
                                         end
                                        end
                                      end 
                                      if loot_table[contas][1]~=nil then lootovani=0 loot_table[contas]=nil if drag==1 and storcont~=0 then UO.DropC(storcont) drag=0 storcont=0 end ClientMessage('SysMsg','Looter: roztrizeno',60) end end,
}
--- O L D   L O O T E R---------------------------------------------------------------------------------
function wait_until(podminka,lag,timeout)
 if timeout == nil then repeat wait (lag) until podminka == true 
 else time_to_run = getticks() + timeout
 repeat wait (lag) until podminka == true or time_to_run < getticks() end
end

function roztrid(co,odkud)
  use_object(odkud)
  wait_until(UO.ContID==odkud,200,1000)
  wait (200)
  local ncnt = UO.ScanItems(true)
  for nindex = 0,(ncnt-1) do
    local nid,ntype,nkind,ncontid,nx,ny,nz,nstack,nrep,ncol = UO.GetItem(nindex)
    for i1 = 1,#co do for i2 = 1,#co[i1] do
      if ntype == co[i1][i2]["type"] and ncontid == odkud and (ncol == co[i1][i2]["col"] or co[i1][i2]["col"] == "col") and co[i1]["storeCont"] ~= nil then
        dragndrop(nid,nstack,co[i1]["storeCont"]) wait(500) wait(ltr_lag_pause)
      end
    end end
  end
ClientMessage('SysMsg','Looter: roztrizeno',60)
end
--------------------------------------------------------------------------------------------------------
-- hotkeys
local command = 'wait'
local Funcs = {
---ZBRANE
['kopi']         =      function() local weapon = ScanItems(false,{Type=kopi['type'],Kind=0,ContID=UO.BackpackID},{Col=2414})
                                   if weapon[1] ~= nil then
                                    for i=1,#weapon do if weapon[i].ContID~=UO.CharID then use_object(weapon[i].ID) tarcurs_remover=1 lastequipedID=weapon[i].ID lastequipedTYPE=weapon[i].Type return end end
                                   else ClientMessage('SysMsg','Nemas zadne kopi',5555) end end,
['bic']            =      function() local weapon = ScanItems(true,{Type=bic['type'],Kind=0,ContID=UO.BackpackID})
                                     if weapon[1] ~= nil then
                                       local shield = ScanItems(true,{Type=stit,Kind=0,ContID=UO.BackpackID})
                                       use_object(weapon[1].ID) tarcurs_remover=1 lastequipedID=weapon[1].ID lastequipedTYPE=weapon[1].Type
                                       if shield[1] ~= nil then wait(minimal_pause) use_object(shield[1].ID) end
                                     else ClientMessage('SysMsg','Nemas bic',5555) end end,
['1hand']          =      function() local weapon = ScanItems(true,{Type=weapon_1H,Kind=0,ContID=UO.BackpackID})
                                     if weapon[1] ~= nil then
                                       local shield = ScanItems(true,{Type=stit,Kind=0,ContID=UO.BackpackID})
                                       use_object(weapon[1].ID) tarcurs_remover=1 lastequipedID=weapon[1].ID lastequipedTYPE=weapon[1].Type
                                       if shield[1] ~= nil then wait(minimal_pause) use_object(shield[1].ID) end
                                     else ClientMessage('SysMsg','Nemas zadnou jednorucku',5555) end end,
['ostep']          =      function() local weapon = ScanItems(true,{Type=kopi['type'],Kind=0,Col=2414,ContID=UO.BackpackID})
                                     if weapon[1] ~= nil then
                                       for i=1,#weapon do if weapon[i].ContID==UO.CharID then return end end
                                       use_object(weapon[1].ID) tarcurs_remover=1 lastequipedID=weapon[1].ID lastequipedTYPE=weapon[1].Type
                                     else ClientMessage('SysMsg','Nemas zadny ostep',5555) end end,
['kuse']           =      function() local weapon = ScanItems(true,{Type=kuse,Kind=0,ContID=UO.BackpackID})
                                     if weapon[1] ~= nil then use_object(weapon[1].ID) lastequipedID=weapon[1].ID lastequipedTYPE=weapon[1].Type else ClientMessage('SysMsg','Nemas kusi',5555) end end,
['luk']            =      function() local weapon = ScanItems(true,{Type=luk,Kind=0,ContID=UO.BackpackID})
                                     if weapon[1] ~= nil then use_object(weapon[1].ID) lastequipedID=weapon[1].ID lastequipedTYPE=weapon[1].Type else ClientMessage('SysMsg','Nemas luk',5555) end end,
['disarm']         =      function() for i=1,#weapon_1H do if lastequipedTYPE==weapon_1H[i] or lastequipedTYPE==bic['type'] then dragndrop(lastequipedID,1,UO.CharID) lastequipedID=0 lastequipedTYPE=0 return end end UO.Macro(24,1) lastequipedID=0 lastequipedTYPE=0 end,
['re-equip']       =      function() local equip = ScanItems(false,{Type=equip_types,Kind=0,ContID=UO.BackpackID})
                                     if equip[1] ~= nil then
                                      for i=1,#equip do use_object(equip[i].ID) wait(minimal_pause) end
                                     else ClientMessage('SysMsg','Nemas nic k equipnuti',5555) end end,
---ABILITKY
['krvko']          =      function() if krvko_timer<getticks()-30000 then UO.Macro(1,0,'.startability 20')
                                       if check_for_sysmsg({string['krvko']},10,100)==true then krvko_timer=getticks() end 
                                     else ClientMessage('SysMsg','Krvko already activated',5555) end end,
['para']           =      function() local weapon = ScanItems(true,{Type=vrhaci_nuz['type'],Kind=0},{ID=paradyka_blessed})
                                     if weapon[1] ~= nil then
                                       for i=1,#weapon do if weapon[i].ContID==UO.CharID then return end end
                                       use_object(weapon[1].ID)
                                     else local last_chance = ScanItems(true,{ID=paradyka_blessed,Kind=0})
                                      if last_chance[1] ~= nil then use_object(last_chance[1].ID) end end end,
['poison']         =      function() UO.Macro(1,0,'.lektvar jed')
                                     for i=1,#zbrane_poison do if lastequipedTYPE==zbrane_poison[i] then WaitForTarCurs(500) target_object(lastequipedID,1) end end
                                     end,
['srazeni']        =      function() if srazeni_timer<getticks()-3000 then UO.Macro(1,0,'.startability 17')
                                       if check_for_sysmsg({string['srazeni']},10,100)==true then srazeni_timer=getticks() end 
                                     else ClientMessage('SysMsg','Srazeni already activated',5555) end end,
['port']           =      function() if port_timer<getticks()-3000 then UO.Macro(1,0,'.startability 25') wait(100)
                                       if UO.SysMsg:find(' do teleportace...') then port_timer=getticks() end 
                                     else ClientMessage('SysMsg','Port already activated',5555) end end,
['nabij kusi']     =      function() UO.Macro(1,0,'.nabij') end,
['odpocinek']      =      function() UO.Macro(1,0,'.startability 4') end,
---LEKTVARY
['GH']             =      function() UO.Macro(1,0,'.lektvar lecit')    if potka_timer<=getticks() and check_for_sysmsg({string['potka1'],string['potka2']},10,200)==true then potka_timer=getticks()+potion_timer['GH'] end end,
['obnova']         =      function() UO.Macro(1,0,'.obnova')           if potka_timer<=getticks() and check_for_sysmsg({string['potka1'],string['potka2']},10,200)==true then potka_timer=getticks()+potion_timer['obnova'] end end,
['protijed']       =      function() UO.Macro(1,0,'.lektvar protijed') if potka_timer<=getticks() and check_for_sysmsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['protijed'] end end,
['ocelka']         =      function() UO.Macro(1,0,'.lektvar ocel')     if potka_timer<=getticks() and check_for_sysmsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['ocelka'] end end,
['explosko - drop'] =     function() UO.Macro(1,0,'.lektvar vybuch')   WaitForTarCurs(100) target_object(UO.CharID,1) end,
['explosko - throw'] =    function() UO.Macro(1,0,'.lektvar vybuch')   WaitForTarCurs(100) target_object(UO.LTargetID,1) end,
['sila']           =      function() UO.Macro(1,0,'.lektvar sila')     if potka_timer<=getticks() and check_for_sysmsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['sila'] end end,
['hbitost']        =      function() UO.Macro(1,0,'.lektvar hbitost')  if potka_timer<=getticks() and check_for_sysmsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['hbitost'] end end,
['osvezko']        =      function() UO.Macro(1,0,'.lektvar osvez')    if potka_timer<=getticks() and check_for_sysmsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['osvezko'] end end,
['dispell']        =      function() UO.Macro(1,0,'.lektvar rozptyl')  if potka_timer<=getticks() and check_for_sysmsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['dispell'] end end,
['invisko']        =      function() UO.Macro(1,0,'.lektvar neviditelnost')  if potka_timer<=getticks() and check_for_sysmsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['invisko'] end end,
['nezranko']       =      function() UO.Macro(1,0,'.lektvar nezranitelnost') if potka_timer<=getticks() and check_for_sysmsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['nezranko'] end end,
['inko']           =      function() UO.Macro(1,0,'.lektvar povest')   if potka_timer<=getticks() and check_for_sysmsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['inko'] end end,
['wine']           =      function() if wine_timer<getticks()-3000 then
                                      local piticko = ScanItems(false,{Name='Lahev Vina',Type=lahev_vina['type'],Kind=0})
                                      if piticko[1] ~= nil then use_object(piticko[1].ID) wine_timer=getticks() 
                                      else ClientMessage('SysMsg','Nem� u sebe v�no',5555) end
                                     else ClientMessage('SysMsg','Je�t� nem��e� p�t',5555) end end,
---SKILLY
['HID']            =      function() --if mounted()==true then ClientMessage('SysMsg','Na koni se nem��e� skr�t',0) return end
                                     if hid_pocitani~=true then 
                                     hid_cntr = {} warmode_cancel() UO.Macro(13,21) hid_pocitani=true hidtimer=getticks() end end,
['fast HID']       =      function() if hid_pocitani~=true then 
                                     hid_cntr = {} warmode_cancel() UO.Macro(13,21) wait(10) always_run('on') hid_pocitani=true hidtimer=getticks() end end,
['detect hiding']  =      function() if hid_pocitani==true then hid_pocitani=false ClientMessage('ExMsg','hid: canceled',0,UO.CharID) end ClientMessage('SysMsg','Chces se pokusit hledat skryte',0) UO.Macro(13,14) end,
['forensics']      =      function() UO.Macro(13,19) end,
['item identification'] = function() UO.Macro(13,3) end,
['animal lore']    =      function() UO.Macro(13,2) end,
['animal taming']  =      function() UO.Macro(13,35) end,
['anatomy']        =      function() UO.Macro(13,1) end,
['eval int']       =      function() UO.Macro(13,16) end,
['spirit speak']   =      function() UO.Macro(13,32) end,
['cmuchani']       =      function() if mounted()==true then ClientMessage('SysMsg','Na koni nem��e� �muchat',0) return end
                                     UO.TargCurs=true UO.SysMessage('Kde chce� �muchat?',54) while UO.TargCurs==true do wait (100) if getkey('esc') then UO.TargCurs=false return end end
                                     if UO.LTargetID==UO.CharID then ClientMessage('SysMsg','Pro� �muchat ve vlastn�m batohu?',0) return end
                                     local bagl=World().InContainer(UO.LTargetID).WithType(3701).Items
--                                     local bagl=ScanItems(true,{Type=3701,ContID=UO.LTargetID})
                                     if bagl[1]~=nil then use_object(bagl[1].ID) ClientMessage('SysMsg','�much�m',0) else ClientMessage('SysMsg','Batoh nenalezen!',0) end end,
['stealing']       =      function() UO.Macro(13,33) end,
['provokace']      =      function() warmode_cancel() UO.Macro(13,22) end,
['usmirovani']     =      function() warmode_cancel() UO.Macro(13,9) end,
['lakani']         =      function() warmode_cancel() UO.Macro(13,15) end,
['tracking']       =      function() warmode_cancel() UO.Macro(13,38) end,
['track players']  =      function() warmode_cancel() UO.Macro(13,38) timeout = getticks() + 300
                                     repeat wait (1) until UO.ContName == 'objpicker gump' or timeout < getticks()
                                     UO.Click(UO.CursorX,UO.CursorY,true,true,true,false)
                                     UO.Click(UO.ContPosX+172,UO.ContPosY+65,true,true,true,false)
                                     UO.Click(UO.ContPosX+172,UO.ContPosY+65,true,true,true,false) end,
['track animals']  =      function() warmode_cancel() UO.Macro(13,38) timeout = getticks() + 300
                                     repeat wait (1) until UO.ContName == 'objpicker gump' or timeout < getticks()
                                     UO.Click(UO.CursorX,UO.CursorY,true,true,true,false)
                                     UO.Click(UO.ContPosX+64,UO.ContPosY+64,true,true,true,false)
                                     UO.Click(UO.ContPosX+64,UO.ContPosY+64,true,true,true,false) end,
---PET CONTROL
['pet do baglu']   =      function() local zmensovak = ScanItems(false,{Type=lektvar_zmensovani['type'],Col=lektvar_zmensovani['col'],Kind=0})
                                     local obr = ScanItems(false,{ID=pet['id_velky'],Kind=1,Dist=3})
                                     if zmensovak[1]~=nil and obr[1]~=nil then dragndrop(zmensovak[1].ID,1,obr[1].ID) end
                                     wait(200) wait(minimal_pause-200)
                                     local mrnousek = ScanItems(false,{Type=pet['typ']['type'],Kind=1,Dist=3})
                                     if mrnousek[1]~=nil then dragndrop(mrnousek[1].ID,1,UO.CharID) end end,                                      
['pet z baglu']    =      function() local pet = ScanItems(false,{Type=pet['typ']['type'],Col=pet['typ']['col'],Kind=0})
                                     if pet[1] ~= nil then use_object(pet[1].ID) end end,
---MOUNT
['naskoc']         =      function() if mounted()==true then ClientMessage('SysMsg','Already mounted!',0) return end
                                     local mini = ScanItems(true,{Type=jezditko['typ']['type'],Col=jezditko['typ']['col'],Kind=0})
                                     if mini[1] ~= nil then use_object(mini[1].ID) wait(minimal_pause) end
                                     local kul = ScanItems(true,{ID=jezditko['kul_id'],Dist=3,Kind=1})
                                     if kul[1] ~= nil then use_object(kul[1].ID) wait(minimal_pause) end
                                     if mini[1]==nil and kul[1]==nil then UO.Macro(3,0,tostring(jezditko['name'])..(' stay')) end
                                     local kun = ScanItems(true,{ID=jezditko['id_velky'],Dist=3})
                                     if kun[1] ~= nil then use_object(kun[1].ID) end
                                     if kul[1] ~= nil then wait(minimal_pause) dragndrop(kul[1].ID,1,UO.BackpackID) end
                                     if kun[1] == nil then ClientMessage('SysMsg','Mount nenalezen!',0) end
                                     end,
--['naskoc']       =      function() local start=getticks()
--                                   local itemy=World().Visible()
--                                   local mounted=itemy.Equipped().WithType(mounti).Items if mounted[1]~=nil then ClientMessage('SysMsg','Already mounted!',0) return end
--                                   local mini = itemy.InAnyContainer().WithType(jezditko['typ']['type']).WithCol(jezditko['typ']['col']).Items
--                                   local kul = itemy.OnGround().WithID(jezditko['kul_id']).InRange(3).Items
--                                   local kun=itemy.OnGround().WithID(jezditko['id_velky']).InRange(3).Items
--                                   if mini[1]~=nil then use_object(mini[1].ID) wait(minimal_pause) kun=itemy.OnGround().WithID(jezditko['id_velky']).InRange(3).Live().Items end                                      
--                                   if kul[1]~=nil then use_object(kul[1].ID) wait(minimal_pause) end
--                                   if kun[1]~=nil then use_object(kun[1].ID)
--                                     if check_for_sysmsg({string['mount too far'],string['mount timer']},10,100)==true then if volani_kone<getticks()-3000 then UO.Macro(3,0,tostring(jezditko['name'])..(' come')) volani_kone=getticks() end
--                                     else volani_kone=getticks()-3000 end end
--                                   if kul[1] ~= nil then wait(minimal_pause-100) dragndrop(kul[1].ID,1,UO.BackpackID) end
--                                   if kun[1] == nil then
--                                     local zabehlejkun = ScanItems(true,{ID=jezditko['id_velky'],Dist=11})
--                                     if zabehlejkun[1] ~= nil then if volani_kone<getticks()-3000 then UO.Macro(3,0,tostring(jezditko['name'])..(" come")) volani_kone=getticks() end
--                                     else ClientMessage('SysMsg','Mount nenalezen!',0) end
--                                   end UO.SysMessage(tostring(getticks()-start),5555) end,
['seskoc']         =      function() if mounted()==false then UO.Macro(3,0,tostring(jezditko['name'])..(" stay"))
                                     else warmode_cancel() use_object(UO.CharID) end end,
--['seskoc']       =      function() if mount=='dismounted' then UO.Macro(3,0,tostring(jezditko['name'])..(" stay")) volani_kone=getticks()-3000
--                                   else warmode_cancel() use_object(UO.CharID) mount='dismounted' end end,
['uvaz']           =      function() if mounted()==true then warmode_cancel() use_object(UO.CharID) UO.Macro(3,0,tostring(jezditko['name'])..(" stay")) volani_kone=getticks()-3000 wait(minimal_pause) end
                                     local kun = ScanItems(true,{ID=jezditko['id_velky'],Dist=3})
                                     if kun[1] ~= nil then
                                       local kul = ScanItems(true,{ID=jezditko['kul_id']})
                                       if kul[1]~=nil then
                                         if kul[1].Kind == 0 or (kul[1].Dist <= 3 and (kul[1].X~=kun[1].X or kul[1].Y~=kun[1].Y or kul[1].Z~=kun[1].Z)) then
                                          UO.Drag(jezditko['kul_id']) time_to_run=getticks()+200 repeat wait(1) wait(minimal_pause-1) until UO.LLiftedID==id or time_to_run<getticks() UO.DropG(kun[1].X,kun[1].Y,kun[1].Z)  wait (100)
                                          wait(minimal_pause-100) use_object(jezditko['kul_id']) WaitForTarCurs(200) target_object(kun[1].ID,1)
                                         elseif kul[1].Kind == 1 and kul[1].Dist <= 3 and kul[1].X==kun[1].X and kul[1].Y==kun[1].Y and kul[1].Z==kun[1].Z then
                                         use_object(jezditko['kul_id']) WaitForTarCurs(200) target_object(kun[1].ID,1) end
                                       else ClientMessage('SysMsg','Kul nenalezen!',0) end
                                     else ClientMessage('SysMsg','Mount nenalezen!',0)
                                     end end,
---LOOTER
['loot mrtvoly']   =      function() if lootovani==0 then
                                      local mrtvola = ScanItems(true,{Type=8198,Kind=1,Dist=1},{ID=vylooceno})
                                      if mrtvola[1]~=nil then ClientMessage('SysMsg','Looter: '..mrtvola[1].Name,60) looter_func['open_body'](mrtvola[1].ID) lootovani=mrtvola[1].ID typlootu='loot' loot_pause=getticks()
                                      else ClientMessage('SysMsg','Looter: mrtvola nenalezena',60) end
                                     else ClientMessage('SysMsg','Looter: uz lootuju',60) end end,
['loot target']    =      function() if lootovani==0 then
                                      UO.TargCurs = true UO.SysMessage('Looter: Co chces vylootit?',60)
                                      local tOut = getticks()
                                      while UO.TargCurs == true do wait (100) if getkey('esc') or tOut<getticks()-3000 then UO.TargCurs = false ClientMessage('SysMsg','Looter: timeout',60) return end end
                                      local targeted_container = UO.LTargetID
                                      local mrtvola = ScanItems(true,{ID=targeted_container,Type=8198,Kind=1})
                                      if mrtvola[1]~=nil then looter_func['open_body'](mrtvola[1].ID) lootovani=mrtvola[1].ID typlootu='loot' loot_pause=getticks()
                                      elseif UO.ContID~=targeted_container then UO.NextCPosX=0 UO.NextCPosY=0 use_object(targeted_container) lootovani=targeted_container typlootu='loot' loot_pause=getticks() end
                                     else ClientMessage('SysMsg','Looter: uz lootuju',60) end end,
['loot all']       =      function() if lootovani==0 then
                                      UO.TargCurs = true UO.SysMessage('Looter: Co chces vyluxovat?',60)
                                      local tOut = getticks()
                                      while UO.TargCurs == true do wait (100) if getkey('esc') or tOut<getticks()-3000 then UO.TargCurs = false ClientMessage('SysMsg','Looter: timeout',60) return end end
                                      local targeted_container = UO.LTargetID
                                      local mrtvola = ScanItems(true,{ID=targeted_container,Type=8198,Kind=1})
                                      if mrtvola[1]~=nil then looter_func['open_body'](mrtvola[1].ID) lootovani=mrtvola[1].ID typlootu='vse' loot_pause=getticks()
                                      elseif UO.ContID~=targeted_container then UO.NextCPosX=0 UO.NextCPosY=0 use_object(targeted_container) lootovani=targeted_container typlootu='vse' loot_pause=getticks() end
                                     else ClientMessage('SysMsg','Looter: uz lootuju',60) end end,
--['loot vysyp']   =      function() if lootovani==0 then
--                                    if UO.ContID~=lootBagID then UO.NextCPosX=0 UO.NextCPosY=0 use_object(lootBagID) lootovani=1 loot_pause=getticks() end
--                                   else ClientMessage('SysMsg','Looter: uz lootuju',60) end end,
['loot vysyp']     =      function() roztrid(lootit,lootBagID) end,
---MISCELLANEOUS
['toggle sound']   =      function() UO.Macro(8,0) local pack=wait_for_gump(1,500) if pack==false then return end -- repeat wait(100) until UO.ContName=='OptionsGump'
                                     UO.Click(422,233,true,true,true,false) wait(1000)
                                     UO.Click(828,536,true,true,true,false) end,
['always run']     =      function() always_run() end,                                                                       
['use hand']       =      function() UO.Macro(1,0,'.usehand') end,
['transparency']   =      function() UO.Macro(29,0) end,
['open journal']   =      function() UO.Macro(8,3) end,
['open map']       =      function() UO.Macro(8,0) repeat wait(100) until UO.ContName=='OptionsGump'
                                     UO.Click(980,220,true,true,true,false) wait(1000) -- interface
                                     UO.Click(425,408,true,true,true,false) wait(1000) -- disable menu bar
                                     UO.Click(639,537,true,true,true,false) wait(1000) -- pouzit
                                     UO.Click(68,21,true,true,true,false) wait(1000)   -- mapa
--                                   UO.Click(68,21,true,true,true,false) wait(1000)   -- mapa
                                     UO.Click(425,408,true,true,true,false) wait(1000) -- disable menu bar
                                     UO.Click(828,536,true,true,true,false) end,
['open paperdoll'] =      function() UO.Macro(8,1) end, -- local pack=wait_for_gump(1,500) if pack==false then return end UO.ContPosX=808 UO.ContPosY=-16 end,
['open backpacks'] =      function() --item=ScanItems(false,{ID=bagl_id}) if item[1]~=nil then UO.NextCPosX=0 UO.NextCPosY=0 use_object(bagl_id) local pack=wait_for_gump(1,500) if pack==false then return end UO.ContPosX=826 UO.ContPosY=546 return end 
                                     UO.NextCPosX=802 UO.NextCPosY=357 use_object(UO.BackpackID) local pack1=wait_for_cont(UO.BackpackID,1,500) if pack1==false then return end
                                     UO.NextCPosX=0 UO.NextCPosY=0 use_object(bagl_id) local pack2=wait_for_cont(bagl_id,1,500) if pack2==false then return end UO.ContPosX=826 UO.ContPosY=546 end,
['buff lista']     =      function() UO.Macro(1,0,'.buff_lista') end,
['dekorater']      =      function() UO.Macro(1,0,'.dekorater') end,
['SchopText']      =      function() UO.Macro(1,0,'.schoptext') local pack=wait_for_gump(1,500) if pack==true then UO.ContPosX=UO.CursorX-50 UO.ContPosY=UO.CursorY-70 end end,
['mapa - on']      =      function() cm_mapa('on') end,
--['mapa - off']   =      function() cm_mapa('off') uoam_prepinac='off' end,
['gamma - on']     =      function() cm_gamma('on') gamma_prepinac='on' end,
['gamma - off']    =      function() cm_gamma('off') gamma_prepinac='off' end,
['print skills']   =      function() ClientMessage('SysMsg','Bojov�',5555) for i=1,#skilly['Bojov�'] do ClientMessage('SysMsg',tostring(skilly['Bojov�'][i]['name'])..' '..tostring(skilly['Bojov�'][i]['value'][1]/10)..'/'..tostring(skilly['Bojov�'][i]['value'][3]/10),1259) end
                                     ClientMessage('SysMsg','V�robn�',5555) for i=1,#skilly['V�robn�'] do ClientMessage('SysMsg',tostring(skilly['V�robn�'][i]['name'])..' '..tostring(skilly['V�robn�'][i]['value'][1]/10)..'/'..tostring(skilly['V�robn�'][i]['value'][3]/10),1673) end
                                     ClientMessage('SysMsg','Magick�',5555) for i=1,#skilly['Magick�'] do ClientMessage('SysMsg',tostring(skilly['Magick�'][i]['name'])..' '..tostring(skilly['Magick�'][i]['value'][1]/10)..'/'..tostring(skilly['Magick�'][i]['value'][3]/10),1266) end
                                     ClientMessage('SysMsg','Hrani���sk�',5555) for i=1,#skilly['Hrani���sk�'] do ClientMessage('SysMsg',tostring(skilly['Hrani���sk�'][i]['name'])..' '..tostring(skilly['Hrani���sk�'][i]['value'][1]/10)..'/'..tostring(skilly['Hrani���sk�'][i]['value'][3]/10),68) end
                                     ClientMessage('SysMsg','Zlod�jsk�',5555) for i=1,#skilly['Zlod�jsk�'] do ClientMessage('SysMsg',tostring(skilly['Zlod�jsk�'][i]['name'])..' '..tostring(skilly['Zlod�jsk�'][i]['value'][1]/10)..'/'..tostring(skilly['Zlod�jsk�'][i]['value'][3]/10),55) end
                                     ClientMessage('SysMsg','Bardsk�',5555) for i=1,#skilly['Bardsk�'] do ClientMessage('SysMsg',tostring(skilly['Bardsk�'][i]['name'])..' '..tostring(skilly['Bardsk�'][i]['value'][1]/10)..'/'..tostring(skilly['Bardsk�'][i]['value'][3]/10),1378) end
                                     
end,
---DALSI
['obvaz']          =      function() UO.Macro(1,0,'.obvaz') WaitForTarCurs(500) target_object(UO.CharID,1) end,
['kuchani']        =      function() for i=1,#zbrane_kuchani do if lastequipedTYPE==zbrane_kuchani[i] then UO.Macro(1,0,'.usehand') return end end
                                     local nuz = ScanItems(true,{Type=nuz_na_stahovani['type'],Kind=0})
                                     if nuz[1] ~= nil then use_object(nuz[1].ID)
                                     else ClientMessage('SysMsg','Nemas nuz na stahovani',5555) end end,
['hlava']          =      function() UO.Macro(1,0,'.utrhni_hlavu') end,
['otevri']         =      function() local doors = ScanItems(true,{Type=zavrene_dvere,Dist=2,RelZ={-5,-4,-3,-2,-1,0,1,2,3,4,5}})
                                     if doors[1] ~= nil then
                                      if doors[2]~=nil then
                                       if doors[1].Y==doors[2].Y then
                                        if math.abs(doors[1].RelX)<math.abs(doors[2].RelX) then use_object(doors[1].ID)
                                        elseif  math.abs(doors[1].RelX)>math.abs(doors[2].RelX) then use_object(doors[2].ID) end
                                       elseif doors[1].X==doors[2].X then
                                        if math.abs(doors[1].RelY)<math.abs(doors[2].RelY) then use_object(doors[1].ID)
                                        elseif  math.abs(doors[1].RelY)>math.abs(doors[2].RelY) then use_object(doors[2].ID) end
                                       else ClientMessage('SysMsg','Chyba',0) end
                                      else use_object(doors[1].ID) end 
                                     else ClientMessage('SysMsg','Dvere nenalezeny',0)
                                     end end,
['zavri']          =      function() local doors = ScanItems(true,{Type=otevrene_dvere,Dist=2,RelZ={-5,-4,-3,-2,-1,0,1,2,3,4,5}})
                                     for i = 1,#doors do if doors[i] ~= nil then if i > 1 then wait(minimal_pause) end use_object(doors[i].ID) end end end,
['kopat poklad']   =      function() UO.Macro(1,0,'.kopat_poklad') end,
['napln toulec']   =      function() UO.TargCurs=true UO.SysMessage('Vyber ��py',54) repeat wait(10) until UO.TargCurs==false
                                     local sipy = ScanItems(true,{ID=UO.LTargetID,Type={3903,7163},Kind=0})
                                     local toulec = ScanItems(true,{Type=15193,ContID=UO.CharID})
                                     local toulec_kapacita=nil local odecti=0
                                     if sipy[1]~=nil and toulec[1]~=nil then
                                      if sipy[1].ContID~=UO.BackpackID then
                                       local sipu_v_toulci=string_to_number(toulec[1].Details) if sipu_v_toulci~=nil then odecti=sipu_v_toulci end
                                       if toulec[1].Name=='Obrovsk� Toulec ' then toulec_kapacita=500-odecti
                                       elseif toulec[1].Name=='Velk� Toulec ' then toulec_kapacita=400-odecti
                                       elseif toulec[1].Name=='Toulec ' then toulec_kapacita=300-odecti
                                       elseif toulec[1].Name=='Mal� Toulec ' then toulec_kapacita=200-odecti end
                                       dragndrop(sipy[1].ID,toulec_kapacita,UO.BackpackID) wait(minimal_pause) end
                                      use_object(toulec[1].ID) WaitForTarCurs(500) target_object(sipy[1].ID,1)
                                     else ClientMessage('SysMsg','Tohle nejde',38) end end,
['vysyp toulec']   =      function() local toulec = ScanItems(true,{Type=15193,ContID=UO.CharID})
                                     if toulec[1]~=nil and toulec[1].Details~='Prazdny' then use_object(toulec[1].ID) WaitForTarCurs(200) target_object(toulec[1].ID,1)
                                     else ClientMessage('SysMsg','Nem� toulec s ��py',38) end end,
['prelejvator']    =      function() UO.TargCurs = true wait(200) UO.SysMessage('Co chce� vysypat?',5555)
                                     while UO.TargCurs == true do wait (100) if getkey('esc') then ClientMessage('SysMsg','Stop',5555) return end end
                                     local vysypat = UO.LTargetID
                                     UO.TargCurs = true wait(200) UO.SysMessage('Kam to chce� vysypat?',5555)
                                     while UO.TargCurs == true do wait (100) if getkey('esc') then ClientMessage('SysMsg','Stop',5555) return end end
                                     local kos = UO.LTargetID
                                     if vysypat~=nil and vysypat~=0 and kos~=nil and kos~=0 then
                                       local itemy = ScanItems(false,{ContID=vysypat})
                                       if itemy~=nil then
                                         for i=1,#itemy do dragndrop(itemy[i].ID,itemy[i].Stack,kos) wait(1000) end
                                       end
                                     end ClientMessage('SysMsg','Hotovo',5555) end,
['hide item']    =      function() UO.TargCurs = true UO.SysMessage('Co chce� schovat?',5555) while UO.TargCurs == true do wait (100) end UO.HideItem(UO.LTargetID) end,
['resync']       =      function() UO.Macro(1,0,'.resync') end,
}
while UO.CliLogged==false do wait(1000) end
---------------
-- MAIN LOOP --
---------------
--j=journal:new()
while UO.CliLogged==true do
    setatom('Request','true')
    while getatom('Request') == 'true' do wait(loop_wait) end
    cmd = getatom('Request')
    if cmd ~= 'nil' then -- ClientMessage('SysMsg','PUSH: '..cmd,5555)
      if Funcs[cmd] then Funcs[cmd]() end
      cmd = 'nil'
    end        
    if hid_pocitani==true then hid_pocitadlo() end
--    if dyka_pocitani==true then dyka_pocitadlo() end
--    if skok_pocitani==true then skok_pocitadlo() end
-- looter --
    if lootovani~=0 and lootovani~=1 and lootovani==UO.ContID and loot_pause<getticks()-500 then looter_func['main_fce'](lootovani,typlootu) loot_pause=getticks() end
    if lootovani~=0 and lootovani==1 and loot_pause<getticks()-500 then looter_func['roztrid'](lootBagID) loot_pause=getticks() end
    if lootovani~=0 and lootovani~=1 and lootovani~=UO.ContID and loot_pause<getticks()-3000 then ClientMessage('SysMsg','Looter: timeout',60) lootovani=0 end
------------
    if tarcurs_remover==1 and UO.TargCurs==true then tarcurs_remover=nil UO.Key('esc') end
    if (UO.MaxHits==0 or UO.MaxStam==0) and status_prodleva<getticks()-1000 then UO.Macro(8,2) UO.Macro(10,2) UO.ContPosX=821 UO.ContPosY=311 status_prodleva=getticks() end
    if UO.CharStatus:find('G') and warmode_alert<getticks()-10000 then warmode_alert=getticks() ClientMessage('SysMsg','Warning: Warmode ON',0) end
--    if potka_timer>getticks() then potka_pocitadlo() end
-- stats
    if ((status['Hits']~=UO.Hits and UO.MaxHits~=0) or (status['Stamina']~=UO.Stamina and UO.MaxStam~=0)) and UO.Hits~=nil and UO.Stamina~=nil and status_msg_wait<getticks()-status_msg_pauza then stats_func['HP']() status['Hits']=UO.Hits status['Stamina']=UO.Stamina status_msg_wait=getticks() end
    if status['EnemyHits']~=UO.EnemyHits and UO.EnemyHits~=nil and UO.EnemyID~=0 and UO.EnemyID~=nil and status_msg_wait<getticks()-status_msg_pauza then stats_func['enemy HP']() status['EnemyHits']=UO.EnemyHits status_msg_wait=getticks() end
    if (status['MinDmg']~=UO.MinDmg or status['MaxDmg']~=UO.MaxDmg) and (UO.MinDmg~=nil or UO.MaxDmg~=nil) and UO.MaxDmg~=0 and status_msg_wait<getticks()-status_msg_pauza then stats_func['DMG']() status['MinDmg']=UO.MinDmg status['MaxDmg']=UO.MaxDmg status_msg_wait=getticks() end
    if (status['Weight']~=UO.Weight or status['MaxWeight']~=UO.MaxWeight) and (UO.Weight~=nil or UO.MaxWeight~=nil) and UO.MaxWeight~=0 and status_msg_wait<getticks()-status_msg_pauza then stats_func['weight']() status['Weight']=UO.Weight status['MaxWeight']=UO.MaxWeight status_msg_wait=getticks() end
    if status['AR']~=UO.AR and UO.AR~=nil and status_msg_wait<getticks()-status_msg_pauza then stats_func['AR']() status['AR']=UO.AR status_msg_wait=getticks() end
    if status['FR']~=UO.FR and UO.FR~=nil and (UO.FR<500 or UO.FR>(-500)) and status_msg_wait<getticks()-status_msg_pauza then stats_func['FR']() status['FR']=UO.FR status_msg_wait=getticks() end
    if status['CR']~=UO.CR and UO.CR~=nil and (UO.CR<500 or UO.CR>(-500)) and status_msg_wait<getticks()-status_msg_pauza then stats_func['CR']() status['CR']=UO.CR status_msg_wait=getticks() end
    if status['PR']~=UO.PR and UO.PR~=nil and (UO.PR<500 or UO.PR>(-500)) and status_msg_wait<getticks()-status_msg_pauza then stats_func['PR']() status['PR']=UO.PR status_msg_wait=getticks() end
    if status['ER']~=UO.ER and UO.ER~=nil and (UO.ER<500 or UO.ER>(-500)) and status_msg_wait<getticks()-status_msg_pauza then stats_func['ER']() status['ER']=UO.ER status_msg_wait=getticks() end
-- journal
--    local jrnl=j:find('*vrh� d�ku*','|946|You see: '..tostring(jezditko['name'])..'|',' na skok.*')
--    local jrnl=j:find('*vrh� d�ku*',' na skok.*','|1268|Tv� �toky jsou te mnohem bolestivja�|')
--    if jrnl==1 then if dyka_pocitani~=true then dyka_pocitani=true dyka_cntr={} dyka_timer=getticks() j:clear() end end
--    if jrnl==2 then UO.Macro(3,0,tostring(jezditko['name'])..(' stay')) j:clear() end
--    if jrnl==2 then if skok_pocitani~=true then skok_pocitani=true skok_cntr={} skok_timer=getticks() j:clear() end end
--    if jrnl==3 then krvko='activated' krvko_timer=getticks() UO.SysMessage('Krvko activated',5555) end
--    if mounted()==false then Funcs['naskoc']() end
--sysmsg
    if msg_queue[1]~=nil then msg_queue[1]() table.remove(msg_queue,1) end 
end
if gamma_prepinac=='on' then cm_gamma('off') end
--if uoam_prepinac=='on' then cm_mapa('off') end