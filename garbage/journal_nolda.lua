local jrnl = dofile(getinstalldir()..'scripts/journal.lua')
local sl = dofile(getinstalldir()..'scripts/simplelib.lua')

function ignore(msg)
  return string.find(msg, '\ufffddat') or
  string.find(msg, 'kolovratek') or
  (...)
end 
     
j = journal:new()

local f = sl.file(getinstalldir()..'tmp/antiafk', "write")
   
print("start")      
while( true ) do
   if(UO.Hits < 15) then       
     f.streamout('malo zivotu\n')
   end
   if(j:next() ~= nil) then
      msg = j:last()
      if not ignore(msg) then
        print(msg)
        f.streamout('a'..msg..'\n')
      end
      j:clear()
   end      
   wait(50)
end             
print("stop")      
f.finalize()