dofile('functions/FindItems.lua')

function use_object(UObjectID) UO.LObjectID = UObjectID UO.Macro(17,0) end

local lektvar = ScanItems(true,{Type=lektvar_skryti_povesti['type'],Col=lektvar_skryti_povesti['col'],ContID=UO.BackpackID,Kind=0})
local start_stack = lektvar[1].Stack - 1

use_object(lektvar[1].ID)
local start_time = getticks()

repeat wait (50)
use_object(lektvar[1].ID)
local lektvar = ScanItems(true,{Type=lektvar_skryti_povesti['type'],Col=lektvar_skryti_povesti['col'],ContID=UO.BackpackID,Kind=0})
until start_stack-1==lektvar[1].Stack

print(getticks()-start_time)