local command = 'wait'
local Funcs = { ['Heal'] = function() print('cast heal') end,
                ['fireball'] = function() print('cast fireball') end,
}
 
while not getkey('f12') do
      setatom('Request','true')
      while getatom('Request') == 'true' do wait(1) end
      cmd = getatom('Request')
      if cmd ~= 'nil' then
          if Funcs[cmd] then Funcs[cmd]() end
          cmd = 'nil'
      end        
end
 