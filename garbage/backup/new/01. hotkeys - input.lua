---------------
---- VARS -----
---------------
local queue_wait=getticks()
local minimal_pause=10 -- minimalni pauza mezi vyhazovanim jednotlivych hotkeys z fronty
local loop_wait=50 -- pauza v hlavnim loopu z duvodu redukce naroku na cpu
--local command={}
local modkeys = {'ctrl','alt','shift','space'}
local hotkey = {
---TARGETY
[{'1'}]                 =    'Target Next',
[{'2'}]                 =    'Attack Last',
[{'3'}]                 =    'Target Previous',
[{'1','alt'}]           =    'Attack Closest',
---ZBRANE
[{'a','ctrl'}]          =    'kopi',
[{'s','ctrl'}]          =    '1hand',
[{'d','ctrl'}]          =    'bic',
[{'z','ctrl'}]          =    'ostep',
[{'x','ctrl'}]          =    'strelne',
[{'c','ctrl'}]          =    'disarm',
---ABILITKY
[{'s'}]                 =    'krvko',
[{'s','shift'}]         =    'para',
[{'s','space'}]         =    'srazeni',
[{'w'}]                 =    'port',
[{'x'}]                 =    'nabij kusi',
[{'z'}]                 =    'odpocinek',
---LEKTVARY
[{'a','alt'}]           =    'obnova',
[{'s','alt'}]           =    'GH',
[{'d','alt'}]           =    'ocelka',
[{'w','alt'}]           =    'explosko',
[{'e','alt'}]           =    'protijed',
[{'z','shift'}]         =    'osvezko',
[{'c','shift'}]         =    'sila',
[{'x','shift'}]         =    'hbitost',
[{'q','space'}]         =    'invisko',
[{'d','space'}]         =    'nezranko',
[{'v','shift'}]         =    'inko',
---SKILLY
[{'q','alt'}]           =    'HID',
[{'v'}]                 =    'detect hiding',
[{'p'}]                 =    'forensics',
[{'i'}]                 =    'item identification',
[{'k'}]                 =    'stealing',
[{'1','shift'}]         =    'provokace',
[{'2','shift'}]         =    'usmirovani',
[{'3','shift'}]         =    'lakani',
[{'l'}]                 =    'tracking',
[{'f'}]                 =    'track players',
[{'l','alt'}]           =    'track animals',
---MOUNT
[{'a'}]                 =    'naskoc',
[{'a','shift'}]         =    'seskoc',
[{'a','shift','alt'}]   =    'uvaz',
---PET CONTROL
[{'d','shift'}]         =    'all stay',
[{'e','shift'}]         =    'all come',
[{'f','shift'}]         =    'all stop',
[{'r','shift'}]         =    'all go',
[{'e','shift','alt'}]   =    'pet z baglu',
[{'d','shift','alt'}]   =    'pet do baglu',
---LOOTER
[{'f1'}]                =    'loot mrtvoly',
[{'f2'}]                =    'loot target',
[{'f3'}]                =    'loot all',
[{'f4'}]                =    'loot vysyp',
---MISCELLANEOUS
[{'q','alt','shift'}]   =    'quit uo',
[{'j','alt'}]           =    'open journal',
[{'f10'}]               =    'open paperdoll',
[{'f5'}]                =    'open backpacks',
[{'f9'}]                =    'buff lista',
[{'f11'}]               =    'dekorater',
[{'v','space'}]         =    'SchopText',
[{'g','space'}]         =    'gamma - on',
[{'g','shift','space'}] =    'gamma - off',
---DALSI
[{'d'}]                 =    'obvaz',
[{'z','ctrl','shift'}]  =    'kuchani',
[{'z','alt','shift'}]   =    'hlava',
[{'q'}]                 =    'otevri',
[{'q','shift'}]         =    'zavri',
[{'j'}]                 =    'kopat poklad',
[{'o','alt'}]           =    'napln toulec',
[{'p','alt'}]           =    'vysyp toulec',
}

local hotkey = {
---TARGETY
[{'1'}]                 =    'Target Next',
[{'2'}]                 =    'Attack Last',
[{'3'}]                 =    'Target Previous',
[{'1','alt'}]           =    'Attack Closest',
---PET CONTROL
[{'d','shift'}]         =    'all stay',
[{'e','shift'}]         =    'all come',
[{'f','shift'}]         =    'all stop',
[{'r','shift'}]         =    'all go',
---LOOTER
[{'f1'}]                =    'loot mrtvoly',
[{'f2'}]                =    'loot target',
[{'f3'}]                =    'loot all',
[{'f4'}]                =    'loot vysyp',
---MISCELLANEOUS
[{'q','alt','shift'}]   =    'quit uo',
[{'j','alt'}]           =    'open journal',
[{'f10'}]               =    'open paperdoll',
[{'f5'}]                =    'open backpacks',
[{'f9'}]                =    'buff lista',
[{'f11'}]               =    'dekorater',
[{'v','space'}]         =    'SchopText',
[{'g','space'}]         =    'gamma - on',
[{'g','shift','space'}] =    'gamma - off',
}

---------------
---- FUNCS ----
---------------
function ClearUOMsg() for i=1,140 do UO.Key('back') end end

function keys_enabled()
  wait(200) UO.Msg("|HOTKEYS ZAPNUTY") for i=1,140 do UO.Key('space') end
end

function keys_disabled(var)
  ClearUOMsg() wait(100)
  if     var=='message' then UO.Msg (''  )
  elseif var=='whisper' then UO.Msg ('; ')
  elseif var=='yell'    then UO.Msg ('! ')
  elseif var=='emote'   then UO.Msg (': ')
  elseif var=='party'   then UO.Msg ('/' ) end
  while true do wait (1) if getkey('enter') then
    while getkey('enter') do wait(1) end keys_enabled() return
  end end
end

function modifier_key(mod_key1,mod_key2)
 local notkeys = {}
 local cntr = 1
 for i=1,#modkeys do if mod_key1~=modkeys[i] and mod_key2~=modkeys[i] then notkeys[cntr]=modkeys[i] cntr=cntr+1 end end
 for i=1,#notkeys do if getkey(notkeys[i]) then return false end end
 if (getkey(mod_key1) or mod_key1==nil) and (getkey(mod_key2) or mod_key2==nil) then return true
 else return false end
end

---queue
queue = {}
queue.mt = {
     __index = queue,
}
 
function queue.New()
   local self = {}
   setmetatable(self,queue.mt)
   return self
end
 
function queue:push(s)
   if #self>0 then for i=1,#self do if self[i]:find(s) then return end end end -- brani pridani akce, pokud je uz ve fronte
   table.insert(self, 1, s)
end
 
function queue:dequeue()
   local s
   if #self > 0 and queue_wait<getticks()-minimal_pause then
        s = self[#self]
        self[#self] = nil
        queue_wait=getticks()
   end
   return s
end

---------------
---- INIT -----
---------------
local Commands = queue.New()
ClearUOMsg()
wait (100)
keys_enabled() 
---------------
-- MAIN LOOP --
---------------
while UO.CliLogged==true do wait(loop_wait)
  if getkey('m')     and modifier_key(nil,nil)     then while getkey('m')     do wait(1) end keys_disabled('message')      end
  if getkey('m')     and modifier_key('alt',nil)   then while getkey('m')     do wait(1) end keys_disabled('whisper')      end
  if getkey('m')     and modifier_key('shift',nil) then while getkey('m')     do wait(1) end keys_disabled('yell')         end
  if getkey('n')     and modifier_key(nil,nil)     then while getkey('n')     do wait(1) end keys_disabled('emote')        end
  if getkey('b')     and modifier_key(nil,nil)     then while getkey('b')     do wait(1) end keys_disabled('party')        end
  if getkey('enter') and modifier_key(nil,nil)     then while getkey('enter') do wait(1) end ClearUOMsg() keys_enabled()   end
  if getkey('b')     and modifier_key('space',nil) then while getkey('b')     do wait(1) end ClearUOMsg() wait (100) UO.Msg ('/add'..'\013\010') keys_enabled()   end
  if getkey('n')     and modifier_key('space',nil) then while getkey('n')     do wait(1) end ClearUOMsg() wait (100) UO.Msg ('/accept'..'\013\010') keys_enabled() end
  for key,command in pairs(hotkey) do
    if getkey(key[1]) and modifier_key(key[2],key[3]) then UO.SysMessage('PUSH: '..command,5555) Commands:push(command) while getkey(key[1]) do wait(1) end end
  end
  if getatom('Request') == 'true' then
    command = Commands:dequeue()
    if command then setatom('Request',command) UO.SysMessage('DEQUEUE: '..command,5555) else setatom('Request','nil') end   
  end
end