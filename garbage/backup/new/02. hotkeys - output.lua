dofile('functions/FindItems.lua')
dofile('functions/journal.lua')
dofile('data/items.lua')
dofile('functions/communicator.lua')
dofile('functions/looter_funcs.lua')             -- OLD LOOTER functions
---------------
---- VARS -----
---------------
local minimal_pause = 500 -- minimalni pauza mezi akcemi podle pravidel shardu
local loop_wait=50 -- pauza v hlavnim loopu z duvodu redukce naroku na cpu
local action_wait = 500 -- minimalni pauza mezi jednotlivymi akcemi

local bagl_id = 1074563268 -- druhy bagl na otevreni, krome BACKPACK
local lootBagID = 1074563275
local uvaz_kul_id = 1074184920
local jezditko = {['name']='Fialka',['id_velky']=1045721,['typ']=hneda_slechta}
--local jezditko = {['name']='Mraziva',['id_velky']=912859,['typ']=bily_kun}
--local jezditko = {['name']='Stinova vrazedkyne',['id_velky']=1057577,['typ']=cerna_slechta}
--local pet = {['name']='medvedi slecna',['id_velky']=670596,['typ']=medved_grizzly}
local pet = {['name']='Adelaida',['id_velky']=898875,['typ']=obri_had}
local weapon_1H = {kratke_kopi['type'][1],kratke_kopi['type'][2],kris['type'][1],kris['type'][2],dyka['type'][1],dyka['type'][2]}
local strelne = {tezka_kuse['type'][1],skladany_luk['type'][1],elfi_luk['type'],tezka_kuse['type'][2],skladany_luk['type'][2]}
local stit = {kruhovy_kovovy_stit['type'],dreveny_stit['type']}

-- looter ----------------------------------------------------------------------------------------------
local zlatky    = {['storeCont']=nil,zlatak,stribrnak,medak}
local regy_obyc = {['storeCont']=1074225859,mandragora,zensen,rulik,sirny_prach,krvavy_mech,pavucina,kost,cerna_perla,katova_kukla,cerny_vres}
local regy_vzac = {['storeCont']=1073832617,krvava_kost,cerna_kost,kost_demona,netopyri_kridlo,krystal_nox,urodna_prst,pemza,suche_drevo,prach_z_hrobu,obsidian,krev_demona,draci_krev,draci_srdce}
local esence    = {['storeCont']=1074697417,esence_ohne,esence_vzduchu,esence_slizu,esence_zeme,esence_vody}
local lektvary  = {['storeCont']=1074583195,slabsi_lektvar_hbitosti,slabsi_lektvar_sily,slabsi_lektvar_moudrosti,lektvar_svetla,slabsi_lecivy_lektvar,slabsi_protijed,slabsi_lektvar_osvezeni,slabsi_lektvar_obnovy,slabsi_vybusny_lektvar,slabsi_jed}
local sipy      = {['storeCont']=1074563299,sip,elfi_sip,smrtici_sip,ohnivy_sip,stribrny_sip,zlaty_sip}
local sipky     = {['storeCont']=1074563250,sipka,drtiva_sipka,ohniva_sipka,stribrna_sipka,zlata_sipka}
local kuze      = {['storeCont']=1073746071,hromada_kuzi,hromada_vlcich_kuzi,hromada_medvedich_kuzi,rozstrihana_kuze,rozstrihana_vlci_kuze,rozstrihana_medvedi_kuze}
local hadry     = {['storeCont']=nil,dublet,serpa,roba,kosile,zdobena_kosile,obycejne_saty,dlouhe_kalhoty,kratke_kalhoty,tunika,zdobene_saty}
local pirka     = {['storeCont']=1074697417,pirko,orli_pirko,havrani_pirko}
local pruty     = {['storeCont']=1073832997,prut_zeleza,prut_medi,prut_stribra,prut_zlata}
local trofeje   = {['storeCont']=1073832997,vrrci_tlapka,skreti_tesak,zamotek_obriho_pavouka,sklipkani_zamotek}
local lahve     = {['storeCont']=1074226166,prazdna_lahev}
local strivka   = {['storeCont']=1073832997,strevo} 
local ostatni   = {['storeCont']=nil,obvaz,krvavy_obvaz,hulka,svitek_prvni_stupen}

local lootit = {zlatky,regy_obyc,regy_vzac,esence,lektvary,sipy,sipky,kuze,hadry,pirka,pruty,trofeje,lahve,strivka,ostatni}
ltr_lag_pause = 0
--------------------------------------------------------------------------------------------------------

local mount='dismounted'
local krvko='deactivated'
local krvko_timer=getticks()-32000
local warmode_alert=getticks()-10000
local status_prodleva=getticks()-1000 -- zabranuje nekolikanasobnemu vytazeny statusu
local tarcurs_remover={}
local gamma_prepinac={}

local status = {['Hits']=UO.Hits,['Stamina']=UO.Stamina,['EnemyHits']=UO.EnemyHits,['EnemyID']=UO.EnemyID,
                ['MinDmg']=UO.MinDmg,['MaxDmg']=UO.MaxDmg,['Weight']=UO.Weight,['MaxWeight']=UO.MaxWeight,
                ['AR']=UO.AR,['FR']=UO.FR,['CR']=UO.CR,['PR']=UO.PR,['ER']=UO.ER}
local status_msg_wait = getticks()
local status_msg_pauza=100 -- prodleva mezi status zpravami

-- looter ----------------------------------------------------------------------------------------------
dontloot = {8197,8252}

--drop all values from nested tables in 'lootit' in one table 'lootit2'
local lootit2={}
for i1=1,#lootit do for i2=1,#lootit[i1] do
 if type(lootit[i1][i2]['type']) == 'table' then
  for i3=1,#lootit[i1][i2]['type'] do table.insert(lootit2,lootit[i1][i2]['type'][i3]) end
 else table.insert(lootit2,lootit[i1][i2]['type']) end
end end

--remove duplicate values from 'lootit2' and creates table 'loot'
local hash = {}
local loot = {}
for _,v in ipairs(lootit2) do
   if (not hash[v]) then
       loot[#loot+1] = v
       hash[v] = true
   end
end
---------------
---- FUNCS ----
---------------
function warmode_cancel() while UO.CharStatus:find("G") and UO.Hits>0 do UO.Macro(6,0) wait(50) end end
function hid_cancel() if hid_pocitani==true then hid_pocitani=false UO.ExMsg(UO.CharID,3,0,"hid: canceled") end end
function use_object(UObjectID) UO.LObjectID = UObjectID UO.Macro(17,0) end
function target_object(TObjectID,TObjectKind) UO.LTargetID = TObjectID UO.LTargetKind = TObjectKind UO.Macro(22,0) end
function dragndrop(id,stack,drop) UO.Drag(id,stack) time_to_run=getticks()+200 repeat wait(1) wait(action_wait-1) until UO.LLiftedID==id or time_to_run<getticks() UO.DropC(drop) end

function wait_for_gump(lag,timeout)
 time_to_run = getticks() + timeout
 local vstupX=UO.ContPosX local vstupY=UO.ContPosY
 repeat wait (lag) until UO.ContPosX~=vstupX or UO.ContPosY~=vstupY or time_to_run < getticks()
 if UO.ContPosX~=vstupX or UO.ContPosY~=vstupY then return true else return false end
end

function WaitForTarCurs(timeout)
 time_to_run = getticks() + timeout
 repeat wait (1) until UO.TargCurs == true or time_to_run < getticks()
 if UO.TargCurs == true then return true else return false end
end

function FindNUse(type,col)
local item = ScanItems(true,{Type=type,Col=col,Kind=0},{ContID=UO.CharID})
if item[1] ~= nil then use_object(item[1].ID) end
end

local hid_cntr = {}
function hid_pocitadlo()
  if UO.CharStatus:find("G") then hid_cancel() return end
  if hid_cntr[1]~=1 and (UO.SysMsg=="Nemas ani trosku chut delat neco takoveho" or UO.SysMsg=="Nen� mo�n�, abys to dok�zala pr�v� te�.") then
    hid_pocitani=false return end
  for f=1,4 do
    if hid_cntr[f]~=1 and hidtimer+500*f < getticks() then
      if f==4 then hid_pocitani=false break end
      hid_cntr[f]=1 UO.ExMsg(UO.CharID,3,0,'hid: '..4-f) break
    end
  end 
end

local dyka_cntr = {}
function dyka_pocitadlo()
  for f=1,5 do
    if dyka_cntr[f]~=1 and dyka_timer+1000*f < getticks() then
      if f==5 then dyka_pocitani=false break end
      dyka_cntr[f]=1 UO.ExMsg(UO.CharID,3,0,'dyka: '..4-f) break
    end
  end 
end

local skok_cntr = {}
function skok_pocitadlo()
  for f=1,5 do
    if skok_cntr[f]~=1 and skok_timer+1000*f < getticks() then
      if f==5 then skok_pocitani=false break end
      skok_cntr[f]=1 UO.ExMsg(UO.CharID,3,0,'skok: '..4-f) break
    end
  end 
end

-- stats
function stats_hp() wait(10) if UO.Hits~=0 then UO.ExMsg (UO.CharID,3,35,tostring(UO.Hits)..(' / ')..(UO.Stamina)) else cm_screen() end end
function stats_ehp() wait(10) local variable=1
                     if UO.EnemyHits==0 then if vypocet~=100 then UO.SysMessage('VARZDAAAAA!',38) cm_screen() end end
                     if UO.EnemyHits==4 and UO.EnemyID~=enemy then variable=50 end   
                     vypocet=(UO.EnemyHits/2)*variable if vypocet > 100 then vypocet = 100 end
                     if UO.EnemyHits~=0 then UO.ExMsg (UO.EnemyID,3,57,tostring((vypocet)..('%'))) end
                     local enemy=UO.EnemyID end
function stats_dmg() wait(10) UO.ExMsg (UO.CharID,3,1259,tostring('dmg: ')..(UO.MinDmg)..(' - ')..(UO.MaxDmg)) end                                          
function stats_weight() wait(10) UO.ExMsg (UO.CharID,3,1378,tostring("nalozeni: ")..(UO.Weight)..(' / ')..(UO.MaxWeight)) end
function stats_ar() wait(10) UO.ExMsg (UO.CharID,3,1673,tostring('armor: ')..(UO.AR)) end
function stats_fr() wait(10) UO.ExMsg (UO.CharID,3,40,tostring('ohen: ')..(UO.FR)) end
function stats_cr() wait(10) UO.ExMsg (UO.CharID,3,1266,tostring('mraz: ')..(UO.CR)) end
function stats_pr() wait(10) UO.ExMsg (UO.CharID,3,1269,tostring('kyselina: ')..(UO.PR)) end
function stats_er() wait(10) UO.ExMsg (UO.CharID,3,54,tostring('elektrina: ')..(UO.ER)) end

-- hotkeys
local command = 'wait'
local Funcs = {
---TARGETY
['Target Next']    =      function() UO.Macro(50,5) end,
['Attack Last']    =      function() UO.Macro(53,0) end,
['Target Previous'] =     function() UO.Macro(51,5) end,
['Attack Closest'] =      function() UO.Macro(52,5) UO.Macro(53,0) end,
---ZBRANE
['kopi']           =      function() local weapon = ScanItems(true,{Type=kopi['type'],Kind=0},{Col=2414})
                                     if weapon[1] ~= nil then
                                      for i=1,#weapon do if weapon[i].ContID~=UO.CharID then use_object(weapon[i].ID) tarcurs_remover=1 return end end
                                     else UO.SysMessage('Nemas zadne kopi',5555)
                                     end end,
['1hand']          =      function() local weapon = ScanItems(true,{Type=weapon_1H,Kind=0},{ContID=UO.CharID})
                                     if weapon[1] ~= nil then
                                       local shield = ScanItems(true,{Type=stit,Kind=0},{ContID=UO.CharID})
                                       use_object(weapon[1].ID) tarcurs_remover=1
                                       if shield[1] ~= nil then wait(minimal_pause) use_object(shield[1].ID) end
                                     else UO.SysMessage('Nemas zadnou jednorucku',5555)
                                     end end,
['bic']            =      function() local weapon = ScanItems(true,{Type=bic['type'],Kind=0},{ContID=UO.CharID})
                                     if weapon[1] ~= nil then
                                       local shield = ScanItems(true,{Type=stit,Kind=0},{ContID=UO.CharID})
                                       use_object(weapon[1].ID) tarcurs_remover=1
                                       if shield[1] ~= nil then wait(minimal_pause) use_object(shield[1].ID) end
                                     else UO.SysMessage('Nemas bic',5555)
                                     end end,
['ostep']          =      function() local weapon = ScanItems(true,{Type=kopi['type'],Kind=0,Col=2414})
                                     if weapon[1] ~= nil then
                                       for i=1,#weapon do if weapon[i].ContID==UO.CharID then return end end
                                       use_object(weapon[1].ID) tarcurs_remover=1
                                     else UO.SysMessage('Nemas zadny ostep',5555)
                                     end end,
['strelne']        =      function() local weapon = ScanItems(true,{Type=strelne,Kind=0},{ContID=UO.CharID})
                                     if weapon[1] ~= nil then use_object(weapon[1].ID) else UO.SysMessage('Nemas strelnou zbran',5555) end end,
['disarm']         =      function() UO.Macro(24,1) end,
---ABILITKY
['krvko']          =      function() if krvko=='deactivated' then UO.Macro(1,0,'.startability 20') wait(100)
                                       if UO.SysMsg=='Tv� �toky jsou te mnohem bolestivja�' then krvko='activated' krvko_timer=getticks() UO.SysMessage('Krvko activated',5555) end 
                                     else UO.SysMessage('Krvko already activated',5555) end end,
['para']           =      function() local weapon = ScanItems(true,{Type=vrhaci_nuz['type'],Kind=0})
                                     if weapon[1] ~= nil then
                                       for i=1,#weapon do if weapon[i].ContID==UO.CharID then return end end
                                       use_object(weapon[1].ID)
                                     end end,
['srazeni']        =      function() local weapon = ScanItems(true,{Type=bic['type'],Kind=0},{ContID=UO.CharID})
                                     local shield = ScanItems(true,{Type=stit,Kind=0},{ContID=UO.CharID})
                                     UO.Macro(1,0,'.startability 17')
                                     if weapon[1] ~= nil then wait(minimal_pause) use_object(weapon[1].ID) tarcurs_remover=1 
                                       if shield[1] ~= nil then wait(minimal_pause) use_object(shield[1].ID) end
                                     end end,
['port']           =      function() UO.Macro(1,0,'.startability 25') end,
['nabij kusi']     =      function() UO.Macro(1,0,'.nabij') end,
['odpocinek']      =      function() UO.Macro(1,0,'.startability 4') end,
---LEKTVARY
['GH']             =      function() UO.Macro(1,0,'.lektvar lecit') end,
['obnova']         =      function() UO.Macro(1,0,'.obnova') end,
['protijed']       =      function() UO.Macro(1,0,'.lektvar protijed') end,
['ocelka']         =      function() UO.Macro(1,0,'.lektvar ocel') end,
['explosko']       =      function() UO.Macro(1,0,'.lektvar vybuch') WaitForTarCurs(100) target_object(UO.CharID,1) end,
['sila']           =      function() UO.Macro(1,0,'.lektvar sila') end,
['hbitost']        =      function() UO.Macro(1,0,'.lektvar hbitost') end,
['osvezko']        =      function() UO.Macro(1,0,'.lektvar osvez') end,
['invisko']        =      function() UO.Macro(1,0,'.lektvar neviditelnost') end,
['nezranko']       =      function() UO.Macro(1,0,'.lektvar nezranitelnost') end,
['inko']           =      function() UO.Macro(1,0,'.lektvar povest') end,
---SKILLY
['HID']            =      function() if hid_pocitani~=true then 
                                     hid_cntr = {} warmode_cancel() UO.Macro(13,21) hid_pocitani=true hidtimer=getticks() end end,
['detect hiding']  =      function() if hid_pocitani==true then hid_pocitani=false UO.ExMsg(UO.CharID,3,0,"hid: canceled") end UO.SysMessage('Chces se pokusit hledat skryte') UO.Macro(13,14) end,
['forensics']      =      function() UO.Macro(13,19) end,
['item identification'] = function() UO.Macro(13,3) end,
['stealing']       =      function() UO.Macro(13,33) end,
['provokace']      =      function() UO.Macro(13,22) end,
['usmirovani']     =      function() UO.Macro(13,9) end,
['lakani']         =      function() UO.Macro(13,15) end,
['tracking']       =      function() warmode_cancel() UO.Macro(13,38) end,
['track players']  =      function() warmode_cancel() UO.Macro(13,38) timeout = getticks() + 300
                                     repeat wait (1) until UO.ContName == 'objpicker gump' or timeout < getticks()
                                     UO.Click(UO.CursorX,UO.CursorY,true,true,true,false)
                                     UO.Click(UO.ContPosX+172,UO.ContPosY+65,true,true,true,false)
                                     UO.Click(UO.ContPosX+172,UO.ContPosY+65,true,true,true,false) end,
['track animals']  =      function() warmode_cancel() UO.Macro(13,38) timeout = getticks() + 300
                                     repeat wait (1) until UO.ContName == 'objpicker gump' or timeout < getticks()
                                     UO.Click(UO.CursorX,UO.CursorY,true,true,true,false)
                                     UO.Click(UO.ContPosX+64,UO.ContPosY+64,true,true,true,false)
                                     UO.Click(UO.ContPosX+64,UO.ContPosY+64,true,true,true,false) end,
---PET CONTROL
['all stay']       =      function() UO.Macro(3,0,'all stay') end,
['all come']       =      function() UO.Macro(3,0,'all come') end,
['all stop']       =      function() UO.Macro(3,0,'all stop') end,
['all go']         =      function() UO.Macro(3,0,'all go') end,
['pet do baglu']   =      function() local zmensovak = ScanItems(true,{Type=lektvar_zmensovani["type"],Col=lektvar_zmensovani["col"],Kind=0})
                                     local obr = ScanItems(true,{ID=pet['id_velky'],Kind=1,Dist=3})
                                     if zmensovak[1]~=nil and obr[1]~=nil then dragndrop(zmensovak[1].ID,1,obr[1].ID) end
                                     wait(200) wait(minimal_pause-200)
                                     local mrnousek = ScanItems(true,{Type=pet['typ']['type'],Kind=1,Dist=3})
                                     if mrnousek[1]~=nil then dragndrop(mrnousek[1].ID,1,UO.CharID) end end,                                      
['pet z baglu']    =      function() local pet = ScanItems(true,{Type=pet['typ']['type'],Col=pet['typ']['col'],Kind=0})
                                     if pet[1] ~= nil then use_object(pet[1].ID) end end,
---MOUNT
['naskoc']         =      function() local mini = ScanItems(true,{Type=jezditko['typ']['type'],Col=jezditko['typ']['col'],Kind=0})
                                     if mini[1] ~= nil then use_object(mini[1].ID) wait(minimal_pause) end
                                     local kul = ScanItems(true,{ID=uvaz_kul_id,Dist=3,Kind=1})
                                     if kul[1] ~= nil then use_object(kul[1].ID) wait(minimal_pause) end
                                     local kun = ScanItems(true,{ID=jezditko['id_velky'],Dist=3})
                                     if kun[1] ~= nil then use_object(kun[1].ID) wait (100)
                                       if UO.SysMsg == 'Nedos�hne� na tu bytost.' or UO.SysMsg == 'Takhle rychle po sesednut� nasednout nem��e�' then UO.Macro(3,0,tostring(jezditko['name'])..(' come'))
                                       else mount='mounted' end end
                                     if kul[1] ~= nil then wait(minimal_pause-100) dragndrop(kul[1].ID,1,UO.BackpackID) end
                                     if kun[1] == nil then
                                       local zabehlejkun = ScanItems(true,{ID=jezditko['id_velky'],Dist=10})
                                       if zabehlejkun[1] ~= nil then UO.Macro(3,0,tostring(jezditko['name'])..(" come"))
                                       else UO.SysMessage('Mount nenalezen!') end
                                     end end,
['seskoc']         =      function() warmode_cancel() UO.LObjectID=UO.CharID UO.Macro(17,0) UO.Macro(3,0,tostring(jezditko['name'])..(" stay")) mount='dismounted' end,
['uvaz']           =      function() local kun = ScanItems(true,{ID=jezditko['id_velky'],Dist=3})
                                   if kun[1] ~= nil then
                                     local kul = ScanItems(true,{ID=uvaz_kul_id})
                                     if kul[1]~=nil then
                                       if kul[1].Kind == 0 or (kul[1].Dist <= 3 and (kul[1].X~=kun[1].X or kul[1].Y~=kun[1].Y or kul[1].Z~=kun[1].Z)) then
                                        UO.Drag(uvaz_kul_id) time_to_run=getticks()+200 repeat wait(1) wait(minimal_pause-1) until UO.LLiftedID==id or time_to_run<getticks() UO.DropG(kun[1].X,kun[1].Y,kun[1].Z)  wait (100)
                                        wait(minimal_pause-100) use_object(uvaz_kul_id) WaitForTarCurs(200) target_object(kun[1].ID,1)
                                       elseif kul[1].Kind == 1 and kul[1].Dist <= 3 and kul[1].X==kun[1].X and kul[1].Y==kun[1].Y and kul[1].Z==kun[1].Z then
                                       use_object(uvaz_kul_id) WaitForTarCurs(200) target_object(kun[1].ID,1) end
                                     else UO.SysMessage('Kul nenalezen!') end
                                   else UO.SysMessage('Mount nenalezen!')
                                   end end,
---LOOTER
['loot mrtvoly']   =      function() lootuj("mrtvoly",loot,lootBagID) end,
['loot target']    =      function() lootuj("target",loot,lootBagID) end,
['loot all']       =      function() lootuj("target", "vse",UO.CharID) end,
['loot vysyp']     =      function() roztrid(lootit,lootBagID) end,
---MISCELLANEOUS
['quit uo']        =      function() UO.Macro(20,0) stop() end,
['open journal']   =      function() UO.Macro(8,3) end,
['open paperdoll'] =      function() UO.Macro(8,1) local pack=wait_for_gump(1,500) if pack==false then return end UO.ContPosX=808 UO.ContPosY=-16 end,
['open backpacks'] =      function() UO.NextCPosX=778 UO.NextCPosY=336 use_object(UO.BackpackID) local pack=wait_for_gump(1,500) if pack==false then return end
                                     UO.NextCPosX=0 UO.NextCPosY=0 use_object(bagl_id) local pack=wait_for_gump(1,500) if pack==false then return end UO.ContPosX=777 UO.ContPosY=499 end,
['buff lista']     =      function() UO.Macro(1,0,'.buff_lista') end,
['dekorater']      =      function() UO.Macro(1,0,'.dekorater') end,
['SchopText']      =      function() UO.Macro(1,0,'.schoptext') local pack=wait_for_gump(1,500) if pack==true then UO.ContPosX=UO.CursorX-50 UO.ContPosY=UO.CursorY-70 end end,
['gamma - on']     =      function() cm_gamma('on') gamma_prepinac='on' end,
['gamma - off']    =      function() cm_gamma('off') gamma_prepinac='off' end,
---DALSI
['obvaz']          =      function() UO.Macro(1,0,'.obvaz') WaitForTarCurs(500) target_object(UO.CharID,1) end,
['kuchani']        =      function() local nuz = ScanItems(true,{Type=nuz_na_stahovani['type'],Kind=0})
                                     if nuz[1] ~= nil then use_object(nuz[1].ID)
                                     else UO.SysMessage('Nemas nuz na stahovani',5555) UO.Macro(1,0,'.usehand') end end,
['hlava']          =      function() UO.Macro(1,0,'.utrhni_hlavu') end,
['otevri']         =      function() local doors = ScanItems(true,{Type=zavrene_dvere,Dist=2,RelZ={-5,-4,-3,-2,-1,0,1,2,3,4,5}})
                                     if doors[1] ~= nil then
                                      if doors[2]~=nil then
                                       if doors[1].Y==doors[2].Y then
                                        if math.abs(doors[1].RelX)<math.abs(doors[2].RelX) then use_object(doors[1].ID)
                                        elseif  math.abs(doors[1].RelX)>math.abs(doors[2].RelX) then use_object(doors[2].ID) end
                                       elseif doors[1].X==doors[2].X then
                                        if math.abs(doors[1].RelY)<math.abs(doors[2].RelY) then use_object(doors[1].ID)
                                        elseif  math.abs(doors[1].RelY)>math.abs(doors[2].RelY) then use_object(doors[2].ID) end
                                       else UO.SysMessage('Chyba') end
                                      else use_object(doors[1].ID) end 
                                     else UO.SysMessage('Dvere nenalezeny')
                                     end end,
['zavri']          =      function() local doors = ScanItems(true,{Type=otevrene_dvere,Dist=2,RelZ={-5,-4,-3,-2,-1,0,1,2,3,4,5}})
                                     for i = 1,#doors do if doors[i] ~= nil then if i > 1 then wait(minimal_pause) end use_object(doors[i].ID) end end end,
['kopat poklad']   =      function() UO.Macro(1,0,'.kopat_poklad') end,
['napln toulec']   =      function() UO.TargCurs=true UO.SysMessage('Vyber ��py',54) repeat wait(1) until UO.TargCurs==false
                                     local sipy = ScanItems(true,{ID=UO.LTargetID,Type={3903,7163},ContID=UO.BackpackID})
                                     local toulec = ScanItems(true,{Type=15193,ContID=UO.CharID})
                                     if sipy[1]~=nil and toulec[1]~=nil then for i=1,6 do if i > 1 then wait(minimal_pause) end use_object(toulec[1].ID) WaitForTarCurs(200) target_object(sipy[1].ID,1) end
                                     else UO.SysMessage('Tohle nejde',38) end end,
['vysyp toulec']   =      function() local toulec = ScanItems(true,{Type=15193,ContID=UO.CharID})
                                     if toulec[1]~=nil and toulec[1].Details~='Prazdny' then for i=1,6 do if i > 1 then wait(minimal_pause) end use_object(toulec[1].ID) WaitForTarCurs(200) target_object(toulec[1].ID,1) end
                                     else UO.SysMessage('Nem� toulec s ��py',38) end end,
['prelejvator']    =      function() UO.TargCurs = true wait(200) UO.SysMessage('Co chce� vysypat?',5555)
                                     while UO.TargCurs == true do wait (100) if getkey('esc') then UO.SysMessage('Stop',5555) return end end
                                     local vysypat = UO.LTargetID
                                     UO.TargCurs = true wait(200) UO.SysMessage('Kam to chce� vysypat?',5555)
                                     while UO.TargCurs == true do wait (100) if getkey('esc') then UO.SysMessage('Stop',5555) return end end
                                     local kos = UO.LTargetID
                                     if vysypat~=nil and vysypat~=0 and kos~=nil and kos~=0 then
                                       local itemy = ScanItems(true,{ContID=vysypat})
                                       if itemy~=nil then
                                         for i=1,#itemy do dragndrop(itemy[i].ID,itemy[i].Stack,kos) wait(1000) end
                                       end
                                     end UO.SysMessage('Hotovo',5555) end,
}

---------------
-- MAIN LOOP --
---------------
j=journal:new()
UO.SysMessage(mount,5555)
while UO.CliLogged==true do
    setatom('Request','true')
    while getatom('Request') == 'true' do wait(loop_wait) end
    cmd = getatom('Request')
    if cmd ~= 'nil' then
      if Funcs[cmd] then Funcs[cmd]() end
      cmd = 'nil'
    end        
    if hid_pocitani==true then hid_pocitadlo() end
    if dyka_pocitani==true then dyka_pocitadlo() end
    if skok_pocitani==true then skok_pocitadlo() end
    if krvko=='activated' and krvko_timer<getticks()-32000 then krvko='deactivated' UO.SysMessage('Krvko deactivated',5555) end
    if tarcurs_remover==1 and UO.TargCurs==true then tarcurs_remover=nil UO.Key('esc') end
    if (UO.MaxHits==0 or UO.MaxStam==0) and status_prodleva<getticks()-1000 then UO.Macro(8,2) UO.Macro(10,2) UO.ContPosX=821 UO.ContPosY=311 status_prodleva=getticks() end
    if UO.CharStatus:find("G") and warmode_alert<getticks()-10000 then warmode_alert=getticks() UO.SysMessage("Warning: Warmode ON") end
-- stats
    if ((status['Hits']~=UO.Hits and UO.MaxHits~=0) or (status['Stamina']~=UO.Stamina and UO.MaxStam~=0)) and UO.Hits~=nil and UO.Stamina~=nil and status_msg_wait<getticks()-status_msg_pauza then stats_hp() status['Hits']=UO.Hits status['Stamina']=UO.Stamina status_msg_wait=getticks() end
    if status['EnemyHits']~=UO.EnemyHits and UO.EnemyHits~=nil and UO.EnemyID~=0 and UO.EnemyID~=nil and status_msg_wait<getticks()-status_msg_pauza then stats_ehp() status['EnemyHits']=UO.EnemyHits status_msg_wait=getticks() end
    if (status['MinDmg']~=UO.MinDmg or status['MaxDmg']~=UO.MaxDmg) and (UO.MinDmg~=nil or UO.MaxDmg~=nil) and UO.MaxDmg~=0 and status_msg_wait<getticks()-status_msg_pauza then stats_dmg() status['MinDmg']=UO.MinDmg status['MaxDmg']=UO.MaxDmg status_msg_wait=getticks() end
    if (status['Weight']~=UO.Weight or status['MaxWeight']~=UO.MaxWeight) and (UO.Weight~=nil or UO.MaxWeight~=nil) and UO.MaxWeight~=0 and status_msg_wait<getticks()-status_msg_pauza then stats_weight() status['Weight']=UO.Weight status['MaxWeight']=UO.MaxWeight status_msg_wait=getticks() end
    if status['AR']~=UO.AR and UO.AR~=nil and status_msg_wait<getticks()-status_msg_pauza then stats_ar() status['AR']=UO.AR status_msg_wait=getticks() end
    if status['FR']~=UO.FR and UO.FR~=nil and (UO.FR<500 or UO.FR>(-500)) and status_msg_wait<getticks()-status_msg_pauza then stats_fr() status['FR']=UO.FR status_msg_wait=getticks() end
    if status['CR']~=UO.CR and UO.CR~=nil and (UO.CR<500 or UO.CR>(-500)) and status_msg_wait<getticks()-status_msg_pauza then stats_cr() status['CR']=UO.CR status_msg_wait=getticks() end
    if status['PR']~=UO.PR and UO.PR~=nil and (UO.PR<500 or UO.PR>(-500)) and status_msg_wait<getticks()-status_msg_pauza then stats_pr() status['PR']=UO.PR status_msg_wait=getticks() end
    if status['ER']~=UO.ER and UO.ER~=nil and (UO.ER<500 or UO.ER>(-500)) and status_msg_wait<getticks()-status_msg_pauza then stats_er() status['ER']=UO.ER status_msg_wait=getticks() end
-- journal
--    local jrnl=j:find('*vrh� d�ku*','|946|You see: '..tostring(jezditko['name'])..'|',' na skok.*')
    local jrnl=j:find('*vrh� d�ku*',' na skok.*','|1268|Tv� �toky jsou te mnohem bolestivja�|')
    if jrnl==1 then if dyka_pocitani~=true then dyka_pocitani=true dyka_cntr={} dyka_timer=getticks() j:clear() end end
--    if jrnl==2 and mount=='mounted' then UO.Macro(3,0,tostring(jezditko['name'])..(' come')) j:clear() mount='dismounted' end
    if jrnl==2 then if skok_pocitani~=true then skok_pocitani=true skok_cntr={} skok_timer=getticks() j:clear() end end
--    if jrnl==3 then krvko='activated' krvko_timer=getticks() UO.SysMessage('Krvko activated',5555) end
end
if gamma_prepinac=='on' then cm_gamma('off') end