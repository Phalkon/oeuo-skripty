-- Skript na hid
-- by Phalkon

dofile('functions/basic.lua')

function autorun_check()
 UO.Macro(32,0) UO.Macro(32,0) if UO.SysMsg == "Always Run is now off." then return ("OFF") elseif UO.SysMsg == "Always Run is now on." then return ("ON") end
end

function autorun_prepinac(prepinac)
 local chleba_s_maslem = autorun_check()
 if chleba_s_maslem ~= nil and prepinac ~= chleba_s_maslem then UO.Macro(32,0) end
end

function hid(cislo)
  warmode_cancel()
  UO.Macro(13,21) -- hid
  autorun_prepinac("ON")
  wait (500)
  if UO.SysMsg == "Nemas ani trosku chut delat neco takoveho" then return end
  if UO.SysMsg == "Nen� mo�n�, abys to dok�zala pr�v� te�." then return end
  if cislo == 0 and UO.SysMsg == "Mus� po�kat na dokon�en� jin� akce." then return end
  if cislo == 0 and UO.SysMsg == "Jsi p��li� zam�stn�n(a) bojem." then return end
  if cislo == 0 then cislo = 3 end
  odpocet(cislo)
end

function odpocet(cislo2)
  pocitadlo = cislo2
  repeat
    UO.ExMsg(UO.CharID,3,0,tostring(pocitadlo))
    local hid_timer = getticks() + 500
    if pocitadlo == 1 then autorun_prepinac("OFF") end
    while hid_timer > getticks() do
      wait (1)
      if getkey("ALT") and getkey("q") then hid(pocitadlo-1) end
    end
  pocitadlo = pocitadlo - 1
  until pocitadlo <= 0  
end

     
while true do
  wait (1)
  if getkey("ALT") and getkey("q") then hid(3) end
end