dofile('functions/FindItems.lua')
dofile('data/items.lua')
dofile('functions/communicator.lua')
dofile('data/looter.lua')             -- OLD LOOTER
--dofile('functions/hotkey_funcs.lua')  -- OLD LOOTER
---------------
---- VARS -----
---------------
local minimal_pause = 500

local bagl_id = 1074181592
local jezditko = {["name"]='Zdechlina',["id_velky"]=681726,["type_maly"]=8484}
local pet = {["name"]='Pan Chlupaty',["id_velky"]=548662,["type_maly"]=8478}
--local pet = {["name"]='Adelaida',["id_velky"]=576293,["type_maly"]=8444}
local uvaz_kul_id = 1074482137
local weapon_1H = {kratke_kopi["type"][1],kris["type"][1],dyka["type"][1],kratke_kopi["type"][2],kris["type"][2],dyka["type"][2]}
local strelne = {tezka_kuse["type"][1],skladany_luk["type"][1],elfi_luk["type"],tezka_kuse["type"][2],skladany_luk["type"][2]}
local stit = {kruhovy_kovovy_stit["type"]}

local warmode_alert=getticks()-10000
local tarcurs_remover={}

local hits = UO.Hits
local stamka = UO.Stamina
local DmgMin = UO.MinDmg
local DmgMax = UO.MaxDmg
local nalozeni = UO.Weight
local nalozeniMAX = UO.MaxWeight
local armor=UO.AR
local ohen=UO.FR
local mraz=UO.CR
local kyselina=UO.PR
local elektrina=UO.ER
local ehits = UO.EnemyHits
local enemy = UO.EnemyID
local variable = 1
---------------
---- FUNCS ----
---------------
function warmode_cancel() while UO.CharStatus:find("G") do UO.Macro(6,0) wait(50) end end
function hid_cancel() if hid_pocitani==true then hid_pocitani=false UO.ExMsg(UO.CharID,3,0,"hid: canceled") end end
function use_object(UObjectID) UO.LObjectID = UObjectID UO.Macro(17,0) end
function target_object(TObjectID,TObjectKind) UO.LTargetID = TObjectID UO.LTargetKind = TObjectKind UO.Macro(22,0) end
function dragndrop(id,stack,drop) UO.Drag(id,stack) time_to_run=getticks()+200 repeat wait(1) until UO.LLiftedID==id or time_to_run<getticks() UO.DropC(drop) end

function wait_for_gump(lag,timeout)
 time_to_run = getticks() + timeout
 local vstupX=UO.ContPosX local vstupY=UO.ContPosY
 repeat wait (lag) until UO.ContPosX~=vstupX or UO.ContPosY~=vstupY or time_to_run < getticks()
 if UO.ContPosX~=vstupX or UO.ContPosY~=vstupY then return true else return false end
end

function WaitForTarCurs(timeout)
 time_to_run = getticks() + timeout
 repeat wait (1) until UO.TargCurs == true or time_to_run < getticks()
 if UO.TargCurs == true then return true else return false end
end

function FindNUse(type,col)
local item = ScanItems(true,{Type=type,Col=col,Kind=0},{ContID=UO.CharID})
if item[1] ~= nil then use_object(item[1].ID) end
end

local hid_cntr = {}
function hid_pocitadlo()
  if UO.CharStatus:find("G") then hid_cancel() return end
  if hid_cntr[1]~=1 and (UO.SysMsg=="Nemas ani trosku chut delat neco takoveho" or UO.SysMsg=="Nen� mo�n�, abys to dok�zala pr�v� te�.") then
    hid_pocitani=false return end
  for f=1,4 do
    if hid_cntr[f]~=1 and hidtimer+500*f < getticks() then
      if f==4 then hid_pocitani=false break end
      hid_cntr[f]=1 UO.ExMsg(UO.CharID,3,0,"hid: "..4-f) break
    end
  end 
end

local command = 'wait'
local Funcs = {
---TARGETY
['Target Next']    =      function() UO.Macro(50,5) end,
['Attack Last']    =      function() UO.Macro(53,0) end,
['Target Previous'] =     function() UO.Macro(51,5) end,
['Attack Closest'] =      function() UO.Macro(52,5) UO.Macro(53,0) end,
---ZBRANE
['kopi']           =      function() local weapon = ScanItems(true,{Type=kopi["type"],Kind=0},{Col=2414})
                                     if weapon[1] ~= nil then
                                      for i=1,#weapon do if weapon[i].ContID~=UO.CharID then use_object(weapon[i].ID) tarcurs_remover=1 end end
                                     end end,
['1hand']          =      function() local weapon = ScanItems(true,{Type=weapon_1H,Kind=0},{ContID=UO.CharID})
                                     if weapon[1] ~= nil then
                                       local shield = ScanItems(true,{Type=stit,Kind=0},{ContID=UO.CharID})
                                       use_object(weapon[1].ID) tarcurs_remover=1
                                       if shield[1] ~= nil then wait(minimal_pause) use_object(shield[1].ID) end
                                     end end,
['bic']            =      function() local weapon = ScanItems(true,{Type=bic["type"],Kind=0},{ContID=UO.CharID})
                                     if weapon[1] ~= nil then
                                       local shield = ScanItems(true,{Type=stit,Kind=0},{ContID=UO.CharID})
                                       use_object(weapon[1].ID) tarcurs_remover=1
                                       if shield[1] ~= nil then wait(minimal_pause) use_object(shield[1].ID) end
                                     end end,
['ostep']          =      function() local weapon = ScanItems(true,{Type=kopi["type"],Kind=0,Col=2414})
                                     if weapon[1] ~= nil then
                                       for i=1,#weapon do if weapon[i].ContID==UO.CharID then return end end
                                       use_object(weapon[1].ID) tarcurs_remover=1
                                     end end,
['strelne']        =      function() local weapon = ScanItems(true,{Type=strelne,Kind=0},{ContID=UO.CharID})
                                     if weapon[1] ~= nil then use_object(weapon[1].ID) end end,
['disarm']         =      function() UO.Macro(24,1) end,
---ABILITKY
['krvko']          =      function() UO.Macro(1,0,'.startability 20') end,
['para']           =      function() local weapon = ScanItems(true,{Type=vrhaci_nuz["type"],Kind=0})
                                     if weapon[1] ~= nil then
                                       for i=1,#weapon do if weapon[i].ContID==UO.CharID then return end end
                                       local shield = ScanItems(true,{Type=stit,Kind=0})
                                       use_object(weapon[1].ID)
                                       if shield[1] ~= nil then
                                        if shield[1].ContID==UO.CharID then wait(100) wait(minimal_pause-100) use_object(shield[1].ID)
                                        else wait(minimal_pause) use_object(shield[1].ID) end
                                       end
                                     end end,
['srazeni']        =      function() local weapon = ScanItems(true,{Type=bic["type"],Kind=0},{ContID=UO.CharID})
                                     local shield = ScanItems(true,{Type=stit,Kind=0},{ContID=UO.CharID})
                                     if weapon[1] ~= nil then wait(minimal_pause) use_object(weapon[1].ID) end tarcurs_remover=1
                                     if shield[1] ~= nil then wait(minimal_pause) use_object(shield[1].ID) end
                                     UO.Macro(1,0,".startability 17") end,
['port']           =      function() UO.Macro(1,0,'.startability 25') end,
['nabij kusi']     =      function() UO.Macro(1,0,'.nabij') end,
['odpocinek']      =      function() UO.Macro(1,0,'.startability 4') end,
---LEKTVARY
['GH']             =      function() UO.Macro(1,0,".lecit") end,
['obnova']         =      function() UO.Macro(1,0,".obnova") end,
['protijed']       =      function() UO.Macro(1,0,".protijed") end,
['ocelka']         =      function() UO.Macro(1,0,".lektvar ocel") end,
['explosko']       =      function() UO.Macro(1,0,".lektvar vybuch") WaitForTarCurs(100) target_object(UO.CharID,1) end,
['hbitost']        =      function() UO.Macro(1,0,".lektvar hbitost") end,
['osvezko']        =      function() UO.Macro(1,0,".lektvar osvez") end,
['invisko']        =      function() UO.Macro(1,0,".lektvar neviditelnost") end,
['nezranko']       =      function() UO.Macro(1,0,".lektvar nezranitelnost") end,
['inko']           =      function() FindNUse(lektvar_skryti_povesti["type"],lektvar_skryti_povesti["col"]) end,
---SKILLY
['HID']            =      function() if hid_pocitani~=true then 
                                     hid_cntr = {} warmode_cancel() UO.Macro(13,21) hid_pocitani=true hidtimer=getticks() end end,
['detect hiding']  =      function() if hid_pocitani==true then hid_pocitani=false UO.ExMsg(UO.CharID,3,0,"hid: canceled") end UO.Macro(13,14) end,
['forensics']      =      function() UO.Macro(13,19) end,
['item identification'] = function() UO.Macro(13,3) end,
['stealing']       =      function() UO.Macro(13,33) end,
['provokace']      =      function() UO.Macro(13,22) end,
['usmirovani']     =      function() UO.Macro(13,9) end,
['lakani']         =      function() UO.Macro(13,15) end,
['tracking']       =      function() UO.Macro(13,38) end,
['track players']  =      function() UO.Macro(13,38) timeout = getticks() + 300
                                     repeat wait (1) until UO.ContName == "objpicker gump" or timeout < getticks()
                                     UO.Click(UO.CursorX,UO.CursorY,true,true,true,false)
                                     UO.Click(UO.ContPosX+172,UO.ContPosY+65,true,true,true,false)
                                     UO.Click(UO.ContPosX+172,UO.ContPosY+65,true,true,true,false) end,
['track animals']  =      function() UO.Macro(13,38) timeout = getticks() + 300
                                     repeat wait (1) until UO.ContName == "objpicker gump" or timeout < getticks()
                                     UO.Click(UO.CursorX,UO.CursorY,true,true,true,false)
                                     UO.Click(UO.ContPosX+64,UO.ContPosY+64,true,true,true,false)
                                     UO.Click(UO.ContPosX+64,UO.ContPosY+64,true,true,true,false) end,
---PET CONTROL
['all stay']       =      function() UO.Macro(3,0,'all stay') end,
['all come']       =      function() UO.Macro(3,0,'all come') end,
['all stop']       =      function() UO.Macro(3,0,'all stop') end,
['all go']         =      function() UO.Macro(3,0,'all go') end,
['pet do baglu']   =      function()   local zmensovak = ScanItems(true,{Type=lektvar_zmensovani["type"],Col=lektvar_zmensovani["col"],Kind=0})
                                       local obr = ScanItems(true,{ID=pet['id_velky'],Kind=1,Dist=3})
                                       if zmensovak[1]~=nil and obr[1]~=nil then dragndrop(zmensovak[1].ID,1,obr[1].ID) end
                                       wait (200) wait (minimal_pause-200)
                                       local mrnousek = ScanItems(true,{Type=pet['type_maly'],Kind=1,Dist=3})
                                       if mrnousek[1]~=nil then dragndrop(mrnousek[1].ID,1,UO.CharID) end
                                       end,
                                       
['pet z baglu']    =      function() local pet = ScanItems(true,{Type=pet['type_maly'],Kind=0})
                                     if pet[1] ~= nil then use_object(pet[1].ID) end end,
---MOUNT
['naskoc']         =      function() local mini = ScanItems(true,{Type=jezditko['type_maly'],Kind=0})
                                     if mini[1] ~= nil then use_object(mini[1].ID) wait(minimal_pause) end
                                     local kul = ScanItems(true,{ID=uvaz_kul_id,Dist=3,Kind=1})
                                     if kul[1] ~= nil then use_object(kul[1].ID) wait(minimal_pause) end
                                     local kun = ScanItems(true,{ID=jezditko['id_velky'],Dist=3})
                                     if kun[1] ~= nil then use_object(kun[1].ID) wait (100)
                                       if UO.SysMsg == "Nedos�hne� na tu bytost." or UO.SysMsg == "Takhle rychle po sesednut� nasednout nem��e�" then UO.Macro(3,0,tostring(jezditko['name'])..(" come")) end end
                                     if kul[1] ~= nil then wait(minimal_pause-100) dragndrop(kul[1].ID,1,UO.BackpackID) end
                                     if kun[1] == nil then
                                       local zabehlejkun = ScanItems(true,{ID=jezditko['id_velky'],Dist=10})
                                       if zabehlejkun[1] ~= nil then UO.Macro(3,0,tostring(jezditko['name'])..(" come"))
                                       else UO.SysMessage('Mount nenalezen!') end
                                     end end,
['seskoc']       =      function() UO.LObjectID=UO.CharID UO.Macro(17,0) UO.Macro(3,0,tostring(jezditko['name'])..(" stay")) end,
['uvaz']         =      function() local kun = ScanItems(true,{ID=jezditko['id_velky'],Dist=3})
                                   if kun[1] ~= nil then
                                     local kul = ScanItems(true,{ID=uvaz_kul_id})
                                     if kul[1].Kind == 0 or (kul[1].Dist <= 3 and (kul[1].X~=kun[1].X or kul[1].Y~=kun[1].Y or kul[1].Z~=kun[1].Z)) then
                                       UO.Drag(uvaz_kul_id) time_to_run=getticks()+200 repeat wait(1) until UO.LLiftedID==id or time_to_run<getticks() UO.DropG(kun[1].X,kun[1].Y,kun[1].Z)  wait (100)
                                       wait(minimal_pause-100) use_object(uvaz_kul_id) WaitForTarCurs(200) target_object(kun[1].ID,1)
                                     elseif kul[1].Kind == 1 and kul[1].Dist <= 3 and kul[1].X==kun[1].X and kul[1].Y==kun[1].Y and kul[1].Z==kun[1].Z then
                                       use_object(uvaz_kul_id) WaitForTarCurs(200) target_object(kun[1].ID,1) end
                                   else UO.SysMessage('Mount nenalezen!')
                                   end end,
---LOOTER
['loot mrtvoly']   =      function() lootuj("mrtvoly",loot,lootBagID) end,     -- OLD LOOTER
['loot target']   =      function() lootuj("target",loot,lootBagID) end,      -- OLD LOOTER
['loot all']   =      function() lootuj("target", "vse",UO.CharID) end,    -- OLD LOOTER
['loot vysyp']   =      function() roztrid(lootit,lootBagID) end,            -- OLD LOOTER
---MISCELLANEOUS
['quit uo']        =      function() UO.Macro(20,0) stop() end,
['open journal']   =      function() UO.Macro(8,3) end,
['open paperdoll'] =      function() UO.Macro(8,1) local pack=wait_for_gump(1,500) if pack==false then return end UO.ContPosX=808 UO.ContPosY=-16 end,
['open backpacks'] =      function() UO.NextCPosX=778 UO.NextCPosY=336 use_object(UO.BackpackID) local pack=wait_for_gump(1,500) if pack==false then return end
                                     UO.NextCPosX=0 UO.NextCPosY=0 use_object(1073936462) local pack=wait_for_gump(1,500) if pack==false then return end UO.ContPosX=777 UO.ContPosY=499 end,
--                                     UO.NextCPosX=0 UO.NextCPosY=0 use_object(1073895020) local pack=wait_for_gump(1,500) if pack==false then return end UO.ContPosX=634 UO.ContPosY=590 end,
['buff lista']     =      function() UO.Macro(1,0,'.buff_lista') end,
['dekorater']      =      function() UO.Macro(1,0,'.dekorater') end,
['SchopText']      =      function() UO.Macro(1,0,'.schoptext') local pack=wait_for_gump(1,500) if pack==true then UO.ContPosX=UO.CursorX-50 UO.ContPosY=UO.CursorY-70 end end,
---DALSI
['obvaz']          =      function() UO.Macro(1,0,'.obvaz') WaitForTarCurs(100) target_object(UO.CharID,1) end,
['usehand']        =      function() UO.Macro(1,0,'.usehand') end,
['otevri']         =      function() local doors = ScanItems(true,{Type=zavrene_dvere,Dist=2,RelZ={-5,-4,-3,-2,-1,0,1,2,3,4,5}})
                                     if doors[1] ~= nil then
                                      if doors[2]~=nil then
                                       if doors[1].Y==doors[2].Y then
                                        if math.abs(doors[1].RelX)<math.abs(doors[2].RelX) then use_object(doors[1].ID)
                                        elseif  math.abs(doors[1].RelX)>math.abs(doors[2].RelX) then use_object(doors[2].ID) end
                                       elseif doors[1].X==doors[2].X then
                                        if math.abs(doors[1].RelY)<math.abs(doors[2].RelY) then use_object(doors[1].ID)
                                        elseif  math.abs(doors[1].RelY)>math.abs(doors[2].RelY) then use_object(doors[2].ID) end
                                       else UO.SysMessage('Chyba') end
                                      else use_object(doors[1].ID) end 
                                     else UO.SysMessage('Dvere nenalezeny')
                                     end end,
['zavri']          =      function() local doors = ScanItems(true,{Type=otevrene_dvere,Dist=2,RelZ={-5,-4,-3,-2,-1,0,1,2,3,4,5}})
                                     for i = 1,#doors do if doors[i] ~= nil then if i > 1 then wait(minimal_pause) end use_object(doors[i].ID) end end end,
['kopat poklad']   =      function() UO.Macro(1,0,'.kopat_poklad') end,
['napln toulec']   =      function() UO.TargCurs=true UO.SysMessage('Vyber ��py',54) repeat wait(1) until UO.TargCurs==false
                                     local sipy = ScanItems(true,{ID=UO.LTargetID,Type={3903,7163},ContID=UO.BackpackID})
                                     local toulec = ScanItems(true,{Type=15193,ContID=UO.CharID})
                                     if sipy[1]~=nil and toulec[1]~=nil then for i=1,6 do if i > 1 then wait(minimal_pause) end use_object(toulec[1].ID) WaitForTarCurs(200) target_object(sipy[1].ID,1) end
                                     else UO.SysMessage('Tohle nejde',38) end end,
['vysyp toulec']   =      function() local toulec = ScanItems(true,{Type=15193,ContID=UO.CharID})
                                     if toulec[1]~=nil and toulec[1].Details~='Prazdny' then for i=1,6 do if i > 1 then wait(minimal_pause) end use_object(toulec[1].ID) WaitForTarCurs(200) target_object(toulec[1].ID,1) end
                                     else UO.SysMessage('Nem� toulec s ��py',38) end end,
}
---------------
-- MAIN LOOP --
---------------
while UO.CliLogged==true do
  -----vyta�en� statusu-------
  if UO.MaxHits == 0 or UO.MaxStam == 0 then UO.Macro(8,2) UO.Macro(10,2) UO.ContPosX=821 UO.ContPosY=311 end 
  ------------------------
  repeat
    setatom('Request','true')
    while getatom('Request') == 'true' do wait(1) end
    cmd = getatom('Request')
    if cmd ~= 'nil' then
      if Funcs[cmd] then Funcs[cmd]() end
      cmd = 'nil'
    end        
    if hid_pocitani==true then hid_pocitadlo() end
    if tarcurs_remover==1 and UO.TargCurs==true then tarcurs_remover=nil UO.Key('esc') end
    if UO.CharStatus:find("G") and warmode_alert<getticks()-10000 then warmode_alert=getticks() UO.SysMessage("Warning: Warmode ON") end
  until (hits~=UO.Hits and UO.Hits~=nil) or (stamka~=UO.Stamina and UO.Stamina~=nil) or (DmgMin~=UO.MinDmg and UO.MinDmg~=nil) or (DmgMax~=UO.MaxDmg and UO.MaxDmg~=nil) or (armor~=UO.AR and UO.AR~=nil) or (ohen~=UO.FR and UO.FR~=nil) or (mraz~=UO.CR and UO.CR~=nil) or (kyselina~=UO.PR and UO.PR~=nil) or (elektrina~=UO.ER and UO.ER~=nil) or (nalozeni~=UO.Weight and UO.Weight~=nil) or (nalozeniMAX~=UO.MaxWeight and UO.MaxWeight~=nil) or (ehits~=UO.EnemyHits and UO.EnemyID~=nil)
  ---------------------------------------------------------------
  ----------------- S T A T S -----------------------------------
  ---------------------------------------------------------------
  ----CharHP+CharStam----------
  if (hits~=UO.Hits and UO.Hits~=nil) or (stamka~=UO.Stamina and UO.Stamina~=nil) then
    if UO.MaxHits~=0 and UO.MaxStam~=0 then
      UO.ExMsg (UO.CharID,3,35,tostring(UO.Hits)..(" / ")..(UO.Stamina)) end -- mozna zmena na barvu: 38
--      if hits~=UO.Hits and UO.MaxHits~=0 and UO.MaxStam~=0 then if hits>UO.Hits then UO.SysMessage("Combat: -"..hits-UO.Hits.." HP",38) end end
      if UO.Hits==0 and UO.MaxHits~=0 and UO.MaxStam~=0 then cm_screen() end
    hits=UO.Hits
    stamka=UO.Stamina
  end
  ----EnemyHP------------------
  if ehits~=UO.EnemyHits and UO.EnemyID~=0 then
    if UO.EnemyHits==0 then if vypocet~=100 then UO.SysMessage("VARZDAAAAA!",38) cm_screen() end end
    if UO.EnemyHits==4 and UO.EnemyID~=enemy then variable=50 end   
    vypocet=(UO.EnemyHits/2)*variable if vypocet > 100 then vypocet = 100 end
    if UO.EnemyHits~=0 then UO.ExMsg (UO.EnemyID,3,57,tostring((vypocet)..("%"))) end
    ehits=UO.EnemyHits
    enemy=UO.EnemyID
    variable=1
  end
  -----DMG-------------
  if (DmgMin ~= UO.MinDmg or DmgMax ~= UO.MaxDmg) and (UO.MinDmg ~= nil or UO.MaxDmg ~= nil) then
    if UO.MaxDmg~=0 then
      UO.ExMsg (UO.CharID,3,1259,tostring("dmg: ")..(UO.MinDmg)..(" - ")..(UO.MaxDmg)) end
    DmgMin = UO.MinDmg
    DmgMax = UO.MaxDmg
  end
  -----Nalozeni-------------
  if (nalozeni~=UO.Weight or nalozeniMAX~=UO.MaxWeight) and (UO.Weight~=nil or UO.MaxWeight~=nil) then
    if UO.MaxWeight~=0 then
      UO.ExMsg (UO.CharID,3,1378,tostring("nalozeni: ")..(UO.Weight)..(" / ")..(UO.MaxWeight)) end
    nalozeni = UO.Weight
    nalozeniMAX = UO.MaxWeight
  end
  -----Armor-------------
  if armor~=UO.AR and UO.AR~=nil then
    UO.ExMsg (UO.CharID,3,1673,tostring("armor: ")..(UO.AR))
    armor=UO.AR
  end
  -----Resisty-------------
  if ohen~=UO.FR and UO.FR~=nil then
    if UO.FR<500 or UO.FR>(-500) then
      UO.ExMsg (UO.CharID,3,40,tostring("ohen: ")..(UO.FR)) end
    ohen=UO.FR
  end
  if mraz~=UO.CR and UO.CR~=nil then
    if UO.CR<500 or UO.CR>(-500) then
      UO.ExMsg (UO.CharID,3,1266,tostring("mraz: ")..(UO.CR)) end
    mraz=UO.CR
  end
  if kyselina~=UO.PR and UO.PR~=nil then
    if UO.PR<500 or UO.PR>(-500) then
      UO.ExMsg (UO.CharID,3,1269,tostring("kyselina: ")..(UO.PR)) end
    kyselina=UO.PR
  end
  if elektrina~=UO.ER and UO.ER~=nil then
    if UO.ER<500 or UO.ER>(-500) then
      UO.ExMsg (UO.CharID,3,54,tostring("elektrina: ")..(UO.ER)) end
    elektrina=UO.ER
  end
end