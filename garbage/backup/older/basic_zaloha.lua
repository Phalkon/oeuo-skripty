-- Basic Lua functions for Open EasyUO
-- by Phalkon

-- CORE FUNCTION
function wait_until(podminka,timeout)
 if timeout == nil then repeat wait (1) until podminka == true 
 else time_to_run = getticks() + timeout
 repeat wait (1) until podminka == true or time_to_run < getticks() end
end
----------------

-- MiniFunctions
function makelast() UO.Msg(".makelast"..'\013\010') end
function backpack_check() UO.LObjectID = UO.BackpackID UO.Macro(17,0) end
function warmode_cancel() if UO.CharStatus:find("G") then UO.Macro(6,0) end end
function use_object(UObjectID) UO.LObjectID = UObjectID UO.Macro(17,0) end
function target_object(TObjectID,TObjectKind) UO.LTargetID = TObjectID UO.LTargetKind = TObjectKind UO.Macro(22,0) end
function WaitForTarCurs(for_how_long) if for_how_long == nil then for_how_long = 500 end wait_until(UO.TargCurs == true,for_how_long) end
function dragndrop(id,stack,drop) UO.Drag(id,stack) wait_until(UO.LLiftedID == id,200) UO.DropC(drop) end

------------- FUNCTIONS ---------------
---------------------------------------

-- FINDITEM
-- local item = finditem("id","type","kind","contid","x","y","z","stack","rep","col")
ignorelist = {}
function finditem(id,type,kind,contid,x,y,z,stack,rep,col)
  ncnt = UO.ScanItems(true)
  for nindex = 0,(ncnt-1) do
    local nid,ntype,nkind,ncontid,nx,ny,nz,nstack,nrep,ncol = UO.GetItem(nindex)
    if (nid == id or id == "id") and (ntype == type or type == "type") and (nkind == kind or kind == "kind") and (ncontid == contid or contid == "contid") and (nx == x or x == "x") and (ny == y or y == "y") and (nz == z or z == "z") and (nstack == stack or stack == "stack") and (nrep == rep or rep == "rep") and (ncol == col or col == "col") and ignorelist[nid] == nil then
      return {nid,ntype,nkind,ncontid,nx,ny,nz,nstack,nrep,ncol}
    end
  end
  return nil
end

-- TARGETCURS CANCEL
function target_cancel(parametr)
  if parametr == "maybe" then
    if UO.TargCurs == true then
      UO.TargCurs = false
    end
  end
  if parametr == "certain" then
   local timer = getticks() + 300
    repeat wait (1) until UO.TargCurs == true or timer < getticks()
    UO.TargCurs = false
  end
end

-- LOGOUT COUNTDOWN

function logout_coutdown(coutdown)
for cntdwn = coutdown,0,-1 do
UO.SysMessage("Logout countdown: "..tostring(cntdwn),155) wait (1000) end
UO.Macro(8,1)
repeat wait(100) until UO.CharID==UO.ContID
UO.ContPosX = 830
UO.ContPosY = -25
UO.Click(1041,78,true,true,true,false) wait(100)
UO.Click(719,392,true,true,true,false)
end