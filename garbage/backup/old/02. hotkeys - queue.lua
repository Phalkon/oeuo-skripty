---------------
---- VARS -----
---------------
local queue_wait=getticks()
local minimal_pause=500
--local command={}
---------------
---- FUNCS ----
---------------
queue = {}
queue.mt = {
     __index = queue,
}
 
function queue.New()
   local self = {}
   setmetatable(self,queue.mt)
   return self
end
 
function queue:push(s)
   if #self>0 then for i=1,#self do if self[i]:find(s) then return end end end -- brani pridani akce, pokud je uz ve fronte
   table.insert(self, 1, s)
end
 
function queue:dequeue()
   local s
   if #self > 0 and queue_wait<getticks()-minimal_pause then
        s = self[#self]
        self[#self] = nil
        queue_wait=getticks()
   end
   return s
end
---------------
---- INIT -----
---------------
local Commands = queue.New()
---------------
-- MAIN LOOP --
--------------- 
while UO.CliLogged==true do
   command = getatom('CMD')
   if command ~= 'wait' then
                setatom('CMD','wait')
                Commands:push(command) --[[ UO.SysMessage('push '.. command,5555) ]]
                command = 'wait'
   else wait(1) end
   if getatom('Request') == 'true' then
      command = Commands:dequeue()
      if command then setatom('Request',command) --[[ UO.SysMessage(command,5555) ]] else setatom('Request','nil') end
      command = 'wait'
   end
end