-- SCANJOURNAL
function scanjournal (radky)
  local nNewRef,nCnt = UO.ScanJournal(0)
  if radky > nCnt then radky = nCnt end
  for nindex = 0,radky do
    local sLine,nCol = UO.GetJournal(nindex)
    return {sLine,nCol}
  end
end