-- Functions for looter
-- by Phalkon

--dofile('basic.lua')

local vylooceno = {}

function wait_until(podminka,lag,timeout)
 if timeout == nil then repeat wait (lag) until podminka == true 
 else time_to_run = getticks() + timeout
 repeat wait (lag) until podminka == true or time_to_run < getticks() end
end

--miniFunctions
function use_object(UObjectID) UO.LObjectID = UObjectID UO.Macro(17,0) end
function open_body(body) while UO.ContID~=body do UO.NextCPosX=0 UO.NextCPosY=0 use_object(body) wait_until(UO.ContID==body,10,1000) end contas = body vylooceno[body] = 1 end
function looter_msg(msg,col) UO.SysMessage("Looter: "..msg,col) end
function check_weight(rezerva) if UO.Weight+rezerva >= UO.MaxWeight then looter_msg("Zb�v� "..UO.MaxWeight-UO.Weight.." kamen�",1) end end 
function dragndrop(id,stack,drop) UO.Drag(id,stack) time_to_run=getticks()+200 repeat wait(1) wait(minimal_pause-1) until UO.LLiftedID==id or time_to_run<getticks() UO.DropC(drop) end

--MAIN FUNCTION
function lootuj(kde,co,kam)
 if kde == "mrtvoly" then
  local mrtvola = ScanItems(true,{Type=8198,Kind=1,Dist=1})
  for i=1,#mrtvola do
   if mrtvola[i]~=nil and vylooceno[mrtvola[i].ID]==nil then
   open_body(mrtvola[i].ID) wait_until(UO.ContID==contas,10,1000) break end
  end
 end
 if kde == "target" then
  UO.TargCurs = true
  while UO.TargCurs == true do wait(100) end
  open_body(UO.LTargetID)
  wait_until(UO.ContID==contas,10,1000)
 end
-- if not UO.CharStatus:find("H") then warmode_cancel() UO.Macro(13,21) end
-- wait (200)
 if co == "vse" then
  local VeciKLootu = ScanItems(true,{ContID=contas},{Type=dontloot})
  if VeciKLootu~=nil then
    for i=1,#VeciKLootu do dragndrop(VeciKLootu[i].ID,VeciKLootu[i].Stack,kam) wait(500) wait(ltr_lag_pause) end
  end
 end
 if co ~= "vse" then
  local VeciKLootu = ScanItems(true,{Type=co,ContID=contas})
  if VeciKLootu~=nil then
    for i=1,#VeciKLootu do if contas==UO.BackpackID then break end dragndrop(VeciKLootu[i].ID,VeciKLootu[i].Stack,kam) wait(500) wait(ltr_lag_pause) end
  end
 end
check_weight(20)
looter_msg("Hotovo",60)
end

function roztrid(co,odkud)
  use_object(odkud)
  wait_until(UO.ContID==odkud,200,1000)
  wait (200)
  local ncnt = UO.ScanItems(true)
  for nindex = 0,(ncnt-1) do
    local nid,ntype,nkind,ncontid,nx,ny,nz,nstack,nrep,ncol = UO.GetItem(nindex)
    for i1 = 1,#co do for i2 = 1,#co[i1] do
      if ntype == co[i1][i2]["type"] and ncontid == odkud and (ncol == co[i1][i2]["col"] or co[i1][i2]["col"] == "col") and co[i1]["storeCont"] ~= nil then
        dragndrop(nid,nstack,co[i1]["storeCont"]) wait(500) wait(ltr_lag_pause)
      end
    end end
  end
looter_msg("Hotovo",60)
end