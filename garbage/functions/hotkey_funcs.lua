dofile('basic.lua')
dofile('journal.lua')

local hid_cntr = {}

function reload_data()
hits = UO.Hits
stamka = UO.Stamina
DmgMin = UO.MinDmg
DmgMax = UO.MaxDmg
nalozeni = UO.Weight
nalozeniMAX = UO.MaxWeight
armor=UO.AR
ohen=UO.FR
mraz=UO.CR
kyselina=UO.PR
elektrina=UO.ER
ehits = UO.EnemyHits
enemy = UO.EnemyID
variable = 1
end

function keys_enabled()
wait(200) UO.Msg("/HOTKEYS ZAPNUTY") for i=1,140 do UO.Key("space") end
reload_data()
end

function keys_disabled(var)
ClearUOMsg() wait(100)
if var=="message" then UO.Msg("")
elseif var=="emote" then UO.Msg(": ")
elseif var=="party" then UO.Msg("/") end
--UO.SysMessage("HOTKEYS VYPNUTY")
while true do wait (1)
if getkey("enter") then keys_enabled() return end
end end

function pocitadla()
------------ hid pocitadlo -----------------------  
 if hid_pocitani==true then
  if UO.CharStatus:find("G") then hid_pocitadlo=nil hid_pocitani=false UO.ExMsg(UO.CharID,3,0,"hid: canceled") end
  if hid_pocitadlo==0 and UO.SysMsg=="Nemas ani trosku chut delat neco takoveho" then hid_pocitadlo=nil end
  if hid_pocitadlo==0 and UO.SysMsg=="Nen� mo�n�, abys to dok�zala pr�v� te�." then hid_pocitadlo=nil end
  if hid_pocitadlo==0 and hidtimer+500 < getticks() then hid_pocitadlo=1 UO.ExMsg(UO.CharID,3,0,"hid: 3") end
  if hid_pocitadlo==1 and hidtimer+1000 < getticks() then hid_pocitadlo=2 UO.ExMsg(UO.CharID,3,0,"hid: 2") end
  if hid_pocitadlo==2 and hidtimer+1500 < getticks() then hid_pocitadlo=3 UO.ExMsg(UO.CharID,3,0,"hid: 1") end
  if hid_pocitadlo==3 and hidtimer+2000 < getticks() then hid_pocitadlo=nil hid_pocitani=false end
 end
--------------------------------------------------
------------ lektvary pocitadlo -----------------------
 if lektvar_pocitani==true then  
  if lektvar_pocitadlo==0 then wait(100)
    if UO.SysMsg:find("Zpr�va: Nem� u sebe") or UO.SysMsg:find("Nem��e� p�t dal�� lektvar tak rychle po p�edchoz�m lektvaru") then lektvar_pocitadlo=nil lektvar_pocitani=false
    else UO.ExMsg(UO.CharID,3,0,"potka: "..tostring(math.ceil((lektvar_timer-getticks())/1000))) lektvar_pocitadlo=1 end end
  if lektvar_pocitadlo==1 and lektvar_timer < getticks()+5000 then UO.ExMsg(UO.CharID,3,0,"potka: 5") lektvar_pocitadlo=2 end
  if lektvar_pocitadlo==2 and lektvar_timer < getticks()+4000 then UO.ExMsg(UO.CharID,3,0,"potka: 4") lektvar_pocitadlo=3 end
  if lektvar_pocitadlo==3 and lektvar_timer < getticks()+3000 then UO.ExMsg(UO.CharID,3,0,"potka: 3") lektvar_pocitadlo=4 end
  if lektvar_pocitadlo==4 and lektvar_timer < getticks()+2000 then UO.ExMsg(UO.CharID,3,0,"potka: 2") lektvar_pocitadlo=5 end
  if lektvar_pocitadlo==5 and lektvar_timer < getticks()+1000 then UO.ExMsg(UO.CharID,3,0,"potka: 1") lektvar_pocitadlo=6 end
  if lektvar_pocitadlo==6 and lektvar_timer < getticks() then UO.ExMsg(UO.CharID,3,5555,"potka: ready") lektvar_pocitadlo=nil lektvar_pocitani=false end
 end
--------------------------------------------------  
------------ skok pocitadlo -----------------------
 if skok_pocitani==true then
  if skok_pocitadlo==0 then UO.ExMsg(UO.CharID,3,0,"skok: 4") skok_pocitadlo=1 end
  if skok_pocitadlo==1 and skok_timer < getticks()+3000 then UO.ExMsg(UO.CharID,3,0,"skok: 3") skok_pocitadlo=2 end
  if skok_pocitadlo==2 and skok_timer < getticks()+2000 then UO.ExMsg(UO.CharID,3,0,"skok: 2") skok_pocitadlo=3 end
  if skok_pocitadlo==3 and skok_timer < getticks()+1000 then UO.ExMsg(UO.CharID,3,0,"skok: 1") skok_pocitadlo=4 end
  if skok_pocitadlo==4 and skok_timer < getticks() then UO.ExMsg(UO.CharID,3,5555,"skok!") skok_pocitadlo=nil skok_pocitani=false end
 end
---------------------------------------------------------------
------------ dyka pocitadlo -----------------------
 if dyka_pocitani==true then
  if dyka_pocitadlo==0 then UO.ExMsg(UO.CharID,3,0,"dyka: 4") dyka_pocitadlo=1 end
  if dyka_pocitadlo==1 and dyka_timer < getticks()+3000 then UO.ExMsg(UO.CharID,3,0,"dyka: 3") dyka_pocitadlo=2 end
  if dyka_pocitadlo==2 and dyka_timer < getticks()+2000 then UO.ExMsg(UO.CharID,3,0,"dyka: 2") dyka_pocitadlo=3 end
  if dyka_pocitadlo==3 and dyka_timer < getticks()+1000 then UO.ExMsg(UO.CharID,3,0,"dyka: 1") dyka_pocitadlo=4 end
  if dyka_pocitadlo==4 and dyka_timer < getticks() then UO.ExMsg(UO.CharID,3,5555,"dyka!") dyka_pocitadlo=nil dyka_pocitani=false end
 end
end

-- ZBRAN�
function zbran(type_zbrane)
local weapon = ScanItems(true,{Type=type_zbrane,Kind=0},{ContID=UO.CharID})
if weapon[1] ~= nil then use_object(weapon[1].ID) end
end

function nahod_ostep()
-- local nazemi = ScanItems(true,{Type=kopi["type"],Kind=1,Dist=3,Col=2414})
-- if nazemi[1] ~= nil then use_object(nazemi[1].ID)
 local weapon = ScanItems(true,{Type=kopi["type"],Kind=0,Col=2414})
 if weapon[1] ~= nil then use_object(weapon[1].ID) end
end

function nahod_kopi(type_kopi)
local weapon = ScanItems(true,{Type=kopi["type"],Kind=0},{Col=2414})
if weapon[1] ~= nil then use_object(weapon[1].ID) end
end

function lektvar(type_potionu,col_potionu)
  local potion = ScanItems(true,{Type=type_potionu,Col=col_potionu,Kind=0})
  if potion[1] ~= nil then use_object(potion[1].ID) end
end

function dvere(zavrene_otevrene)
  local doors = ScanItems(true,{Type=zavrene_otevrene,Dist=2,RelZ={-2,-1,0,1,2}})
  for i = 1,#doors do
   if doors[i] ~= nil then use_object(doors[i].ID) end
  end
end

function pet_do_baglu(velkej_id,malej_type)
  local zmensovak = ScanItems(true,{Type=lektvar_zmensovani["type"],Col=lektvar_zmensovani["col"],Kind=0})
  local obr = ScanItems(true,{ID=velkej_id,Kind=1,Dist=3})
  if zmensovak[1]~=nil and obr[1]~=nil then dragndrop(zmensovak[1].ID,1,velkej_id) end
  wait (200)
  local mrnousek = ScanItems(true,{Type=malej_type,Kind=1,Dist=3})
  if mrnousek[1]~=nil then dragndrop(mrnousek[1].ID,1,UO.CharID) end
end

-- MOUNT
function naskoc()
local mini = ScanItems(true,{Type=jezditko_mini,Kind=0})
if mini[1] ~= nil then use_object(mini[1].ID) end
local kul = ScanItems(true,{ID=uvaz_kul_id,Dist=3,Kind=1})
if kul[1] ~= nil then use_object(uvaz_kul_id) end
local kun = ScanItems(true,{ID=jezditko_id,Dist=3})
if kun[1] ~= nil then use_object(jezditko_id) wait (100)
if UO.SysMsg == "Nedos�hne� na tu bytost." or UO.SysMsg == "Takhle rychle po sesednut� nasednout nem��e�" then UO.Macro(3,0,tostring(jezditko_name)..(" come")) end
end
if kul[1] ~= nil then dragndrop(uvaz_kul_id,1,UO.BackpackID) end
if kun[1] == nil then
local zabehlejkun = ScanItems(true,{ID=jezditko_id,Dist=10})
if zabehlejkun[1] ~= nil then UO.Macro(3,0,tostring(jezditko_name)..(" come")) end
end
end

function seskoc()
UO.LObjectID = UO.CharID
UO.Macro(17,0)
UO.Macro(3,0,tostring(jezditko_name)..(" stay"))
end

function uvaz()
local kun = ScanItems(true,{ID=jezditko_id,Dist=3})
if kun[1] ~= nil then
local kul = ScanItems(true,{ID=uvaz_kul_id})
if kul[1].Kind == 0 or (kul[1].Dist <= 3 and (kul[1].X~=kun[1].X or kul[1].Y~=kun[1].Y or kul[1].Z~=kun[1].Z)) then
UO.Drag(uvaz_kul_id) wait_until(UO.LLiftedID == uvaz_kul_id,1,200) UO.DropG(kun[1].X,kun[1].Y,kun[1].Z)  wait (100)
use_object(uvaz_kul_id) WaitForTarCurs(200) target_object(jezditko_id,1)
elseif kul[1].Kind == 1 and kul[1].Dist <= 3 and kul[1].X==kun[1].X and kul[1].Y==kun[1].Y and kul[1].Z==kun[1].Z then
use_object(uvaz_kul_id) WaitForTarCurs(200) target_object(jezditko_id,1)
end
else
seskoc() wait(100) uvaz()
end 
end

function useskill_tracking(co_trackovat)
 UO.Macro(13,38) timeout = getticks() + 300
 repeat wait (1) until UO.ContName == "objpicker gump" or timeout < getticks()
 if co_trackovat=="hraci" then
  UO.Click(UO.ContPosX+172,UO.ContPosY+65,true,true,true,false)
  UO.Click(UO.ContPosX+172,UO.ContPosY+65,true,true,true,false)
--  UO.Click(UO.ContPosX+172,UO.ContPosY+65,false,true,true,false)
 end
 if co_trackovat=="zvirata" then
  UO.Click(UO.ContPosX+64,UO.ContPosY+64,true,true,true,false)
  UO.Click(UO.ContPosX+64,UO.ContPosY+64,true,true,true,false)
--  UO.Click(UO.ContPosX+64,UO.ContPosY+64,false,true,true,false)
 end
end

function pasti()
 local past = ScanItems(true,{ID=UO.LTargetID,Type=4113,Dist=3})
 if past[1]~=nil then UO.Popup(past[1].ID,0,0)
-- wait for popup ---
      local time_to_wait = getticks() + 200
      repeat wait (10) until (UO.ContID==0 and UO.ContPosX==0 and UO.ContPosY==0) or time_to_wait < getticks()
---------------------
      UO.Click(50,40,true,true,true,false)
 end
end
-- NAHO� A SHO� ZBROJ
local bagl_id = 1074354748
local zbroj_type = {{5399,0},{5054,0},{5055,0},{5062,961},{5076,0},{5078,0},{7947,0}}

function prezbrojeni(nahod_shod)
  if nahod_shod == "nahod" then
    use_object(bagl_id)
    wait (200)
    for i = 1,#zbroj_type do     
      local item = finditem("id",zbroj_type[i][1],"kind","contid","x","y","z","stack","rep",zbroj_type[i][2])
      if item ~= nil then
        use_object(item[1]) wait (500)
      end
    end
  end
  if nahod_shod == "shod" then
    for i = #zbroj_type,1,-1 do      
      local item = finditem("id",zbroj_type[i][1],"kind","contid","x","y","z","stack","rep",zbroj_type[i][2])
      if item ~= nil then
        dragndrop(item[1],item[8],bagl_id) wait (500)
      end
    end  
  end
end

function toulec(arg)
local toulec = ScanItems(true,{Type=15193,ContID=UO.CharID})
local sipcnt = {}
if toulec[1].Details=="Prazdny" then sipcnt=0
else
 local a = toulec[1].Details
 local a = "\r\n"..a
 for s,n in a:gmatch("%c+([^%c]-)%s([%d%.]+)") do sipcnt=n/50 end
end
if toulec[1]~=nil then
 if arg=="napln" then
  for i=math.floor(sipcnt),5 do
   local sipy = ScanItems(true,{Type={3903,7163},ContID=UO.BackpackID})
   if sipy[1]~=nil then use_object(toulec[1].ID) WaitForTarCurs(200) target_object(sipy[1].ID,1)
   else break end
  end
 end
 if arg=="vyprazdni" then
  for i=1,math.ceil(sipcnt) do
   use_object(toulec[1].ID) WaitForTarCurs(200) target_object(toulec[1].ID,1)
  end
 end
end
wait(200)
local toulec = ScanItems(true,{Type=15193,ContID=UO.CharID})
UO.SysMessage("toulec: " .. toulec[1].Details)
end