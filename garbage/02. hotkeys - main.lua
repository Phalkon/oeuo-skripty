dofile('../functions/FindItems.lua')
dofile('../functions/journal.lua')
dofile('../functions/communicator.lua')
---------------
---- VARS -----
---------------
local password = 'gGyZh4ei6YRxHY8V'
local minimal_pause = 500
local loop_wait=50
local ignore = {
'|54|Save sv�ta za 10 vte�in|',
'|54|Sv�t se na chv�li zastavil...|',
'|54|Sv�t se zase za�al to�it|',
'|0|It begins to snow.|',
'|0|It begins to rain.|',
'|38|Lilien: a|AFK MODE|',
'|99|You see: Vernon|',
'|99|You see: Erek|',
'|946|You see: Lensar|',
'|946|You see: Carlton|',
'|946|You see: Zachariah|',
'|946|You see: Abdiel|',
'|946|You see: Agustin|',
'|946|You see: Filbert|',
'|946|You see: Lili|',
'|946|You see: Gathi|',
'|946|You see: orel|',
'|946|You see: pralesni papousek|',
'|946|You see: vrana|',
'Vid�, jak Lili prch�',
'Vid�, jak Gathi prch�',
'|946|You see: Hart|',
'|54|Tve zlociny byly promlceny|',
'|38|You see: Edwin|',
'|38|You see: York|',
'|946|You see: Blahoslav|',
'|946|You see: Denton|',
'|946|You see: Harry|',
'|946|You see: Li|',
'|946|You see: Paxton|',
'Nestujte tak blizko.',
'Hej, krok zpatky.',
'Postojite kousek ode mne?',
'Nechci problemy, ale: Krok zpet!',
'Proc stojis tak blizko ke mne?',
--'pop�j� l�hev koYalky',
}
function afk_msg() --[[ UO.Macro(2,0,'pop�j� l�hev ko�alky') ]] end
--local command={}
local modkeys = {'ctrl','alt','shift','space'}
local hotkeys = {
---ZBRANE
[{'a','ctrl'}]          =    'kopi',
[{'s','ctrl'}]          =    'bic',
[{'d','ctrl'}]          =    '1hand',
[{'z','ctrl'}]          =    'ostep',
[{'x','ctrl'}]          =    'kuse',
[{'c','ctrl'}]          =    'luk',
[{'v','ctrl'}]          =    'disarm',
[{'f3','space'}]        =    're-equip',
---ABILITKY
[{'s'}]                 =    'krvko',
[{'s','shift'}]         =    'para',
[{'s','space'}]         =    'srazeni',
[{'f','space'}]         =    'poison',
[{'w'}]                 =    'port',
[{'x'}]                 =    'nabij kusi',
[{'z'}]                 =    'odpocinek',
---LEKTVARY
[{'a','alt'}]           =    'obnova',
[{'s','alt'}]           =    'GH',
[{'d','alt'}]           =    'ocelka',
[{'w','alt'}]           =    'explosko - drop',
[{'w','shift'}]         =    'explosko - throw',
[{'e','alt'}]           =    'protijed',
[{'z','shift'}]         =    'osvezko',
[{'c','shift'}]         =    'sila',
[{'x','shift'}]         =    'hbitost',
[{'e','space'}]         =    'dispell',
[{'q','space'}]         =    'invisko',
[{'d','space'}]         =    'nezranko',
[{'v','shift'}]         =    'inko',
[{'c','alt'}]           =    'wine',
---SKILLY
[{'q','alt'}]           =    'HID',
[{'q','shift','alt'}]   =    'fast HID',
[{'v'}]                 =    'detect hiding',
[{'p'}]                 =    'forensics',
[{'i'}]                 =    'item identification',
[{'i','alt'}]           =    'animal lore',
[{'k','alt'}]           =    'animal taming',
[{'t','space'}]         =    'anatomy',
[{'y','space'}]         =    'eval int',
[{'u'}]                 =    'spirit speak',
[{'j'}]                 =    'cmuchani',
[{'k'}]                 =    'stealing',
[{'1','shift'}]         =    'provokace',
[{'2','shift'}]         =    'usmirovani',
[{'3','shift'}]         =    'lakani',
[{'l'}]                 =    'tracking',
[{'f'}]                 =    'track players',
[{'l','alt'}]           =    'track animals',
---MOUNT
[{'a'}]                 =    'naskoc',
[{'a','shift'}]         =    'seskoc',
[{'a','shift','alt'}]   =    'uvaz',
---PET CONTROL
[{'e','shift','alt'}]   =    'pet z baglu',
[{'d','shift','alt'}]   =    'pet do baglu',
---LOOTER
[{'f1'}]                =    'loot mrtvoly',
[{'f2'}]                =    'loot target',
[{'f3'}]                =    'loot all',
[{'f4'}]                =    'loot vysyp',
---MISCELLANEOUS
[{'j','space'}]         =    'toggle sound',
[{'r'}]                 =    'always run',
[{'y'}]                 =    'use hand',
[{'t','alt'}]           =    'transparency',
[{'f12'}]               =    'open journal',
[{'f11'}]               =    'open map',
[{'f10'}]               =    'open paperdoll',
[{'f5'}]                =    'open backpacks',
[{'f9'}]                =    'buff lista',
[{'f8'}]                =    'dekorater',
[{'v','space'}]         =    'SchopText',
[{'g','space'}]         =    'gamma - on',
[{'g','shift','space'}] =    'gamma - off',
[{'m','space'}]         =    'mapa - on',
--[{'m','alt','space'}] =    'mapa - off',
[{'u','space'}]         =    'print skills',
---DALSI
[{'d'}]                 =    'obvaz',
[{'z','ctrl','shift'}]  =    'kuchani',
[{'z','alt','shift'}]   =    'hlava',
[{'q'}]                 =    'otevri',
[{'q','shift'}]         =    'zavri',
[{'h'}]                 =    'kopat poklad',
[{'o','alt'}]           =    'napln toulec',
[{'p','alt'}]           =    'vysyp toulec',
[{'f6'}]                =    'prelejvator',
[{'h','alt'}]           =    'hide item',
[{'j','alt'}]           =    'resync',
}
local no_action_hotkeys = {
---TALKING
[{'m'}]                 =    function() keys_disabled('message') end,
[{'m','alt'}]           =    function() keys_disabled('whisper') end,
[{'m','shift'}]         =    function() keys_disabled('yell') end,
[{'n'}]                 =    function() keys_disabled('emote') end,
[{'b'}]                 =    function() keys_disabled('party') end,
---HESLA
[{'9'}]                 =    function() wait(100) ClearUOMsg() wait(100) UO.Msg('prstennausnicetchor') end,     -- truhlicka
[{'0'}]                 =    function() wait(100) ClearUOMsg() wait(100) UO.Msg('sramkrajinasklep') end,     -- schranka
---PARTY CONTROL
[{'b','space'}]         =    function() ClearUOMsg() wait (100) UO.Msg ('/add'..'\013\010') keys_enabled() end,
[{'n','space'}]         =    function() ClearUOMsg() wait (100) UO.Msg ('/accept'..'\013\010') keys_enabled() end,
---TARGETY
[{'1'}]                 =    function() UO.Macro(50,5) end,
[{'2'}]                 =    function() UO.Macro(51,5) end,
[{'3'}]                 =    function() if not UO.CharStatus:find('G') and UO.Hits>0 then UO.Macro(6,0) end UO.Macro(53,0) end,
[{'1','alt'}]           =    function() UO.Macro(52,5) UO.Macro(53,0) end,
---PET CONTROL
[{'d','shift'}]         =    function() UO.Macro(3,0,'all stay') end,
[{'e','shift'}]         =    function() UO.Macro(3,0,'all come') end,
[{'f','shift'}]         =    function() UO.Macro(3,0,'all stop') end,
[{'r','shift'}]         =    function() UO.Macro(3,0,'all go') end,
---MISCELLANEOUS

---DALSI
[{'enter'}]             =    function() ClearUOMsg() keys_enabled() end,
[{'esc'}]               =    function() ClearUOMsg() keys_enabled() end,
[{'g'}]                 =    function() UO.Macro(3,0,'guards') end,
[{'h','space'}]         =    function() local x=UO.CharPosX local y=UO.CharPosY ClearUOMsg() wait(200)
                                        UO.Msg('|AFK MODE') for i=1,140 do UO.Key('space') end
                                        j = journal:new()
                                        while not getkey('esc') and x==UO.CharPosX and y==UO.CharPosY do
                                          if j:next() ~= nil then
                                            local msg=j:last()
                                            if msg:find('|64|[DRU�INA]: ') then UO.Msg ('/accept'..'\013\010') end
                                            for i=1,#ignore do if msg:find(ignore[i]) then msg=nil break end end
                                            if msg~=nil then
                                              local nHour, nMinute, nSecond, nMillisec = gettime() if nMinute<10 then nMinute = (0)..nMinute end
                                              local journal_msg = (nHour..(':')..nMinute..(' || ')..msg)
                                              cm_antiafk(journal_msg) print(journal_msg) afk_msg()
                                            end
                                            j:clear() 
                                          end
                                          wait(50) if getkey('enter') then ClearUOMsg() wait(200) UO.Msg('|AFK MODE') for i=1,140 do UO.Key('space') end end
                                        end ClearUOMsg() wait(100) UO.Msg('|HOTKEYS ZAPNUTY') for i=1,140 do UO.Key('space') end end,
}

---------------
---- FUNCS ----
---------------
function ClearUOMsg() for i=1,140 do UO.Key('back') end end

function keys_enabled()
  wait(200) UO.Msg('|HOTKEYS ZAPNUTY') for i=1,140 do UO.Key('space') end
end

function keys_disabled(var)
  ClearUOMsg() wait(100)
  if     var=='message' then UO.Msg (''  )
  elseif var=='whisper' then UO.Msg ('; ')
  elseif var=='yell'    then UO.Msg ('! ')
  elseif var=='emote'   then UO.Msg (': ')
  elseif var=='party'   then UO.Msg ('/' ) end
  while true do wait (1)
        if getkey('enter') then while getkey('enter') do wait(1) end keys_enabled() return end
        if getkey('esc') then while getkey('esc') do wait(1) end ClearUOMsg() keys_enabled() return end
  end
end

function modifier_key(mod_key1,mod_key2)
 local notkeys = {}
 local cntr = 1
 for i=1,#modkeys do if mod_key1~=modkeys[i] and mod_key2~=modkeys[i] then notkeys[cntr]=modkeys[i] cntr=cntr+1 end end
 for i=1,#notkeys do if getkey(notkeys[i]) then return false end end
 if (getkey(mod_key1) or mod_key1==nil) and (getkey(mod_key2) or mod_key2==nil) then return true
 else return false end
end

---queue
queue = {}
queue.mt = {
     __index = queue,
}
 
function queue.New()
   local self = {}
   setmetatable(self,queue.mt)
   return self
end
 
function queue:push(s)
   if #self>0 then for i=1,#self do if self[i]:find(s) then return end end end -- brani pridani akce, pokud je uz ve fronte
   table.insert(self, 1, s)
end

local queue_wait=getticks() 
function queue:dequeue()
   local s
   if #self > 0 and queue_wait<getticks()-minimal_pause then
        s = self[#self]
        self[#self] = nil
        queue_wait=getticks()
   end
   return s
end

---------------
---- INIT -----
---------------
if UO.CliLogged==false then
  local pauzicka = 1000
  UO.Msg(password..'\013\010') wait (pauzicka)
--  UO.Click(260,110,false,true,true,false) wait (pauzicka)
--  UO.Msg('\013\010') wait (pauzicka)
end
while UO.CliLogged==false do wait(1000) end
local Commands = queue.New()
ClearUOMsg()
wait (100)
keys_enabled() 
---------------
-- MAIN LOOP --
---------------
while UO.CliLogged==true do wait(loop_wait)
  for key,command in pairs(no_action_hotkeys) do
    if getkey(key[1]) and modifier_key(key[2],key[3]) then command() while getkey(key[1]) do wait(1) end end
  end
  for key,command in pairs(hotkeys) do
    if getkey(key[1]) and modifier_key(key[2],key[3]) then --[[ UO.SysMessage('PUSH: '..command,5555) ]] Commands:push(command) while getkey(key[1]) do wait(1) end end
  end
  if getatom('Request') == 'true' then wait(10) -- divna pauza... resi problem se zasekavanim skriptu
    command = Commands:dequeue()
    if command then setatom('Request',command) --[[ UO.SysMessage('DEQUEUE: '..command,5555) ]] else setatom('Request','nil') end   
  end
end