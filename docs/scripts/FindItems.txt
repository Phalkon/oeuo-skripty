-- Useage : findItem( [searchTable] [searchIndex] [searchCriteria] )
-- All arguments are optional.
Where;
searchTable is {} or {id xor type} or nil -- {} is default if not passed or not verified
searchIndex is number of index to be returned -- 1 is default if not passed or not verified
searchCriteria is G or G_dist or C or C_containerId -- nil is default if not passed or not verified
-- All arguments dont needs to be in [searchTable],[ searchIndex ],[searchCriteria] order.
You may reorder or not use any of arguments;
findItem( [searchTable],[index],[searchCriteria] )
findItem( [searchCriteria],[searchTable],[index] )
findItem( [searchCriteria],[searchTable] )
findItem( )
-- Returns item table {findID,findType,findX,findY,findZ,findDist,findKind,findStack,findBagID,findRep,findCol,findIndex,findCnt}
if not found any then returns nil for all find's except findCnt(returns 0) and findIndex(returns searchIndex if u search by searchIndex or default 1 for searchIndex)
-- Examples:
fItem = findItem() --> fItem is first found item of all
fItem = findItem({401}) --> fItem is first found {401} type. You may use searchTable {Type,ID,ID,Type} as u want
fItem = findItem(2) --> fItem is indexed with (2) item if there is
fItem = findItem("g") --> fItem is first found item on ground
fItem = findItem("g_2") --> fItem is first found item on ground in 2 square range
fItem = findItem("c") --> fItem is first found item in a container
fItem = findItem("c_"..UO.BackpackID) --> fItem is first found item in Chars Backpack

-- findIndex(index) function get the values of all other findItem() results without restarting the time-consuming findItem() function.
-- Its range vary from 1 to findCnt.
-- As it does in EasyUO, example below is taken from EasyUO(wiki) and adapted

-- Use a single finditem to display the properties of each item on my paperdoll.
-- Output is set to display via print() command

findCnt = findItem("C_"..UO.CharID).findCnt
for i=1,findCnt do
print(UO.Property(findIndex(i).findID))
end

-- ignoreItem( "reset" | {id xor type} [numeric list])
-- ignoreItem will hide items from the findItem() function.
-- If an item ID or type is ignored then any further findItem commands will not find the item as long as the findItem command is used with a matching parameter.
-- Differs from EasyUO's ingoreItem;
if an item type is ignored and findItem() is called using an ID then the item will not be found
if an item ID or type is already ignored and ignoreItem() is called to ignore it again the item ID or type will not be removed from the ignore list
ignoreItem() supports ignoring IDs and TYPEs in the same command.
-- The reset option will flush either all ignore item lists or only a single list if the list option is passed
