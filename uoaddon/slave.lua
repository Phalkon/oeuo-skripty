----- planovani fci
-- nacist default vars, kdyz uoname vars neexistujou + vytvorit novou slozku uoname a nakopirovat default vars

------------------------------------------------------------
-------------------- B E F O R E   L O G I N ---------------
------------------------------------------------------------

local files = {
['file_exist']		= function (file) local f=openfile(file) if f then f:close() return true else return false end end,
['FindItem']		= dofile(getinstalldir()..'/functions/FindItems.lua'),
--['FluentUO']		= dofile(getinstalldir()..'/functions/FluentUO.lua'),
['journal']		= dofile(getinstalldir()..'/functions/journal.lua'),
['communicator']	= dofile(getinstalldir()..'/functions/communicator.lua'),
['base64']		= dofile(getinstalldir()..'/functions/base64.lua'),
['journal_strings']	= dofile(getinstalldir()..'/data/journal_strings.lua'),
}
if files['file_exist'] (getinstalldir()..'/data/pswd.lua') then
files['pswd'] 		= dofile(getinstalldir()..'/data/pswd.lua') end

---------------
---- VARS -----
---------------
local minimal_pause = 500
local loop_wait=1

local stamrezerva=2

local zbrane_poison = {kopi['type'][1],kopi['type'][2],kratke_kopi['type'][1],kratke_kopi['type'][2],kris['type'][1],kris['type'][2],dyka['type'][1],dyka['type'][2]}
local zbrane_kuchani = {kopi['type'][1],kopi['type'][2],kratke_kopi['type'][1],kratke_kopi['type'][2],kris['type'][1],kris['type'][2],dyka['type'][1],dyka['type'][2]}
local equip_types = {5060,5062,5067,5070,5074,5077,5076,5082,5085,5397,5399,5443,5897,5898,5905,5907,7174,7947,15018,15193}
-- looter ----------------------------------------------------------------------------------------------
ltr_lag_pause = 0
--------------------------------------------------------------------------------------------------------
local lastequiped={['ID']=0,['type']=0,['col']=0}
local mounti={16031,16032,16033,16034,16061}
local autorun='deactivated'
local autopilot=0
local autopilot_ID=0
local autopilot_overhead=0
-- timery --
local potka_timer=getticks()
local krvko_timer=getticks()-30000
local srazeni_timer=getticks()-3000
local port_timer=getticks()-3000
local volani_kone=getticks()-3000
local wine_timer=getticks()-3000
------------
local warmode_alert=getticks()-10000
local status_prodleva=getticks()-1000 -- zabranuje nekolikanasobnemu vytazeni statusu
local tarcurs_remover=0
local gamma_prepinac={}
local uoam_prepinac={}

local potion_timer = {
['GH']       =  8000,
['obnova']   = 11000,
['protijed'] =  6500, -- 6600??
['ocelka']   =  8500,
['sila']     =  4000,
['hbitost']  =  4000, -- 4100??
['osvezko']  =  7000,
['dispell']  = 10000,
['invisko']  = 10000,
['nezranko'] = 30000,
['inko']     = 15000,
}
--------------------------------------------------------------------------------------------------------
local poloha_gumpu = {
['status']     = {['x']=1270,['y']=423},
['backpack']   = {['x']=1239,['y']=505},
['batoh2']     = {['x']=1274,['y']=723},
}
local string = {
['potka1']=jString['Pokladas prazdne lahve do batohu.'],
['potka2']=jString['Lahvicka se pri manipulaci poskodila, musis ji vyhodit...'],
['lenost']=jString['Nemas ani trosku chut delat neco takoveho'],
['cannot']={jString['Neni mozne, abys to dokazal prave ted.'],jString['Neni mozne, abys to dokazala prave ted.']},
['krvko']=jString['Tve utoky jsou ted mnohem bolestivejsi'],
['srazeni']=jString['Pripravujes se na srazeni z kone'],
['mount too far']=jString['Nedosahnes na tu bytost.'],
['mount timer']=jString['Takhle rychle po sesednuti nasednout nemuzes'],
}
---------------
---- FUNCS ----
---------------
-- tables and strings --
local Table ={
['Concat'] = function(t1,t2) for i=1,#t2 do t1[#t1+1] = t2[i] end return t1 end,
['Sum'] = function (t) local sum = 0 for k,v in pairs(t) do sum = sum + v end return sum end,
['HasValue'] = function (tab, val) for index, value in ipairs(tab) do if value == val then return true end end return false end,
}
local String={
['Trim'] = function(s) return s:match"^%s*(.*)":match"(.-)%s*$" end,
['ToNumber'] = function(string) local number_str = string.match(string,'%d+') local number = tonumber(number_str) return number end,
}
-- action queue --
local action={
['queue']={},
['wait']=getticks()-500}
action['do']=function(dothis) table.insert(action['queue'],dothis) end
-- sysmsg queue --
local sysmsg={
['queue']={},
['wait']=getticks()-100}
local ClientMessage = function (typ,zprava,barva,kde)
			if typ=='ExMsg' then
			 if kde and barva and zprava then table.insert(sysmsg['queue'],function()UO.ExMsg(kde,3,barva,zprava)end)
			 else table.insert(sysmsg['queue'],function()UO.SysMessage('DEBUG - ClientMessage Error!',5555)end) end
			end
			if typ=='SysMsg' then
			 if zprava and barva then table.insert(sysmsg['queue'],function()UO.SysMessage(zprava,barva)end)
			 else table.insert(sysmsg['queue'],function()UO.SysMessage('DEBUG - ClientMessage Error!',5555)end) end
			end end
----------------
local use_object	= function(UObjectID) UO.LObjectID = UObjectID UO.Macro(17,0) end
local target_object	= function(TObjectID,TObjectKind) UO.LTargetID = TObjectID UO.LTargetKind = TObjectKind UO.Macro(22,0) end
local dragndrop	= function(id,stack,drop) UO.Drag(id,stack) time_to_run=getticks()+200 repeat wait(1) wait(minimal_pause-1) until UO.LLiftedID==id or time_to_run<getticks() UO.DropC(drop) end
local warmode_cancel	= function() if UO.CharStatus:find('G') and UO.Hits>0 then UO.Macro(6,0) end end
local mounted		= function() local IsMounted=ScanItems(true,{Type=mounti,ContID=UO.CharID}) if IsMounted[1] then return true else return false end end
----
local wEquip		= function(zbran) if zbran then action['do'](function() lastequiped={['ID']=zbran.ID,['type']=zbran.Type,['col']=zbran.Col,['name']=zbran.Name} use_object(zbran.ID) end) end end

local CheckForSysMsg	= function(sysmsg,lag,timeout)
				local time_to_run=getticks()+timeout
				repeat wait(lag) for i=1,#sysmsg do if sysmsg[i]==UO.SysMsg then return true end end until time_to_run<getticks()
				return false end
local WaitForTarCurs	= function(timeout)
				time_to_run = getticks() + timeout
				repeat wait (1) until UO.TargCurs == true or time_to_run < getticks()
				if UO.TargCurs == true then return true else return false end end


-- old
function BelongsTo(x,y) for i,v in ipairs(y) do if v == x then return true end end end
function writetofile(msg,path,mode) local file=openfile(path,mode) if file then file:write(msg) file:close() end end
function hid_cancel() if hid_pocitani==true then hid_pocitani=false ClientMessage('ExMsg','hid: canceled',0,UO.CharID) end end

------------------------------------------------------------
-------------------- L O G I N   P R O C E S S -------------
------------------------------------------------------------

local loginpasskey='F1'
while UO.CliLogged==false do wait (100)
 if getkey(loginpasskey) and acc then UO.Msg(dec(acc)) while getkey(loginpasskey) do wait(100) end end
end

------------------------------------------------------------
---------------------- A F T E R   L O G I N ---------------
------------------------------------------------------------
setatom('uo_name',nil)
while UO.CharName=='' do wait(1000)
 if UO.CliLogged==true and (UO.MaxHits==0 or UO.MaxStam==0) and status_prodleva<getticks()-1000 then UO.Macro(8,2) UO.Macro(10,2) UO.ContPosX=poloha_gumpu['status']['x'] UO.ContPosY=poloha_gumpu['status']['y'] status_prodleva=getticks() end
end
local undead_morfs = {'gh�l','kostlivec �arod�j','kostlivec ryt��'}
if BelongsTo(UO.CharName,undead_morfs)==true then setatom('uo_name',dec('QXdh')) else setatom('uo_name',UO.CharName) end


---------------
---- VARS -----
---------------

local varfiles={
['mount']=getinstalldir()..'/data/'..getatom('uo_name')..'/vars/mount.lua',
['mount_mini']=getinstalldir()..'/data/'..getatom('uo_name')..'/vars/mount_mini.lua',
['pet']=getinstalldir()..'/data/'..getatom('uo_name')..'/vars/pet.lua',
['pet_mini']=getinstalldir()..'/data/'..getatom('uo_name')..'/vars/pet_mini.lua',
['kul_id']=getinstalldir()..'/data/'..getatom('uo_name')..'/vars/kul_id.lua',
['looter']=getinstalldir()..'/data/'..getatom('uo_name')..'/looter.lua',
['vars']=getinstalldir()..'/data/'..getatom('uo_name')..'/vars.lua',
['hotkeys']=getinstalldir()..'/data/'..getatom('uo_name')..'/hotkeys.lua',
}

if files['file_exist'] (varfiles['mount']) then
files['mount'] 	= dofile(varfiles['mount']) end
if files['file_exist'] (varfiles['mount_mini']) then
files['mount_mini'] 	= dofile(varfiles['mount_mini']) end
if files['file_exist'] (varfiles['kul_id']) then
files['kul_id'] 	= dofile(varfiles['kul_id']) end
if files['file_exist'] (varfiles['looter']) then
files['looter'] 	= dofile(varfiles['looter']) end
if files['file_exist'] (varfiles['vars']) then
files['vars'] 		= dofile(varfiles['vars']) end

local hotkeys_file = openfile(varfiles['hotkeys'],'r')

local status = {['Hits']=UO.Hits,['Mana']=UO.Mana,['Stamina']=UO.Stamina,['EnemyHits']=UO.EnemyHits,['EnemyID']=UO.EnemyID,
                ['MinDmg']=UO.MinDmg,['MaxDmg']=UO.MaxDmg,['Weight']=UO.Weight,['MaxWeight']=UO.MaxWeight,
                ['AR']=UO.AR,['FR']=UO.FR,['CR']=UO.CR,['PR']=UO.PR,['ER']=UO.ER}
-- looter ----------------------------------------------------------------------------------------------
--local loot_pause=getticks()-500
local dontloot = {} for i=1,#vlasy do table.insert(dontloot,vlasy[i]) end for i=1,#vousy do table.insert(dontloot,vousy[i]) end
local vylooceno={}
local loot_table={}
ltr_global = {} -- ltr_global['loot_pause']=getticks()-500 ltr_global['lootovani']=0 ltr_global['typlootu']=0
--drop all values from nested tables in 'lootit' in one table 'lootit2'
local lootit2={}
for i1=1,#lootit do for i2=1,#lootit[i1] do
 if type(lootit[i1][i2]['type']) == 'table' then
  for i3=1,#lootit[i1][i2]['type'] do table.insert(lootit2,lootit[i1][i2]['type'][i3]) end
 else table.insert(lootit2,lootit[i1][i2]['type']) end
end end

--remove duplicate values from 'lootit2' and create table 'loot'
local hash = {}
local loot = {}
for _,v in ipairs(lootit2) do
   if (not hash[v]) then
       loot[#loot+1] = v
       hash[v] = true
   end
end
--------------------------------------------------------------------------------------------------------
local skilly={
  ['Bojov�']={
    {['name']='Boj beze zbran�',        ['value']={UO.GetSkill('Wres')}}, -- Wrestling
    {['name']='Boj bodn�mi zbran�mi',   ['value']={UO.GetSkill('Fenc')}}, -- Fencing
    {['name']='Boj drtiv�mi zbran�mi',  ['value']={UO.GetSkill('Mace')}}, -- Mace Fighting
    {['name']='Boj se�n�mi zbran�mi',   ['value']={UO.GetSkill('Swor')}}, -- Swordsmanship
    {['name']='Kryt �t�tem',           ['value']={UO.GetSkill('Parr')}}, -- Parrying
    {['name']='Lukost�elba',            ['value']={UO.GetSkill('Arch')}}, -- Archery
    {['name']='Taktika',                ['value']={UO.GetSkill('Tact')}}, -- Tactics
    {['name']='Znalost zbran�',         ['value']={UO.GetSkill('Arms')}}, -- Arms Lore
  },
  ['V�robn�']={
    {['name']='D�evorubectv�',          ['value']={UO.GetSkill('Lumb')}}, -- Lumberjacking
    {['name']='Hornictv�',              ['value']={UO.GetSkill('Mini')}}, -- Mining
    {['name']='Jemn� mechanika',        ['value']={UO.GetSkill('Tink')}}, -- Tinkering
    {['name']='Kov��stv�',              ['value']={UO.GetSkill('Blac')}}, -- Blacksmithy
    {['name']='K�ej�ovstv�',            ['value']={UO.GetSkill('Tail')}}, -- Tailoring
    {['name']='�ezb��stv�',             ['value']={UO.GetSkill('Bowc')}}, -- Bowcraft Fletching
    {['name']='Tesa�stv�',              ['value']={UO.GetSkill('Carp')}}, -- Carpentry
  },
  ['Magick�']={
    {['name']='Alchymie',               ['value']={UO.GetSkill('Alch')}}, -- Alchemy
    {['name']='Antimagie',              ['value']={UO.GetSkill('Resi')}}, -- Resisting Spells
    {['name']='Kartografie',            ['value']={UO.GetSkill('Cart')}}, -- Cartography
    {['name']='Magie',                  ['value']={UO.GetSkill('Mage')}}, -- Magery
    {['name']='Meditace',               ['value']={UO.GetSkill('Medi')}}, -- Meditation
    {['name']='Nekromancie',            ['value']={UO.GetSkill('Necr')}}, -- Necromancy
    {['name']='Odhad inteligence',      ['value']={UO.GetSkill('Eval')}}, -- Evaluating Intelligence
    {['name']='Opisov�n�',              ['value']={UO.GetSkill('Insc')}}, -- Inscription
    {['name']='�e� mrtv�ch',            ['value']={UO.GetSkill('Spir')}}, -- Spirit Speak
  },
  ['Hrani���sk�']={
    {['name']='Anatomie',               ['value']={UO.GetSkill('Anat')}}, -- Anatomy
    {['name']='Krocen� zv��at',         ['value']={UO.GetSkill('Anim')}}, -- Animal Taming
    {['name']='Kuch�n�',                ['value']={UO.GetSkill('Bush')}}, -- Bushido
    {['name']='L��en�',                 ['value']={UO.GetSkill('Heal')}}, -- Healing
    {['name']='L��en� zv��at',          ['value']={UO.GetSkill('Vete')}}, -- Veterinary
    {['name']='Ochutn�v�n�',            ['value']={UO.GetSkill('Tast')}}, -- Taste Identification
    {['name']='Pastevectv�',            ['value']={UO.GetSkill('Herd')}}, -- Herding
    {['name']='Pitv�n�',                ['value']={UO.GetSkill('Fore')}}, -- Forensic Evaluation
    {['name']='Ryba�en�',               ['value']={UO.GetSkill('Fish')}}, -- Fishing
    {['name']='Stopov�n�',              ['value']={UO.GetSkill('Trac')}}, -- Tracking
    {['name']='T�bo�en�',               ['value']={UO.GetSkill('Camp')}}, -- Camping
    {['name']='Va�en�',                 ['value']={UO.GetSkill('Cook')}}, -- Cooking
    {['name']='Znalost zv��e',          ['value']={UO.GetSkill('Anil')}}, -- Animal Lore
  },
  ['Zlod�jsk�']={
    {['name']='�much�n�',               ['value']={UO.GetSkill('Snoo')}}, -- Snooping
    {['name']='Kraden�',                ['value']={UO.GetSkill('Stea')}}, -- Stealing
    {['name']='Odhad ceny',             ['value']={UO.GetSkill('Item')}}, -- Item Identification
    {['name']='Odhalen� skryt�ho',      ['value']={UO.GetSkill('Dete')}}, -- Detecting Hidden
    {['name']='Odstran�n� pasti',       ['value']={UO.GetSkill('Remo')}}, -- Remove Trap
    {['name']='Otev�r�n� z�mku',        ['value']={UO.GetSkill('Lock')}}, -- Lockpicking
    {['name']='Pohyb ve st�nu',         ['value']={UO.GetSkill('Stlt')}}, -- Stealth
    {['name']='Skryt� se',              ['value']={UO.GetSkill('Hidi')}}, -- Hiding
    {['name']='Travi�stv�',             ['value']={UO.GetSkill('Pois')}}, -- Poisoning
  },
  ['Bardsk�']={
    {['name']='Hudba',                  ['value']={UO.GetSkill('Musi')}}, -- Musicianship
    {['name']='L�k�n�',                 ['value']={UO.GetSkill('Disc')}}, -- Discordance
    {['name']='Provokace',              ['value']={UO.GetSkill('Prov')}}, -- Provocation
    {['name']='Usmi�ov�n�',             ['value']={UO.GetSkill('Peac')}}, -- Peacemaking
  },
}
---------------
---- FUNCS ----
---------------
function wait_for_gump(lag,timeout)
 time_to_run = getticks() + timeout
 local vstupX=UO.ContPosX local vstupY=UO.ContPosY
 repeat wait (lag) until UO.ContPosX~=vstupX or UO.ContPosY~=vstupY or time_to_run < getticks()
 if UO.ContPosX~=vstupX or UO.ContPosY~=vstupY then return true else return false end
end

function wait_for_cont(cont,lag,timeout)
 time_to_run = getticks() + timeout
 repeat wait (lag) until UO.ContID==cont or time_to_run < getticks()
 if UO.ContID==cont then return true else return false end
end

-- function FindNUse(type,col)
-- local item = ScanItems(true,{Type=type,Col=col,Kind=0},{ContID=UO.CharID})
-- if item[1] ~= nil then use_object(item[1].ID) end
-- end

function always_run(prepinac)
  if prepinac=='on'  and autorun=='activated' then return end
  if prepinac=='off' and autorun=='deactivated' then return end
  UO.Macro(32,0) if autorun=='activated' then autorun='deactivated' elseif autorun=='deactivated' then autorun='activated' end
end

local hid_cntr = {}
function hid_pocitadlo()
--if UO.CharStatus:find('G') and hidtimer+200 < getticks() then hid_cancel() --[[ always_run('off') ]] return end
  if UO.CharStatus:find('G') and hidtimer+300 < getticks() then hid_cancel() --[[ always_run('off') ]] return end
    if hid_cntr[1]~=1 and (UO.SysMsg==string['lenost'] or BelongsTo(UO.SysMsg,string['cannot'])) then
--    if hid_cntr[1]~=1 and (UO.SysMsg==string['lenost'] or UO.SysMsg==string['cannot']) then
    hid_pocitani=false always_run('off') return end
  for f=1,4 do
    if hid_cntr[f]~=1 and hidtimer+500*f < getticks() then
      if f==4 then always_run('off') hid_pocitani=false break end
      hid_cntr[f]=1 ClientMessage('ExMsg','hid: '..4-f,946,UO.CharID) break  
    end
  end 
end

local potka_timer_odeslano={}
function potka_pocitadlo()
    local time=(potka_timer-getticks())/1000
    if (math.ceil(time)-time)<0.3 then time=math.ceil(time) elseif (time-math.floor(time))<0.3 then time=math.floor(time) else time=nil end
--  if time~=nil and potka_timer_odeslano[time]==nil then ClientMessage('ExMsg','potka: '..time,0,UO.CharID) potka_timer_odeslano[time]=1 end
    if time~=nil and potka_timer_odeslano[time]==nil then ClientMessage('SysMsg','potka: '..time,0) potka_timer_odeslano[time]=1 end
    if time==1 then potka_timer=getticks() potka_timer_odeslano={} end
end


function potka_pocitadlo2()
  if potka_timer<=getticks() then return false
  else local time=(potka_timer-getticks())/1000
       if (math.ceil(time)-time)<=0.5 then time=math.ceil(time) elseif (time-math.floor(time))<0.5 then time=math.floor(time) else time=nil end
       return time end
end

local dyka_cntr = {}
function dyka_pocitadlo()
  for f=1,4 do
    if dyka_cntr[f]~=1 and dyka_timer+1000*f < getticks() then
      if f==4 then dyka_pocitani=false break end
      dyka_cntr[f]=1 ClientMessage('ExMsg','dyka: '..4-f,0,UO.CharID) break
    end
  end 
end

local skok_cntr = {}
function skok_pocitadlo()
  for f=1,5 do
    if skok_cntr[f]~=1 and skok_timer+1000*f < getticks() then
      if f==5 then skok_pocitani=false break end
      skok_cntr[f]=1 ClientMessage('ExMsg','skok: '..4-f,0,UO.CharID) break
    end
  end 
end

local stats_func = {
['HP']       = function() if UO.Hits~=0 then if potka_pocitadlo2()~=false then ClientMessage('ExMsg',tostring(UO.Hits)..(' / ')..(UO.Mana)..(' / ')..(UO.Stamina)..('   (')..potka_pocitadlo2()..(')'),35,UO.CharID)
                                             else ClientMessage('ExMsg',tostring(UO.Hits)..(' / ')..(UO.Mana)..(' / ')..(UO.Stamina),35,UO.CharID) end
                          else cm_screen() end end,
--['HP']     = function() if UO.Hits~=0 then ClientMessage('ExMsg',tostring(UO.Hits)..(' / ')..(UO.Stamina),35,UO.CharID)
--                        else cm_screen() end end,
['enemy HP'] = function() local variable=1
                          if UO.EnemyHits==0 then if vypocet~=100 then ClientMessage('SysMsg','VRAZDAAAAA!',38) cm_screen() end end
                          if UO.EnemyHits==4 and UO.EnemyID~=enemy then variable=50 end   
                          vypocet=(UO.EnemyHits/2)*variable if vypocet > 100 then vypocet = 100 end
                          if UO.EnemyHits~=0 then ClientMessage('ExMsg',tostring((vypocet)..('%')),57,UO.EnemyID) end
                          local enemy=UO.EnemyID end,
['DMG']      = function() if lastequiped['name'] then ClientMessage('ExMsg',String['Trim'](lastequiped['name'])..(': ')..(UO.MinDmg)..(' - ')..(UO.MaxDmg),1259,UO.CharID)
			   else ClientMessage('ExMsg',('dmg: ')..(UO.MinDmg)..(' - ')..(UO.MaxDmg),1259,UO.CharID) end end,
['weight']   = function() ClientMessage('ExMsg',tostring("nalozeni: ")..(UO.Weight)..(' / ')..(UO.MaxWeight),1378,UO.CharID) end,
['AR']       = function() ClientMessage('ExMsg',tostring('armor: ')..(UO.AR),1673,UO.CharID) end,
['FR']       = function() ClientMessage('ExMsg',tostring('ohen: ')..(UO.FR),40,UO.CharID) end,
['CR']       = function() ClientMessage('ExMsg',tostring('mraz: ')..(UO.CR),1266,UO.CharID) end,
['PR']       = function() ClientMessage('ExMsg',tostring('kyselina: ')..(UO.PR),1269,UO.CharID) end,
['ER']       = function() ClientMessage('ExMsg',tostring('elektrina: ')..(UO.ER),54,UO.CharID) end,
}
-- looter ----------------------------------------------------------------------------------------------
local drag=0
local storcont=0
local looter_func = {
['open_container']  = function(container)  if UO.ContID~=container.ID then if UO.CharStatus:find('H') and container.Type==8198 and container.Kind==1 then ClientMessage('SysMsg','Looter: otevreni tela by te odhidlo',60) return false else UO.NextCPosX=0 UO.NextCPosY=0 use_object(container.ID) return true end else return true end end,
['start_loot']      = function(kde,co)     ltr_global['lootovani']=kde.ID ltr_global['contX']=kde.X ltr_global['contY']=kde.Y ltr_global['contZ']=kde.Z ltr_global['typlootu']=co ltr_global['loot_pause']=getticks() end,
['main_fce']        = function(contas,typ) local distance=math.max(math.abs(ltr_global['contX']-UO.CharPosX),math.abs(ltr_global['contY']-UO.CharPosY))
                                           if loot_table[contas]==nil then -- if UO.ContID~=contas then return end
                                            if typ=='loot' then loot_table[contas] = ScanItems(true,{Type=loot,ContID=contas})
                                            elseif typ=='vse' then loot_table[contas] = ScanItems(true,{ContID=contas},{Type=dontloot}) end end
                                           if loot_table[contas][1]~=nil then
                                            if drag==1 then if typ=='loot' then UO.DropC(lootBagID) elseif typ=='vse' then UO.DropC(UO.CharID) end drag=0
                                            elseif distance<3 then UO.Drag(loot_table[contas][1].ID,loot_table[contas][1].Stack)
                                             if loot_table[contas][1].Stack and loot_table[contas][1].Name then
                                              ClientMessage('SysMsg','Looter: '..loot_table[contas][1].Stack..'x '..loot_table[contas][1].Name,60) end
                                             table.remove(loot_table[contas],1) drag=1
                                            else ClientMessage('SysMsg','Looter: Stojis moc daleko, nelze pickovat',60) ltr_global={} return end           
                                           else ltr_global={} loot_table[contas]=nil table.insert(vylooceno,1,contas) if drag==1 then if typ=='loot' then UO.DropC(lootBagID) elseif typ=='vse' then UO.DropC(UO.CharID) end drag=0 end ClientMessage('SysMsg','Looter: vylooceno',60) end end,
['roztrid']         = function(contas)     if loot_table[contas]==nil then loot_table[contas] = ScanItems(true,{Type=loot,ContID=contas}) end
                                           if loot_table[contas]~=nil then
                                            if drag==1 then UO.DropC(storcont) drag=0 storcont=0
                                            else
                                             for i1=1,#loot_table[contas] do
                                              for i2=1,#lootit do for i3=1,#lootit[i2] do
                                               if loot_table[contas][i1].Type==lootit[i2][i3]["type"] and lootit[i2]["storeCont"]~=nil then
                                                UO.Drag(loot_table[contas][1].ID,loot_table[contas][1].Stack) drag=1 storcont=lootit[i2]["storeCont"] break end
                                               end if drag==1 then break end end
                                              table.remove(loot_table[contas],i1) if drag==1 then break end
                                             end
                                            end
                                           end 
                                           if loot_table[contas][1]~=nil then ltr_global={} loot_table[contas]=nil if drag==1 and storcont~=0 then UO.DropC(storcont) drag=0 storcont=0 end ClientMessage('SysMsg','Looter: roztrizeno',60) end end,
['strihani_hadriku'] = function(nuzticky,umyvadylko,batuzek)
					use_object(batuzek) wait_for_cont(batuzek,10,5000) local hadriky_types={} for i=1,#hadry do table.insert(hadriky_types,hadry[i]['type']) end
                                	local hadriky = ScanItems(true,{Type=hadriky_types,ContID=batuzek,Kind=0})
                                	if hadriky then for i=1,#hadriky do use_object(nuzticky) WaitForTarCurs(3000) target_object(hadriky[i].ID,1) wait(minimal_pause) end end
                                	local obvazicky = ScanItems(true,{Type=obvaz['type'],ContID=UO.BackpackID,Kind=0},{Col=0})
                                 	if obvazicky then
                                 	 for i=1,#obvazicky do use_object(obvazicky[i].ID) WaitForTarCurs(3000) target_object(umyvadylko,1) wait(minimal_pause) dragndrop(obvazicky[i].ID,obvazicky[i].Stack,UO.BackpackID) wait(minimal_pause) end
                                	 ClientMessage('SysMsg','Looter: Hotovo',60)
                                	else ClientMessage('SysMsg','Looter: Neni prace',60) end
                                	end,
}
--- O L D   L O O T E R---------------------------------------------------------------------------------
function wait_until(podminka,lag,timeout)
 if timeout == nil then repeat wait (lag) until podminka == true 
 else time_to_run = getticks() + timeout
 repeat wait (lag) until podminka == true or time_to_run < getticks() end
end

function roztrid(co,odkud)
  use_object(odkud)
  wait_until(UO.ContID==odkud,200,1000)
  wait (200)
  local ncnt = UO.ScanItems(true)
  for nindex = 0,(ncnt-1) do
    local nid,ntype,nkind,ncontid,nx,ny,nz,nstack,nrep,ncol = UO.GetItem(nindex)
    for i1 = 1,#co do for i2 = 1,#co[i1] do
      if ntype == co[i1][i2]["type"] and ncontid == odkud and (ncol == co[i1][i2]["col"] or co[i1][i2]["col"] == "col") and co[i1]["storeCont"] ~= nil then
        dragndrop(nid,nstack,co[i1]["storeCont"]) wait(500) wait(ltr_lag_pause)
      end
    end end
  end
ClientMessage('SysMsg','Looter: roztrizeno',60)
end
--------------------------------------------------------------------------------------------------------
-- hotkeys
local command = 'wait'
local Funcs = {
---TARGETY
['target next']	=	function() UO.Macro(50,5) end,
['target previous']	=	function() UO.Macro(51,5) end,
['attack target']	=	function() if not UO.CharStatus:find('G') and UO.Hits>0 then UO.Macro(6,0) end UO.Macro(53,0) end,
['attack closest']	=	function() UO.Macro(52,5) UO.Macro(53,0) end,
---SPELLY
['selfheal']		=	function() UO.Macro(1,0,'.sesli 903') WaitForTarCurs(500) target_object(UO.CharID,1) end,
['summon']		=	function() UO.Macro(1,0,'.startability 10') local pack=wait_for_gump(1,500) if pack==true then UO.ContPosX=UO.CursorX-200 UO.ContPosY=UO.CursorY-100 end end,
---ZBRANE
['kopi']		=	function()
					local weapon = ScanItems(true,{Type=kopi['type'],Kind=0,ContID=UO.BackpackID},{Col=2414})
					if weapon[1] ~= nil then wEquip(weapon[1]) tarcurs_remover=tarcurs_remover+1
					elseif Table['HasValue'](kopi['type'],lastequiped['type']) and lastequiped['col']~=2414 then ClientMessage('SysMsg','Kopi mas v ruce a nemas dalsi',5555)
					else ClientMessage('SysMsg','Nemas zadne kopi',5555) end
				end,
['bic']		=	function()
					local weapon = ScanItems(true,{Type=bic['type'],Kind=0,ContID=UO.BackpackID})
					if weapon[1] ~= nil then
					 local shield = ScanItems(true,{Type=stit,Kind=0,ContID=UO.BackpackID})
					 wEquip(weapon[1]) tarcurs_remover=tarcurs_remover+1
					 if shield[1] ~= nil then wait(minimal_pause) use_object(shield[1].ID) end
					elseif lastequiped['type']==bic['type'] then ClientMessage('SysMsg','Bic mas v ruce a nemas dalsi',5555)
					else ClientMessage('SysMsg','Nemas bic',5555) end
				end,
['1hand']		=	function()
					local weapon = ScanItems(true,{Type=weapon_1H,Kind=0,ContID=UO.BackpackID})
					if weapon[1] ~= nil then
					 local shield = ScanItems(true,{Type=stit,Kind=0,ContID=UO.BackpackID})
					 wEquip(weapon[1]) tarcurs_remover=tarcurs_remover+1
					 if shield[1] ~= nil then wait(minimal_pause) use_object(shield[1].ID) end
					elseif Table['HasValue'](weapon_1H,lastequiped['type']) then ClientMessage('SysMsg','Jednorucku mas v ruce a nemas dalsi',5555)
					else ClientMessage('SysMsg','Nemas zadnou jednorucku',5555) end
				end,
['ostep']		=	function()
					local weapon = ScanItems(true,{Type=kopi['type'],Kind=0,Col=2414,ContID=UO.BackpackID})
					if weapon[1] ~= nil then wEquip(weapon[1]) tarcurs_remover=tarcurs_remover+1
--					elseif HasValue(kopi['type'],lastequiped['type']) and lastequiped['col']==2414 then ClientMessage('SysMsg','Ostep mas v ruce a nemas dalsi',5555)
					else ClientMessage('SysMsg','Nemas zadny ostep',5555) end
				end,
['kuse']		=	function()
					local weapon = ScanItems(true,{Type=kuse,Kind=0,ContID=UO.BackpackID})
					if weapon[1] ~= nil then wEquip(weapon[1])
					elseif Table['HasValue'](kuse,lastequiped['type']) then ClientMessage('SysMsg','Kusi mas v ruce a nemas dalsi',5555)
					else ClientMessage('SysMsg','Nemas kusi',5555) end
				end,
['luk']		=	function()
					local weapon = ScanItems(true,{Type=luk,Kind=0,ContID=UO.BackpackID})
					if weapon[1] ~= nil then wEquip(weapon[1])
					elseif Table['HasValue'](luk,lastequiped['type']) then ClientMessage('SysMsg','Luk mas v ruce a nemas dalsi',5555)
					else ClientMessage('SysMsg','Nemas luk',5555) end
				end,
['disarm']		=	function()
					if lastequiped['ID']==0 then ClientMessage('SysMsg','Nemas zbran k odzbrojeni',1926) return
					else dragndrop(lastequiped['ID'],1,UO.CharID) lastequiped={['ID']=0,['type']=0,['col']=0} return end
				end,
['re-equip']		=	function()
					local equip = ScanItems(true,{Type=equip_types,Kind=0,ContID=UO.BackpackID})
					if equip[1] ~= nil then for i=1,#equip do action['do'](function() use_object(equip[i].ID) end) end
					else ClientMessage('SysMsg','Nemas nic k equipnuti',1926) end
				end,
---ABILITKY
['krvko']		=	function()
					if UO.Stamina<35 then ClientMessage('SysMsg','Jsi prilis unavena! 35 + '..stamrezerva,1360) return
					elseif krvko_timer<getticks()-30000 then UO.Macro(1,0,'.startability 20')
					 if CheckForSysMsg({string['krvko']},10,200)==true then krvko_timer=getticks() end
					else ClientMessage('SysMsg','Krvko already activated',5555) end
				end,
['para']		=	function()
					local weapon = ScanItems(true,{Type=vrhaci_nuz['type'],Kind=0},{ID=paradyka_blessed})
					if weapon[1] ~= nil then
					 for i=1,#weapon do if weapon[i].ContID==UO.CharID then return end end
					 wEquip(weapon[1])
					else local last_chance = ScanItems(true,{ID=paradyka_blessed,Kind=0})
					 if last_chance[1] ~= nil then wEquip(last_chance[1])
					else ClientMessage('SysMsg','Nemas paradyku',5555) end end
				end,
['poison']		=	function()
					UO.Macro(1,0,'.lektvar jed')
					for i=1,#zbrane_poison do if lastequiped['type']==zbrane_poison[i] then WaitForTarCurs(500) target_object(lastequiped['ID'],1) end end
				end,
['srazeni']		=	function()
					if UO.Stamina<20 then ClientMessage('SysMsg','Jsi prilis unavena! 20 + '..stamrezerva,1360) return
					elseif srazeni_timer<getticks()-3000 then UO.Macro(1,0,'.startability 17') krvko_timer=getticks()-30000
					 if CheckForSysMsg({string['srazeni']},10,200)==true then srazeni_timer=getticks() end
					else ClientMessage('SysMsg','Srazeni already activated',5555) end
				end,
['port']		=	function()
					if port_timer<getticks()-3000 then UO.Macro(1,0,'.startability 25') wait(200)
					 if UO.SysMsg:find(' do teleportace...') then port_timer=getticks() end
					else ClientMessage('SysMsg','Port already activated',5555) end
				end,
['nabij kusi']		=	function() action['do'](function() UO.Macro(1,0,'.nabij') end) end,
['odpocinek']		=	function() action['do'](function() UO.Macro(1,0,'.startability 4') end) end,
---LEKTVARY
['GH']			=	function() UO.Macro(1,0,'.lektvar lecit')    if potka_timer<=getticks() and CheckForSysMsg({string['potka1'],string['potka2']},10,200)==true then potka_timer=getticks()+potion_timer['GH'] end end,
['obnova']		=	function() UO.Macro(1,0,'.obnova')           if potka_timer<=getticks() and CheckForSysMsg({string['potka1'],string['potka2']},10,200)==true then potka_timer=getticks()+potion_timer['obnova'] end end,
['protijed']		=	function() UO.Macro(1,0,'.lektvar protijed') if potka_timer<=getticks() and CheckForSysMsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['protijed'] end end,
['ocelka']		=	function() UO.Macro(1,0,'.lektvar ocel')     if potka_timer<=getticks() and CheckForSysMsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['ocelka'] end end,
['explosko - drop']	=	function() UO.Macro(1,0,'.lektvar vybuch')   WaitForTarCurs(200) target_object(UO.CharID,1) end,
['explosko - throw']	=	function() UO.Macro(1,0,'.lektvar vybuch')   WaitForTarCurs(200) target_object(UO.LTargetID,1) end,
['sila']		=	function() UO.Macro(1,0,'.lektvar sila')     if potka_timer<=getticks() and CheckForSysMsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['sila'] end end,
['hbitost']		=	function() UO.Macro(1,0,'.lektvar hbitost')  if potka_timer<=getticks() and CheckForSysMsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['hbitost'] end end,
['osvezko']		=	function() UO.Macro(1,0,'.lektvar osvez')    if potka_timer<=getticks() and CheckForSysMsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['osvezko'] end end,
['dispell']		=	function() UO.Macro(1,0,'.lektvar rozptyl')  if potka_timer<=getticks() and CheckForSysMsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['dispell'] end end,
['invisko']		=	function() UO.Macro(1,0,'.lektvar neviditelnost')  if potka_timer<=getticks() and CheckForSysMsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['invisko'] end end,
['nezranko']		=	function() UO.Macro(1,0,'.lektvar nezranitelnost') if potka_timer<=getticks() and CheckForSysMsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['nezranko'] end end,
['inko']		=	function() UO.Macro(1,0,'.lektvar povest')   if potka_timer<=getticks() and CheckForSysMsg({string['potka1'],string['potka2']},10,200)==true then  potka_timer=getticks()+potion_timer['inko'] end end,
['wine']		=	function()
					if wine_timer<getticks()-3000 then
					 local piticko = ScanItems(true,{Name='Lahev Vina',Type=lahev_vina['type'],Kind=0})
					 if piticko[1] ~= nil then use_object(piticko[1].ID) wine_timer=getticks()
					 else ClientMessage('SysMsg','Nem� u sebe v�no',5555) end
					else ClientMessage('SysMsg','Je�t� nem��e� p�t',5555) end
				end,
---SKILLY
['HID']		=	function()
--					if mounted()==true then ClientMessage('SysMsg','Na koni se nem��e� skr�t',0) return end
					if hid_pocitani~=true then
					 hid_cntr = {} warmode_cancel() UO.Macro(13,21) hid_pocitani=true hidtimer=getticks() end
				end,
['fast HID']		=	function()
					if hid_pocitani~=true then
					 hid_cntr = {} warmode_cancel() UO.Macro(13,21) wait(10) always_run('on') hid_pocitani=true hidtimer=getticks() end
				end,
['detect hiding']	=	function() action['do'](function()
					if hid_pocitani==true then hid_pocitani=false ClientMessage('ExMsg','hid: canceled',0,UO.CharID) end
					ClientMessage('SysMsg','Chces se pokusit hledat skryte',0) UO.Macro(13,14)
				end) end,
['meditace']       =      function() UO.Macro(13,46) end,
['forensics']      =      function() UO.Macro(13,19) end,
['item identification'] = function() UO.Macro(13,3) end,
['animal lore']    =      function() UO.Macro(13,2) end,
['animal taming']  =      function() UO.Macro(13,35) end,
['anatomy']        =      function() UO.Macro(13,1) end,
['eval int']       =      function() UO.Macro(13,16) end,
['spirit speak']   =      function() UO.Macro(13,32) end,
['cmuchani']       =      function() if mounted()==true then ClientMessage('SysMsg','Na koni nem��e� �muchat',0) return end
                                     UO.TargCurs=true UO.SysMessage('Kde chce� �muchat?',54) while UO.TargCurs==true do wait (100) if getkey('esc') then UO.TargCurs=false return end end
                                     if UO.LTargetID==UO.CharID then ClientMessage('SysMsg','Pro� �muchat ve vlastn�m batohu?',0) return end
--                                     local bagl=World().InContainer(UO.LTargetID).WithType(3701).Items
                                     local bagl=ScanItems(false,{Type=3701,ContID=UO.LTargetID})
                                     if bagl[1]~=nil then use_object(bagl[1].ID) ClientMessage('SysMsg','�much�m',0) else ClientMessage('SysMsg','Batoh nenalezen!',0) end end,
['stealing']       =      function() UO.Macro(13,33) end,
['provokace']      =      function() warmode_cancel() UO.Macro(13,22) end,
['usmirovani']     =      function() warmode_cancel() UO.Macro(13,9) end,
['lakani']         =      function() warmode_cancel() UO.Macro(13,15) end,
['tracking']       =      function() warmode_cancel() UO.Macro(13,38) end,
['track players']  =      function() warmode_cancel() UO.Macro(13,38) timeout = getticks() + 300
                                     repeat wait (1) until UO.ContName == 'objpicker gump' or timeout < getticks()
                                     UO.Click(UO.CursorX,UO.CursorY,true,true,true,false)
                                     UO.Click(UO.ContPosX+172,UO.ContPosY+65,true,true,true,false)
                                     UO.Click(UO.ContPosX+172,UO.ContPosY+65,true,true,true,false) end,
--                                     UO.Macro(2,0,'zav�t�ila') end,
['track animals']  =      function() warmode_cancel() UO.Macro(13,38) timeout = getticks() + 300
                                     repeat wait (1) until UO.ContName == 'objpicker gump' or timeout < getticks()
                                     UO.Click(UO.CursorX,UO.CursorY,true,true,true,false)
                                     UO.Click(UO.ContPosX+64,UO.ContPosY+64,true,true,true,false)
                                     UO.Click(UO.ContPosX+64,UO.ContPosY+64,true,true,true,false) end,
['track all']     =      function() warmode_cancel() UO.Macro(13,38) timeout = getticks() + 300
                                     repeat wait (1) until UO.ContName == 'objpicker gump' or timeout < getticks()
                                     UO.Click(UO.CursorX,UO.CursorY,true,true,true,false)
                                     UO.Click(UO.ContPosX+208,UO.ContPosY+65,true,true,true,false)
                                     UO.Click(UO.ContPosX+208,UO.ContPosY+65,true,true,true,false) end,
---PET CONTROL
['all stay']       =      function() UO.Macro(3,0,'all stay') end,
['all come']       =      function() UO.Macro(3,0,'all come') end,
['all stop']       =      function() UO.Macro(3,0,'all stop') end,
['all go']         =      function() UO.Macro(3,0,'all go') end,
['pet do baglu']   =      function() if pet then
                                      local zmensovak = ScanItems(true,{Type=lektvar_zmensovani['type'],Col=lektvar_zmensovani['col'],Kind=0})
                                      local obr = ScanItems(true,{ID=pet['ID'],Kind=1,Dist=3})
                                      if zmensovak[1]~=nil and obr[1]~=nil then dragndrop(zmensovak[1].ID,1,obr[1].ID) else ClientMessage('SysMsg','Pet nenalezen!',0) return end
                                      wait(200) wait(minimal_pause-200)
                                      local mrnousek = ScanItems(true,{Type=pet_mini['Type'],Kind=1,Dist=3})
                                      if mrnousek[1]~=nil then dragndrop(mrnousek[1].ID,1,UO.CharID) end
                                     else ClientMessage('SysMsg','Pet nebyl nastaven!',0) end end,
['pet z baglu']    =      function() if pet_mini then
                                      local pet = ScanItems(true,{Type=pet_mini['Type'],Col=pet_mini['Col'],Kind=0})
                                      if pet[1] ~= nil then use_object(pet[1].ID) else ClientMessage('SysMsg','Pet nenalezen!',0) return end
                                     else ClientMessage('SysMsg','Pet nebyl nastaven!',0) end end,
---MOUNT
-- ['naskoc']         =      function() local fdebugtimer = getticks() if mounted()==true then ClientMessage('SysMsg','Already mounted!',0) return end
--                                      local mini = ScanItems(true,{Type=mount_mini['Type'],Col=mount_mini['Col'],Kind=0})
--                                      if mini[1] ~= nil then use_object(mini[1].ID) wait(minimal_pause) end
--                                      local kul = ScanItems(true,{ID=kul_id,Dist=3,Kind=1})
--                                      if kul[1] ~= nil then use_object(kul[1].ID) wait(minimal_pause) end
--                                      if mini[1]==nil and kul[1]==nil then UO.Macro(3,0,tostring(mount['Name'])..(' stay')) end
--                                      local kun = ScanItems(true,{ID=mount['ID'],Dist=3})
--                                      if kun[1] ~= nil then use_object(kun[1].ID) end
--                                      if kul[1] ~= nil then wait(minimal_pause) dragndrop(kul[1].ID,1,UO.BackpackID) end
--                                      if kun[1] == nil then ClientMessage('SysMsg','Mount nenalezen!',0) end
--                                      ClientMessage('SysMsg','DEBUG - naskoc '..getticks()-fdebugtimer,5555) end,
['naskoc']		=	function()
					local fdebugtimer = getticks()
					local mini=nil local kul=nil local kun=nil
					local charX=UO.CharPosX local charY=UO.CharPosY
					local ncnt = UO.ScanItems(true)
					for nindex = 0,(ncnt-1) do
					 local nid,ntype,nkind,ncontid,nx,ny,nz,nstack,nrep,ncol = UO.GetItem(nindex)
					 local ndist=math.max(math.abs(nx-charX),math.abs(ny-charY))
					 if Table['HasValue'](mounti,ntype) then ClientMessage('SysMsg','Already mounted!',0) ClientMessage('SysMsg','DEBUG - naskoc '..getticks()-fdebugtimer,5555) return
					 elseif ntype==mount_mini['Type'] and ncol==mount_mini['Col'] and nkind==0 then
					  action['do'](function()use_object(nid)end) action['do'](function()use_object(mount['ID'])end) ClientMessage('SysMsg','DEBUG - naskoc '..getticks()-fdebugtimer,5555) return
					 elseif nid==kul_id and ndist<4 and nkind==1 then kul=nid
					 elseif nid==mount['ID'] and ndist<4 then kun=nid end
					end
					if kul then action['do'](function()use_object(kul)end) end
					if mini==nil and kul==nil then UO.Macro(3,0,tostring(mount['Name'])..(' stay')) end
					if kun then action['do'](function()use_object(kun)end) end
					if kul then action['do'](function()dragndrop(kul,1,UO.BackpackID)end) end
					if not kun then ClientMessage('SysMsg','Mount nenalezen!',0) end
					ClientMessage('SysMsg','DEBUG - naskoc '..getticks()-fdebugtimer,5555)
				end,
['seskoc']         =      function() if mounted()==false then if mount then UO.Macro(3,0,tostring(mount['Name'])..(" stay")) volani_kone=getticks()-3000 end
                                     else warmode_cancel() use_object(UO.CharID) end end,
['uvaz']           =      function() if mount and kul_id then
				       if mounted()==true then warmode_cancel() use_object(UO.CharID) UO.Macro(3,0,tostring(mount['Name'])..(" stay")) volani_kone=getticks()-3000 wait(minimal_pause) end
                                       local kun = ScanItems(true,{ID=mount['ID'],Dist=3})
                                       if kun[1] ~= nil then
                                         local kul = ScanItems(true,{ID=kul_id})
                                         if kul[1]~=nil then
                                           if kul[1].Kind == 0 or (kul[1].Dist <= 3 and (kul[1].X~=kun[1].X or kul[1].Y~=kun[1].Y or kul[1].Z~=kun[1].Z)) then
                                             UO.Drag(kul_id) time_to_run=getticks()+200 repeat wait(1) wait(minimal_pause-1) until UO.LLiftedID==kul_id or time_to_run<getticks() UO.DropG(kun[1].X,kun[1].Y,kun[1].Z)  wait (100)
                                           wait(minimal_pause-100) use_object(kul_id) WaitForTarCurs(300) target_object(kun[1].ID,1)
                                           elseif kul[1].Kind == 1 and kul[1].Dist <= 3 and kul[1].X==kun[1].X and kul[1].Y==kun[1].Y and kul[1].Z==kun[1].Z then
                                             use_object(kul_id) WaitForTarCurs(300) target_object(kun[1].ID,1) end
                                         else ClientMessage('SysMsg','Kul nenalezen!',0) end
                                       else ClientMessage('SysMsg','Mount nenalezen!',0) end
				     else ClientMessage('SysMsg','Mount nebo kul nejsou nastaveny!',0)
				     end end,
---LOOTER
['loot mrtvoly']   =      function() if ltr_global['lootovani']==nil then
                                      local mrtvola = ScanItems(true,{Type=8198,Kind=1,Dist=1},{ID=vylooceno})
                                      if mrtvola[1]~=nil then ClientMessage('SysMsg','Looter: '..mrtvola[1].Name,60) if looter_func['open_container'](mrtvola[1]) then looter_func['start_loot'](mrtvola[1],'loot') else return end
                                      else ClientMessage('SysMsg','Looter: mrtvola nenalezena',60) end
                                     else ClientMessage('SysMsg','Looter: uz lootuju',60) end end,
['loot target']    =      function() if ltr_global['lootovani']==nil then
                                      UO.TargCurs = true UO.SysMessage('Looter: Co chces vylootit?',60)
                                      local tOut = getticks()
                                      while UO.TargCurs == true do wait (100) if getkey('esc') or tOut<getticks()-3000 then UO.TargCurs = false ClientMessage('SysMsg','Looter: timeout',60) return end end
                                      local targeted_container = ScanItems(true,{ID=UO.LTargetID})
                                      if targeted_container[1] then ClientMessage('SysMsg','Looter: '..targeted_container[1].Name,60) if looter_func['open_container'](targeted_container[1]) then looter_func['start_loot'](targeted_container[1],'loot') else return end
                                      else ClientMessage('SysMsg','Looter: nadoba nenalezena',60) end
                                     else ClientMessage('SysMsg','Looter: uz lootuju',60) end end,
['loot all']       =      function() if ltr_global['lootovani']==nil then
                                      UO.TargCurs = true UO.SysMessage('Looter: Co chces vyluxovat?',60)
                                      local tOut = getticks()
                                      while UO.TargCurs == true do wait (100) if getkey('esc') or tOut<getticks()-3000 then UO.TargCurs = false ClientMessage('SysMsg','Looter: timeout',60) return end end
                                      local targeted_container = ScanItems(true,{ID=UO.LTargetID})
                                      if targeted_container[1] then ClientMessage('SysMsg','Looter: '..targeted_container[1].Name,60) if looter_func['open_container'](targeted_container[1]) then looter_func['start_loot'](targeted_container[1],'vse') else return end
                                      else ClientMessage('SysMsg','Looter: nadoba nenalezena',60) end
                                     else ClientMessage('SysMsg','Looter: uz lootuju',60) end end,
--['loot vysyp']   =      function() if lootovani==0 then
--                                    if UO.ContID~=lootBagID then UO.NextCPosX=0 UO.NextCPosY=0 use_object(lootBagID) lootovani=1 loot_pause=getticks() end
--                                   else ClientMessage('SysMsg','Looter: uz lootuju',60) end end,
['loot vysyp']     =      function() roztrid(lootit,lootBagID) end,
['nastrihat hadry'] =     function() local nuzticky = ScanItems(true,{Type=nuzky['type'],ContID=bagl_id,Kind=0}) if nuzticky[1]==nil then ClientMessage('SysMsg','Looter: Nemas nuzky',60) return end
                                     local umyvadylko = ScanItems(true,{Type=umyvadlo['type'],ContID=UO.BackpackID,Kind=0}) if umyvadylko[1]==nil then ClientMessage('SysMsg','Looter: Nemas umyvadlo',60) return end
                                     looter_func['strihani_hadriku'](nuzticky[1].ID,umyvadylko[1].ID,lootBagID)	
				      end,
---SLOVA PRIKAZY
['koupim']		=	function() UO.Macro(3,0,'koupim') end,
['guards']		=	function() UO.Macro(3,0,'guards') end,
---MISCELLANEOUS
['nastav mount']   =      function() UO.TargCurs=true wait(200) UO.SysMessage('Zamer kone',5555) while UO.TargCurs==true do wait(100) if getkey('esc') then ClientMessage('SysMsg','Stop',5555) return end end
                                     local getitem = ScanItems(true,{ID=UO.LTargetID,Kind=1})
				     if getitem[1] then local localName = getitem[1].Name --localName = getitem[1].Name:sub(1, -2) 
				       writetofile("mount = {['Name']='"..localName.."',['ID']="..getitem[1].ID.."}",varfiles['mount'],'w') mount={['Name']=localName,['ID']=getitem[1].ID}
			               ClientMessage('SysMsg','Mount '..tostring(localName)..' nastaven',5555)
				     else ClientMessage('SysMsg','Chyba! Nelze pouzit na itemy v batohu',38) end end,
['nastav mount mini'] =   function() UO.TargCurs=true wait(200) UO.SysMessage('Zamer zmenseneho kone',5555) while UO.TargCurs==true do wait(100) if getkey('esc') then ClientMessage('SysMsg','Stop',5555) return end end
                                     local getitem = ScanItems(true,{ID=UO.LTargetID,Kind=0})
				     if getitem[1] then
				       writetofile("mount_mini = {['Name']='"..getitem[1].Name.."',['Type']="..getitem[1].Type..",['Col']="..getitem[1].Col.."}",varfiles['mount_mini'],'w')
			               mount_mini = {['Name']=getitem[1].Name,['Type']=getitem[1].Type,['Col']=getitem[1].Col}
			               ClientMessage('SysMsg','Mount (mini) '..tostring(getitem[1].Name)..' nastaven',5555)
				     else ClientMessage('SysMsg','Chyba! Item musi byt v batohu',38) end end,
['nastav pet']     =      function() UO.TargCurs=true wait(200) UO.SysMessage('Zamer peta',5555) while UO.TargCurs==true do wait(100) if getkey('esc') then ClientMessage('SysMsg','Stop',5555) return end end
                                     local getitem = ScanItems(true,{ID=UO.LTargetID,Kind=1})
				     if getitem[1] then local localName = getitem[1].Name --localName = getitem[1].Name:sub(1, -2)
				       writetofile("pet = {['Name']='"..localName.."',['ID']="..getitem[1].ID.."}",varfiles['pet'],'w') pet={['Name']=localName,['ID']=getitem[1].ID}
			               ClientMessage('SysMsg','Pet '..tostring(localName)..' nastaven',5555)
				     else ClientMessage('SysMsg','Chyba! Nelze pouzit na itemy v batohu',38) end end,
['nastav pet mini'] =     function() UO.TargCurs=true wait(200) UO.SysMessage('Zamer zmenseneho peta',5555) while UO.TargCurs==true do wait(100) if getkey('esc') then ClientMessage('SysMsg','Stop',5555) return end end
                                     local getitem = ScanItems(true,{ID=UO.LTargetID,Kind=0})
				     if getitem[1] then
				       writetofile("pet_mini = {['Name']='"..getitem[1].Name.."',['Type']="..getitem[1].Type..",['Col']="..getitem[1].Col.."}",varfiles['pet_mini'],'w')
			               pet_mini = {['Name']=getitem[1].Name,['Type']=getitem[1].Type,['Col']=getitem[1].Col}
			               ClientMessage('SysMsg','Pet (mini) '..tostring(getitem[1].Name)..' nastaven',5555)
				     else ClientMessage('SysMsg','Chyba! Item musi byt v batohu',38) end end,
['nastav kul'] =          function() UO.TargCurs=true wait(200) UO.SysMessage('Zamer kul',5555) while UO.TargCurs==true do wait(100) if getkey('esc') then ClientMessage('SysMsg','Stop',5555) return end end
                                     local getitem = ScanItems(true,{ID=UO.LTargetID,Kind=0})
				     if getitem[1] then
				       writetofile('kul_id = '..getitem[1].ID,varfiles['kul_id'],'w')
			               kul_id = getitem[1].ID
			               ClientMessage('SysMsg','Kul '..tostring(getitem[1].Name)..' nastaven',5555)
				     else ClientMessage('SysMsg','Chyba! Item musi byt v batohu',38) end end,
['toggle sound']   =      function() UO.Macro(8,0) local pack=wait_for_gump(1,500) if pack==false then return end -- repeat wait(100) until UO.ContName=='OptionsGump'
                                     UO.Click(422,233,true,true,true,false) wait(1000)
                                     UO.Click(828,536,true,true,true,false) end,
['always run']     =      function() always_run() end,                                                                       
['use hand']       =      function() UO.Macro(1,0,'.usehand') end,
['transparency']   =      function() UO.Macro(29,0) end,
['open journal']   =      function() UO.Macro(8,3) end,
['open map']       =      function() UO.Macro(8,0) repeat wait(100) until UO.ContName=='OptionsGump'
                                     UO.Click(980,220,true,true,true,false) wait(1000) -- interface
                                     UO.Click(425,408,true,true,true,false) wait(1000) -- disable menu bar
                                     UO.Click(639,537,true,true,true,false) wait(1000) -- pouzit
                                     UO.Click(68,21,true,true,true,false) wait(1000)   -- mapa
--                                   UO.Click(68,21,true,true,true,false) wait(1000)   -- mapa
                                     UO.Click(425,408,true,true,true,false) wait(1000) -- disable menu bar
                                     UO.Click(828,536,true,true,true,false) end,
['open paperdoll'] =      function() UO.Macro(8,1) end, -- local pack=wait_for_gump(1,500) if pack==false then return end UO.ContPosX=808 UO.ContPosY=-16 end,
['open backpacks'] =      function() UO.NextCPosX=poloha_gumpu['backpack']['x'] UO.NextCPosY=poloha_gumpu['backpack']['y'] use_object(UO.BackpackID) local pack1=wait_for_cont(UO.BackpackID,1,500) if pack1==false then return end
                                     if bagl_id then UO.NextCPosX=0 UO.NextCPosY=0 use_object(bagl_id) local pack2=wait_for_cont(bagl_id,1,500) if pack2==false then return end UO.ContPosX=poloha_gumpu['batoh2']['x'] UO.ContPosY=poloha_gumpu['batoh2']['y'] end end,
['buff lista']     =      function() UO.Macro(1,0,'.buff_lista') end,
['dekorater']      =      function() UO.Macro(1,0,'.dekorater') end,
['SchopText']      =      function() UO.Macro(1,0,'.schoptext') local pack=wait_for_gump(1,500) if pack==true then UO.ContPosX=UO.CursorX-50 UO.ContPosY=UO.CursorY-70 end end,
['mapa - on']      =      function() cm_mapa('on') end,
--['mapa - off']   =      function() cm_mapa('off') uoam_prepinac='off' end,
['gamma - on']     =      function() cm_gamma('on') gamma_prepinac='on' end,
['gamma - off']    =      function() cm_gamma('off') gamma_prepinac='off' end,
['print skills']   =      function() ClientMessage('SysMsg','Bojov�',5555) for i=1,#skilly['Bojov�'] do ClientMessage('SysMsg',tostring(skilly['Bojov�'][i]['name'])..' '..tostring(skilly['Bojov�'][i]['value'][1]/10)..'/'..tostring(skilly['Bojov�'][i]['value'][3]/10),1259) end
                                     ClientMessage('SysMsg','V�robn�',5555) for i=1,#skilly['V�robn�'] do ClientMessage('SysMsg',tostring(skilly['V�robn�'][i]['name'])..' '..tostring(skilly['V�robn�'][i]['value'][1]/10)..'/'..tostring(skilly['V�robn�'][i]['value'][3]/10),1673) end
                                     ClientMessage('SysMsg','Magick�',5555) for i=1,#skilly['Magick�'] do ClientMessage('SysMsg',tostring(skilly['Magick�'][i]['name'])..' '..tostring(skilly['Magick�'][i]['value'][1]/10)..'/'..tostring(skilly['Magick�'][i]['value'][3]/10),1266) end
                                     ClientMessage('SysMsg','Hrani���sk�',5555) for i=1,#skilly['Hrani���sk�'] do ClientMessage('SysMsg',tostring(skilly['Hrani���sk�'][i]['name'])..' '..tostring(skilly['Hrani���sk�'][i]['value'][1]/10)..'/'..tostring(skilly['Hrani���sk�'][i]['value'][3]/10),68) end
                                     ClientMessage('SysMsg','Zlod�jsk�',5555) for i=1,#skilly['Zlod�jsk�'] do ClientMessage('SysMsg',tostring(skilly['Zlod�jsk�'][i]['name'])..' '..tostring(skilly['Zlod�jsk�'][i]['value'][1]/10)..'/'..tostring(skilly['Zlod�jsk�'][i]['value'][3]/10),55) end
                                     ClientMessage('SysMsg','Bardsk�',5555) for i=1,#skilly['Bardsk�'] do ClientMessage('SysMsg',tostring(skilly['Bardsk�'][i]['name'])..' '..tostring(skilly['Bardsk�'][i]['value'][1]/10)..'/'..tostring(skilly['Bardsk�'][i]['value'][3]/10),1378) end end,
['print hotkeys']  =      function() if hotkeys_file then
                                     while true do
                                           local radek = hotkeys_file:read("*line")
                                           if radek==nil then break
                                           else ClientMessage('SysMsg',radek,5555) end
                                           end end
                                     end,
---DALSI
--['koupim']         =      function() UO.TargCurs=true wait(100) while UO.TargCurs == true do wait(50) if getkey('esc') then return end end
--                          local obchodnik=UO.LTargetID
--                          UO.Popup(obchodnik,UO.CursorX,UO.CursorY)
--                          end,

['autopilot']      =      function() if autopilot==0 then autopilot=1 autopilot_ID=UO.LTargetID always_run('on') ClientMessage('SysMsg','Autopilot on',5555) UO.Click(UO.CursorX,UO.CursorY,false,false,true,false) elseif autopilot==1 then autopilot=0 ClientMessage('SysMsg','Autopilot off',5555) end end,
['truhlicka']      =      function() use_object(1074810686) WaitForTarCurs(500) target_object(1074812913,1) end,
['obvaz']          =      function() UO.Macro(1,0,'.obvaz') WaitForTarCurs(500) target_object(UO.CharID,1) end,
['kuchani']        =      function() for i=1,#zbrane_kuchani do if lastequiped['type']==zbrane_kuchani[i] then UO.Macro(1,0,'.usehand') return end end
                                     local nuz = ScanItems(true,{Type=nuz_na_stahovani['type'],Kind=0})
                                     if nuz[1] ~= nil then use_object(nuz[1].ID)
                                     else ClientMessage('SysMsg','Nemas nuz na stahovani',5555) end end,
['hlava']          =      function() UO.Macro(1,0,'.utrhni_hlavu') end,
['otevri']         =      function() local doors=ScanItems(true,{Type=zavrene_dvere,Dist=2,RelZ={-5,-4,-3,-2,-1,0,1,2,3,4,5}})
                                     if doors then if #doors==1 then use_object(doors[1].ID) return else
                                      local filter=doors local doors={} for i=1,#filter do if filter[i].Dist==1 then table.insert(doors,filter[i]) end end
                                      if #doors==1 then use_object(doors[1].ID) return else
                                       local filter=doors local doors={} for i=1,#filter do if filter[i].RelX==0 or filter[i].RelY==0 then table.insert(doors,filter[i]) end end
                                       if #doors==1 then use_object(doors[1].ID) return
                                       elseif #doors==2 then
                                        if doors[1].Y==doors[2].Y then
                                         if math.abs(doors[1].RelX)<math.abs(doors[2].RelX) then use_object(doors[1].ID)
                                         elseif math.abs(doors[1].RelX)>math.abs(doors[2].RelX) then use_object(doors[2].ID) return end
                                        elseif doors[1].X==doors[2].X then
                                         if math.abs(doors[1].RelY)<math.abs(doors[2].RelY) then use_object(doors[1].ID)
                                         elseif math.abs(doors[1].RelY)>math.abs(doors[2].RelY) then use_object(doors[2].ID) return end
                                        else ClientMessage('SysMsg','Chyba',0) end
                                	else ClientMessage('SysMsg','Chyba: '..tostring(#doors)..' dveri k otevreni',0) end end end
                                     else ClientMessage('SysMsg','Dvere nenalezeny',0)
                                     end end,
-- ['zavri']          =      function() local doors = ScanItems(true,{Type=otevrene_dvere,Dist=2,RelZ={-5,-4,-3,-2,-1,0,1,2,3,4,5}})
--                                      for i = 1,#doors do if doors[i] ~= nil then if i > 1 then wait(minimal_pause) end use_object(doors[i].ID) end end end,
['zavri']		=	function()
					local doors = ScanItems(true,{Type=otevrene_dvere,Dist=2,RelZ={-5,-4,-3,-2,-1,0,1,2,3,4,5}})
					for i = 1,#doors do if doors[i] then action['do'](function()use_object(doors[i].ID)end) end end
				end,
['kopat poklad']   =      function() UO.Macro(1,0,'.kopat_poklad') end,
['napln toulec']   =      function() UO.TargCurs=true UO.SysMessage('Vyber ��py',54) repeat wait(10) until UO.TargCurs==false
                                     local sipy = ScanItems(true,{ID=UO.LTargetID,Type={3903,7163},Kind=0})
                                     local toulec = ScanItems(true,{Type=15193,ContID=UO.CharID})
                                     local toulec_kapacita=nil local odecti=0
                                     if sipy[1]~=nil and toulec[1]~=nil then
                                      if sipy[1].ContID~=UO.BackpackID then
                                       local sipu_v_toulci=String['ToNumber'](toulec[1].Details) if sipu_v_toulci~=nil then odecti=sipu_v_toulci end
                                       if toulec[1].Name=='Obrovsk� Toulec ' then toulec_kapacita=500-odecti
                                       elseif toulec[1].Name=='Velk� Toulec ' then toulec_kapacita=400-odecti
                                       elseif toulec[1].Name=='Toulec ' then toulec_kapacita=300-odecti
                                       elseif toulec[1].Name=='Mal� Toulec ' then toulec_kapacita=200-odecti end
                                       dragndrop(sipy[1].ID,toulec_kapacita,UO.BackpackID) wait(minimal_pause) end
                                      use_object(toulec[1].ID) WaitForTarCurs(500) target_object(sipy[1].ID,1)
                                     else ClientMessage('SysMsg','Tohle nejde',38) end end,
['vysyp toulec']   =      function() local toulec = ScanItems(true,{Type=15193,ContID=UO.CharID})
                                     if toulec[1]~=nil and toulec[1].Details~='Prazdny' then use_object(toulec[1].ID) WaitForTarCurs(200) target_object(toulec[1].ID,1)
                                     else ClientMessage('SysMsg','Nem� toulec s ��py',38) end end,
['prelejvator']    =      function() UO.TargCurs = true wait(200) UO.SysMessage('Co chce� vysypat?',5555)
                                     while UO.TargCurs == true do wait (100) if getkey('esc') then ClientMessage('SysMsg','Stop',5555) return end end
                                     local vysypat = UO.LTargetID
                                     UO.TargCurs = true wait(200) UO.SysMessage('Kam to chce� vysypat?',5555)
                                     while UO.TargCurs == true do wait (100) if getkey('esc') then ClientMessage('SysMsg','Stop',5555) return end end
                                     local kos = UO.LTargetID
                                     if vysypat~=nil and vysypat~=0 and kos~=nil and kos~=0 then
                                       local itemy = ScanItems(true,{ContID=vysypat})
                                       if itemy~=nil then
                                         for i=1,#itemy do dragndrop(itemy[i].ID,itemy[i].Stack,kos) wait(1000) end
                                       end
                                     end ClientMessage('SysMsg','Hotovo',5555) end,
['hide item']    =      function() UO.TargCurs = true UO.SysMessage('Co chce� schovat?',5555) while UO.TargCurs == true do wait (100) end UO.HideItem(UO.LTargetID) end,
['resync']       =      function() UO.Macro(1,0,'.resync') end,
---PODPROGRAMY
--makra
['bard']         =      function() local dothis = dofile(getinstalldir()..'/scripts/makra/bard/bardi_skript.lua')
				ClientMessage('SysMsg','Bard skoncil',5555) end,
['lumberjack']   =      function() local dothis = dofile(getinstalldir()..'/scripts/makra/lumberjack.lua')
				ClientMessage('SysMsg','Lumberjack skoncil',5555) end,
['makelast']     =      function() setatom('Escape','false')
				while getatom('Escape')~='true' do 
				UO.Macro(1,0,'.makelast')
                                local time_to_run = getticks() + 12000
                                wait(10) UO.SysMessage('',0)
                                repeat wait(50)
                                until time_to_run < getticks() or UO.SysMsg:find('Pokl�d� ')
				end
				ClientMessage('SysMsg','Makelast skoncil',5555) end,
['gorily']       =      function() setatom('Escape','false')
				local dothis = dofile(getinstalldir()..'/scripts/makra/gorily.lua')
				ClientMessage('SysMsg','Gorily skoncil',5555) end,
--tools
['sniffer']      =      function() local dothis = dofile(getinstalldir()..'/scripts/tools/sniffer.lua')
				ClientMessage('SysMsg','Sniffer skoncil',5555) end,
['renameplus']    =      function() local dothis = dofile(getinstalldir()..'/scripts/tools/renameplus.lua')
				ClientMessage('SysMsg','Rename Plus skoncil',5555) end,
}
if files['file_exist'] ('custom.lua') then
 local custom_fns = dofile('custom.lua')
 for key,func in pairs(Funcs_custom) do Funcs[key]=func end
end
---------------
---- INIT -----
---------------
UO.Macro(32,0) wait(100) if UO.SysMsg=='Always Run is now on.' then UO.Macro(32,0) end
--j=journal:new()
---------------
-- MAIN LOOP --
---------------
while UO.CliLogged==true and getatom('Reload')~='true' do
-- local loopticks={}									-- DEBUG - loopticks
-- for f=1,100 do										-- DEBUG - loopticks
--     local loopstartticks=getticks()							-- DEBUG - loopticks
    setatom('Request','true')
    while getatom('Request') == 'true' do wait(loop_wait) end
    cmd = getatom('Request')
    if cmd ~= 'nil' then -- ClientMessage('SysMsg','PUSH: '..cmd,5555)
      if Funcs[cmd] then Funcs[cmd]() end
      cmd = 'nil'
    end        
    if hid_pocitani==true then hid_pocitadlo() end
--    if dyka_pocitani==true then dyka_pocitadlo() end
--    if skok_pocitani==true then skok_pocitadlo() end
-- looter --
    if ltr_global['lootovani'] and ltr_global['loot_pause'] then
      if ltr_global['lootovani']~=1 and ltr_global['loot_pause']<getticks()-500 then looter_func['main_fce'](ltr_global['lootovani'],ltr_global['typlootu']) ltr_global['loot_pause']=getticks() end
      if ltr_global['lootovani']==1 and ltr_global['loot_pause']<getticks()-500 then looter_func['roztrid'](lootBagID) ltr_global['loot_pause']=getticks() end
      if ltr_global['lootovani']~=1 and ltr_global['loot_pause']<getticks()-3000 then ClientMessage('SysMsg','Looter: timeout',60) ltr_global={} end
    end
------------
    if tarcurs_remover>0 and UO.TargCurs==true then tarcurs_remover=tarcurs_remover-1 UO.TargCurs=false end
    if UO.CliLogged==true and (UO.MaxHits==0 or UO.MaxStam==0) and status_prodleva<getticks()-1000 then UO.Macro(8,2) UO.Macro(10,2) UO.ContPosX=poloha_gumpu['status']['x'] UO.ContPosY=poloha_gumpu['status']['y'] status_prodleva=getticks() end
    if UO.CharStatus:find('G') and warmode_alert<getticks()-10000 then warmode_alert=getticks() ClientMessage('SysMsg','Warning: Warmode ON',0) end
--    if potka_timer>getticks() then potka_pocitadlo() end
-- autopilot
    if autopilot==1 then
     if autopilot_overhead==0 then
      local get_target=ScanItems(true,{ID=autopilot_ID})
      if get_target[1]~=nil then
       UO.Pathfind(get_target[1].X,get_target[1].Y,get_target[1].Z) autopilot_overhead=autopilot_overhead+1
      else autopilot=0 autopilot_overhead=0 ClientMessage('SysMsg','Autopilot error',5555)
      end
     else autopilot_overhead=autopilot_overhead+1
     end
     if autopilot_overhead>2 then autopilot_overhead=0 end
    end 
-- stats
    if ((status['Hits']~=UO.Hits and UO.MaxHits~=0) or (status['Mana']~=UO.Mana and UO.MaxMana~=0) or (status['Stamina']~=UO.Stamina and UO.MaxStam~=0)) and UO.Hits~=nil and UO.Mana~=nil and UO.Stamina~=nil then stats_func['HP']() status['Hits']=UO.Hits status['Mana']=UO.Mana status['Stamina']=UO.Stamina end
    if status['EnemyHits']~=UO.EnemyHits and UO.EnemyHits~=nil and UO.EnemyID~=0 and UO.EnemyID~=nil then stats_func['enemy HP']() status['EnemyHits']=UO.EnemyHits end
    if (status['MinDmg']~=UO.MinDmg or status['MaxDmg']~=UO.MaxDmg) and (UO.MinDmg~=nil or UO.MaxDmg~=nil) and UO.MaxDmg~=0 then stats_func['DMG']() status['MinDmg']=UO.MinDmg status['MaxDmg']=UO.MaxDmg end
    if UO.Weight and UO.MaxWeight and (status['Weight']~=UO.Weight or status['MaxWeight']~=UO.MaxWeight) and UO.MaxWeight~=0 then stats_func['weight']() status['Weight']=UO.Weight status['MaxWeight']=UO.MaxWeight end
    if UO.AR and status['AR']~=UO.AR then stats_func['AR']() status['AR']=UO.AR end
    if UO.FR and status['FR']~=UO.FR and (UO.FR<500 or UO.FR>(-500)) then stats_func['FR']() status['FR']=UO.FR end
    if UO.CR and status['CR']~=UO.CR and (UO.CR<500 or UO.CR>(-500)) then stats_func['CR']() status['CR']=UO.CR end
    if UO.PR and status['PR']~=UO.PR and (UO.PR<500 or UO.PR>(-500)) then stats_func['PR']() status['PR']=UO.PR end
    if UO.ER and status['ER']~=UO.ER and (UO.ER<500 or UO.ER>(-500)) then stats_func['ER']() status['ER']=UO.ER end
-- journal
--    local jrnl=j:find('*vrh� d�ku*','|946|You see: '..tostring(jezditko['name'])..'|',' na skok.*')
--    local jrnl=j:find('*vrh� d�ku*',' na skok.*')
--    if jrnl==1 then if dyka_pocitani~=true then dyka_pocitani=true dyka_cntr={} dyka_timer=getticks() j:clear() end end
--    if jrnl==2 and mount=='mounted' then UO.Macro(3,0,tostring(jezditko['name'])..(' come')) j:clear() mount='dismounted' end
--    if jrnl==2 then if skok_pocitani~=true then skok_pocitani=true skok_cntr={} skok_timer=getticks() j:clear() end end
--    if jrnl==3 then krvko='activated' krvko_timer=getticks() UO.SysMessage('Krvko activated',5555) end
--run queues --
    if action['queue'][1]~=nil and action['wait']<getticks()-500 then action['queue'][1]() action['wait']=getticks() table.remove(action['queue'],1) end
    if sysmsg['queue'][1]~=nil and sysmsg['wait']<getticks()-100 then sysmsg['queue'][1]() table.remove(sysmsg['queue'],1) end
---------------
--     table.insert(loopticks,f,getticks()-loopstartticks)				-- DEBUG - loopticks
-- end											-- DEBUG - loopticks
-- local loopticks_avg = Table['Sum'](loopticks)/#loopticks				-- DEBUG - loopticks
-- ClientMessage('SysMsg','DEBUG: '..loopticks_avg..'ms (loop)',5555)			-- DEBUG - loopticks
end
if gamma_prepinac=='on' then cm_gamma('off') end
--if uoam_prepinac=='on' then cm_mapa('off') end
setatom ('Reload',nil) dofile('slave.lua')
