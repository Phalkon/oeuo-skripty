local files = {
['file_exist']		= function (file) local f=openfile(file) if f then f:close() return true else return false end end,
['FindItem']		= dofile(getinstalldir()..'/functions/FindItems.lua'),
['journal']		= dofile(getinstalldir()..'/functions/journal.lua'),
['communicator']	= dofile(getinstalldir()..'/functions/communicator.lua'),
['base64']		= dofile(getinstalldir()..'/functions/base64.lua'),
}
if files['file_exist'] (getinstalldir()..'/data/pswd.lua') then
files['pswd'] 		= dofile(getinstalldir()..'/data/pswd.lua') end

local log_journal = function (msg) end -- dodelat!
---------------
---- VARS -----
---------------
local minimal_pause = 500
local loop_wait=1
local ignore = {
'|54|Save svta za 10 vtein|', '|54|Svt se na chvli zastavil...|', '|54|Svt se zase zaal toit|',
'|0|It begins to snow.|', '|0|It begins to rain.|',
'|38|Lilien: a|AFK MODE|',
'|946|You see: Hugin|',
'|946|You see: Munin|',
'|946|You see: Karsten|',
'|946|You see: Brentan|',
'|946|You see: Kohout|',
'|946|You see: Waylan|',
'|946|You see: Loren|',
'|99|You see: Gamble|',
'|946|You see: Jiskra|',
'|99|You see: Patrik|',
'|99|You see: Carson|',
'|99|You see: Nicholas|',
'|99|You see: Vernon|',
'|99|You see: Erek|',
'|946|You see: Lensar|',
'|946|You see: Carlton|',
'|946|You see: Zachariah|',
'|946|You see: Abdiel|',
'|946|You see: Agustin|',
'|946|You see: Filbert|',
'|946|You see: Lili|',
'|946|You see: Gathi|',
'|946|You see: orel|',
'|946|You see: pralesni papousek|',
'|946|You see: vrana|',
'Vid, jak Lili prch',
'Vid, jak Gathi prch',
'|946|You see: Hart|',
'|54|Tve zlociny byly promlceny|',
'|38|You see: Edwin|',
'|38|You see: York|',
'|946|You see: Blahoslav|',
'|946|You see: Denton|',
'|946|You see: Harry|',
'|946|You see: Li|',
'|946|You see: Paxton|',
'Nestujte tak blizko.',
'Hej, krok zpatky.',
'Postojite kousek ode mne?',
'Nechci problemy, ale: Krok zpet!',
'Proc stojis tak blizko ke mne?',
--'popj lhev koYalky',
}
local UOMessage='HOTKEYS ZAPNUTY'
local reload=nil
function afk_msg() --[[ UO.Macro(2,0,'popj lhev koalky') ]] end
--local command={}
local modkeys = {'ctrl','alt','shift','space'}
local no_action_hotkeys = {
---TALKING
[{'m'}]                 =    function() typing('message') end,
[{'m','alt'}]           =    function() typing('whisper') end,
[{'m','shift'}]         =    function() typing('yell') end,
[{'n'}]                 =    function() typing('emote') end,
[{'b'}]                 =    function() typing('party') end,
---HESLA
[{'9'}]                 =    function() if truhla1 then wait(100) ClearUOMsg() wait(100) UO.Msg(dec(truhla1)) end end,     -- truhlicka
[{'0'}]                 =    function() if postovni_schranka then wait(100) ClearUOMsg() wait(100) UO.Msg(dec(postovni_schranka)) end end,     -- schranka
---PARTY CONTROL
[{'b','space'}]         =    function() ClearUOMsg() wait (100) UO.Msg ('/add'..'\013\010') WriteMsg(UOMessage) end,
[{'n','space'}]         =    function() ClearUOMsg() wait (100) UO.Msg ('/accept'..'\013\010') WriteMsg(UOMessage) end,
---TARGETY
--[{'1'}]                 =    function() UO.Macro(50,5) end,
--[{'2'}]                 =    function() UO.Macro(51,5) end,
--[{'3'}]                 =    function() if not UO.CharStatus:find('G') and UO.Hits>0 then UO.Macro(6,0) end UO.Macro(53,0) end,
--[{'1','alt'}]           =    function() UO.Macro(52,5) UO.Macro(53,0) end,
---PET CONTROL
--[{'d','shift'}]         =    function() UO.Macro(3,0,'all stay') end,
--[{'e','shift'}]         =    function() UO.Macro(3,0,'all come') end,
--[{'f','shift'}]         =    function() UO.Macro(3,0,'all stop') end,
--[{'r','shift'}]         =    function() UO.Macro(3,0,'all go') end,
---MISCELLANEOUS
[{'f1','space'}]        =    function() UO.Macro(2,0,'zavětřila') end,
--[{'r','space'}]         =    function() UO.Macro(3,0,'koupim') end,
---DALSI
[{'8','space'}]         =    function() setatom('Reload','true') reload='true' end,
[{'enter'}]             =    function() WriteMsg(UOMessage) end,
[{'esc'}]               =    function() setatom('Escape',true) keys_disabled=nil sekvence('off') UOMessage='HOTKEYS ZAPNUTY' ClearUOMsg() WriteMsg(UOMessage) end,
--[{'g'}]                 =    function() UO.Macro(3,0,'guards') end,
[{'y'}]                 =    function() ClearUOMsg() UOMessage='Sekvence mode' WriteMsg(UOMessage) sekvence('on') end,
[{'7','space'}]         =    function() ClearUOMsg() WriteMsg('PODPROGRAM?')
					 while not getkey('esc') do wait(50) if getkey('enter') then WriteMsg('PODPROGRAM?') end
--makra
					 if getkey('b') then repeat wait(10) until getatom('Request')=='true' setatom('Request','bard') UOMessage='PODPROGRAM: Bard' ClearUOMsg() WriteMsg(UOMessage) keys_disabled=true return end
					 if getkey('l') then repeat wait(10) until getatom('Request')=='true' setatom('Request','lumberjack') UOMessage='PODPROGRAM: Lumberjack' ClearUOMsg() WriteMsg(UOMessage) keys_disabled=true return end
 					 if getkey('o') then repeat wait(10) until getatom('Request')=='true' setatom('Request','makelast') UOMessage='PODPROGRAM: Makelast' ClearUOMsg() WriteMsg(UOMessage) keys_disabled=true return end
 					 if getkey('g') then repeat wait(10) until getatom('Request')=='true' setatom('Request','gorily') UOMessage='PODPROGRAM: Gorily' ClearUOMsg() WriteMsg(UOMessage) keys_disabled=true return end
--tools
 					 if getkey('s') then repeat wait(10) until getatom('Request')=='true' setatom('Request','sniffer') UOMessage='PODPROGRAM: Sniffer' ClearUOMsg() WriteMsg(UOMessage) keys_disabled=true return end
 					 if getkey('r') then repeat wait(10) until getatom('Request')=='true' setatom('Request','renameplus') UOMessage='PODPROGRAM: Rename Plus' ClearUOMsg() WriteMsg(UOMessage) repeat wait(1) until getkey('enter') or getkey('esc') UOMessage='HOTKEYS ZAPNUTY' ClearUOMsg() WriteMsg(UOMessage) return end
 					 end ClearUOMsg() WriteMsg('HOTKEYS ZAPNUTY') end,
[{'h','space'}]         =    function() local x=UO.CharPosX local y=UO.CharPosY ClearUOMsg() wait(200)
                                        UO.Msg('|AFK MODE') for i=1,140 do UO.Key('space') end
                                        j = journal:new()
                                        while not getkey('esc') and x==UO.CharPosX and y==UO.CharPosY do
                                          if j:next() ~= nil then
                                            local msg=j:last()
                                            if msg:find('|64|[DRUINA]: ') then UO.Msg ('/accept'..'\013\010') end
                                            for i=1,#ignore do if msg:find(ignore[i]) then msg=nil break end end
                                            if msg~=nil then
                                              local nHour, nMinute, nSecond, nMillisec = gettime() if nMinute<10 then nMinute = (0)..nMinute end
                                              local journal_msg = (nHour..(':')..nMinute..(' || ')..msg)
                                              cm_antiafk(journal_msg) print(journal_msg) afk_msg()
                                            end
                                            j:clear() 
                                          end
                                          wait(50) if getkey('enter') then ClearUOMsg() wait(200) UO.Msg('|AFK MODE') for i=1,140 do UO.Key('space') end end
                                        end ClearUOMsg() wait(100) UO.Msg('|HOTKEYS ZAPNUTY') for i=1,140 do UO.Key('space') end end,
}
local journal_monitoring = function() if j:next() ~= nil then
					local msg=j:last()
--					if msg:find('|64|[DRUINA]: ') then UO.Msg ('/accept'..'\013\010') end
				        for i=1,#ignore do if msg:find(ignore[i]) then msg=nil break end end
				        if msg~=nil then
				         local nHour, nMinute, nSecond, nMillisec = gettime() if nMinute<10 then nMinute = (0)..nMinute end
				         local journal_msg = (nHour..(':')..nMinute..(' || ')..msg)
					 cm_antiafk(journal_msg) log_journal(journal_msg)
					end
				       end end
---------------
---- FUNCS ----
---------------
function BelongsTo(x,y) for i,v in ipairs(y) do if v == x then return true end end end

function ClearUOMsg() for i=1,140 do UO.Key('back') end end
function WriteMsg(message) for i=1,140 do UO.Key('back') end wait(200) UO.Msg('|'..message) for i=1,140 do UO.Key('space') end end

function sekvence(toggle) if toggle=='on' then keys=sekvence_keys elseif toggle=='off' then keys=hotkeys end end

function typing(var)
  ClearUOMsg() wait(100)
  if     var=='message' then UO.Msg (''  )
  elseif var=='whisper' then UO.Msg ('; ')
  elseif var=='yell'    then UO.Msg ('! ')
  elseif var=='emote'   then UO.Msg (': ')
  elseif var=='party'   then UO.Msg ('/' ) end
  while true do wait (10)
        if getkey('enter') then while getkey('enter') do wait(10) end WriteMsg(UOMessage) return end
        if getkey('esc') then while getkey('esc') do wait(10) end ClearUOMsg() WriteMsg(UOMessage) return end
  end
end

function modifier_key(mod_key1,mod_key2)
 local notkeys = {}
 local cntr = 1
 for i=1,#modkeys do if mod_key1~=modkeys[i] and mod_key2~=modkeys[i] then notkeys[cntr]=modkeys[i] cntr=cntr+1 end end
 for i=1,#notkeys do if getkey(notkeys[i]) then return false end end
 if (getkey(mod_key1) or mod_key1==nil) and (getkey(mod_key2) or mod_key2==nil) then return true
 else return false end
end

---queue
queue = {}
queue.mt = {
     __index = queue,
}
 
function queue.New()
   local self = {}
   setmetatable(self,queue.mt)
   return self
end
 
function queue:push(s)
   if #self>0 then for i=1,#self do if self[i]:find(s) then return end end end -- brani pridani akce, pokud je uz ve fronte
   table.insert(self, 1, s)
end

--local queue_wait=getticks()
function queue:dequeue()
   local s
   if #self > 0 --[[ and queue_wait<getticks()-minimal_pause ]] then
        s = self[#self]
        self[#self] = nil
 --       queue_wait=getticks()
   end
   return s
end

---------------
---- INIT -----
---------------
--if UO.CliLogged==false and acc~=nil then repeat wait(100) until getkey('ctrl')
--  local pauzicka = 1000
--  UO.Msg(dec(acc)..'\013\010') -- wait (pauzicka) UO.Msg('\013\010')
--  UO.Click(260,110,false,true,true,false) wait (pauzicka)
--  UO.Msg('\013\010') wait (pauzicka)
--end
while UO.CliLogged==false do wait(1000) end
while getatom('uo_name')==nil do wait(1000) end
dofile(getinstalldir()..'/data/'..getatom('uo_name')..'/hotkeys.lua')
local Commands = queue.New()
local keys=hotkeys
function sekvence(toggle) if toggle=='on' then keys=sekvence_keys elseif toggle=='off' then keys=hotkeys end end function keys_enabled() keys_disabled=nil sekvence('off') ClearUOMsg() WriteMsg('HOTKEYS ZAPNUTY') end
WriteMsg(UOMessage) 
---------------
-- MAIN LOOP --
---------------
while UO.CliLogged==true and reload==nil do wait(loop_wait)
  for key,command in pairs(no_action_hotkeys) do
    if getkey(key[1]) and modifier_key(key[2],key[3]) then command() while getkey(key[1]) do wait(1) end end
  end
 if not keys_disabled then
  for key,command in pairs(keys) do
    if getkey(key[1]) and modifier_key(key[2],key[3]) then --[[ UO.SysMessage('PUSH: '..command,5555) ]] Commands:push(command) while getkey(key[1]) do wait(1) end if keys==sekvence_keys then keys_enabled() end end
  end
  if getatom('Request') == 'true' then wait(10) -- divna pauza... resi problem se zasekavanim skriptu
    command = Commands:dequeue()
    if command then setatom('Request',command) --[[ UO.SysMessage('DEQUEUE: '..command,5555) ]] else setatom('Request','nil') end   
  end
 end
end
dofile('master.lua')
