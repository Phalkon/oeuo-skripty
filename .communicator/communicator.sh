dir=/home/phalkon/Software/Wine/Andaria/OpenEUO/.communicator

tail -fn0 "$dir"/communicator_file | while read line

do 

if [[ $line =~ .*SCREEN!.* ]]
  then
#      gnome-screenshot
      scrot /home/phalkon/Pictures/Screenshots/Screenshot_%Y-%m-%d_%H-%M-%S.png

elif [[ $line =~ .*GAMMASET!.* ]]
  then
#      gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled false
      xgamma -gamma 1.5
#      gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled true

elif [[ $line =~ .*GAMMARESET!.* ]]
  then
#      gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled false
      xgamma -gamma 1.0
#      gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled true

elif [[ $line =~ .*MAPA!.* ]]
  then
      /home/phalkon/Software/Wine/Andaria/Bin/UOAM > /dev/null 2>&1 &

elif [[ $line =~ .*kill_MAPA!.* ]]
  then
      killall uoam.exe

elif [[ $line =~ .*ANTIAFK!.* ]]
  then
      echo $line > "$dir"/antiafk/lastmsg
      iconv -f windows-1250 -t utf-8 "$dir"/antiafk/lastmsg > "$dir"/antiafk/lastmsg_utf
      notify-send -i ultima_online "ANTIAFK" "$(cat "$dir"/antiafk/lastmsg_utf)"
#     paplay --volume=131072 "$dir"/antiafk/bell.wav

elif [[ $line =~ .*RESTART!.* ]]
  then
      killall client_6.0.13.0.exe
      sleep 10
      /home/phalkon/Software/Wine/Andaria/Bin/StartAll > /dev/null 2>&1 &
fi
done
