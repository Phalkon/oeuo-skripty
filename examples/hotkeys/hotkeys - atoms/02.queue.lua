 
queue = {}
queue.mt = {
     __index = queue,
}
 
 
function queue.New()
   local self = {}
   setmetatable(self,queue.mt)
   return self
end
 
function queue:push(s)
   table.insert(self, 1, s)
end
 
function queue:dequeue()
   local s
   if #self > 0 then
        s = self[#self]
        self[#self] = nil
   end
   return s
end

     
local Commands = queue.New()
     
while not getkey('F12') do
  command = getatom('CMD')
  if command ~= 'wait' then
      setatom('CMD','wait')
      Commands:push(command)
      command = 'wait'
  else wait(1) end
  if getatom('Request') == 'true' then
      command = Commands:dequeue()
      if command then setatom('Request',command) else setatom('Request','nil') end
      command = 'wait'
  end
end
     