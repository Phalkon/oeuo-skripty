    -- hotkeys_wait is being delcared here so it gets
    -- file "scope" instead of global scope.
     
    local hotkeys_wait
    local hotkeys -- function that checks key presses
    local fn1
    local fn2
    local fn3
     
    fn1 = function() -- first hotkey function
            print("FN1 start")
            hotkeys_wait(5000)
            print("FN1 end")
    end
     
    fn2 = function() -- second hotkey function
            print("FN2 start")
            hotkeys_wait(5000)
            print("FN2 end")
    end
     
    fn3 = function() -- etc...
            print("FN3 start")
            hotkeys_wait(5000)
            print("FN3 end")
    end
     
    -- the timeout is just to keep it from "catching" hotkeys too
    -- fast IE: the TimeOUT before another hotkey will be checked
    -- is to give the human time to raise the finger
     
    do
            local TimeOUT = 0
            local Timing = 100
            hotkeys = function()
                    if getticks() < TimeOUT then
                            return
                    end
                    if getkey(1) then
                            TimeOUT = getticks() + Timing
                            fn1()
                    end
                    if getkey(2) then
                            TimeOUT = getticks() + Timing
                            fn2()
                    end
                    if getkey(3) then
                            TimeOUT = getticks() + Timing
                            fn3()
                    end
            end
    end
     
    hotkeys_wait = function(t)
            t = getticks() + t
            repeat
                    wait(1)
                    hotkeys()
            until getticks() >= t
    end
     
    repeat
            print("KEYLOOP")
            hotkeys_wait(1000)
    until false
     
    stop()