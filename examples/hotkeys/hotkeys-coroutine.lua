    local cr_wait -- use coroutine.yield() instead of wait()
    local cr_handler -- handles coroutine.resume() calls for the threads
    local cr_list = {} -- list of coroutine threads used by cr_handler()
     
    local checkkey -- creates a coroutine thread when a key is pressed
    local check_hotkeys -- function to check all hotkeys using checkkey()
    local cr_fn1 -- co-routine based hotkey function (uses cr_wait() instead of wait())
    local cr_fn2 -- etc
    local cr_fn3 -- etc
     
    -- hot keys are checked in the cr_handler() and
    -- in the main loop
     
    check_hotkeys = function()
            checkkey(1,cr_fn1)
            checkkey(2,cr_fn2)
            checkkey(3,cr_fn3)
    end
     
    cr_fn1 = function() -- first hotkey function
            local t = getticks()
            print("FN1 start")
            cr_wait(5000)
            t = getticks() - t
            print("FN1 end = "..t)
    end
     
    cr_fn2 = function() -- second hotkey function
            local t = getticks()
            print("FN2 start")
            cr_wait(5000)
            t = getticks() - t
            print("FN2 end = "..t)
    end
     
    cr_fn3 = function() -- etc...
            local t = getticks()
            print("FN3 start")
            cr_wait(5000)
            t = getticks() - t
            print("FN3 end = "..t)
    end
     
    -- Cheffe's checkkey() modified to use co-routines
    -- instead of directly calling each function.
     
    do
            local old = {}
            checkkey = function(key,func)
                    local b = getkey(key)
                    if b ~= old[key] then
                            old[key] = b
                            if b then
                                    table.insert(cr_list,coroutine.create(func))
                            end
                    end
            end
    end
     
    cr_wait = function(t)
            t = getticks() + t
            repeat
                    coroutine.yield()
            until getticks() >= t
    end
     
    cr_handler = function()
            for i=#cr_list,1,-1 do
                    if(not coroutine.resume(cr_list[i])) then
                            table.remove(cr_list,i)
                    end
                    check_hotkeys()
            end
    end
     
    repeat
            wait(1)
            check_hotkeys() -- function for checking hotkeys using checkkey()
            cr_handler() -- handler for coroutines created by checkkey()
    until false
     
    stop()