setatom('Escape',false)
local TileInit = UO.TileInit()

local required = {
['FindItem']		= dofile(getinstalldir()..'/functions/FindItems.lua'),
['FluentUO']		= dofile(getinstalldir()..'/functions/FluentUO.lua'),
['journal']		= dofile(getinstalldir()..'/functions/journal.lua'),
}

local minimal_pause=500

local sekyrka = ScanItems(true,{Type={3913,3914},ContID=UO.CharID})
if sekyrka[1]==nil then UO.SysMessage('Sekyrku do ruky',0) setatom('Escape','true') end

local strom=nil
local vykaceno={}

local konik=ScanItems(true,{Type=291,Dist=3})
local konik_bag=World().InContainer(konik[1].ID).WithType(3701).Items

local use_object = function(UObjectID) UO.LObjectID = UObjectID UO.Macro(17,0) end
local WaitForTarCurs = function(for_how_long) if for_how_long == nil then for_how_long = 500 end local time_to_run = getticks() + for_how_long repeat wait (100) until UO.TargCurs==true or time_to_run < getticks() end
local dragndrop = function(id,stack,drop) UO.Drag(id,stack) time_to_run=getticks()+200 repeat wait(1) wait(minimal_pause-1) until UO.LLiftedID==id or time_to_run<getticks() UO.DropC(drop) end


local vykaceno_check = function (x,y) if vykaceno then for i=1,#vykaceno do if x==vykaceno[i][1] and y==vykaceno[i][2] then return false end end end return true end
local polena       = function ()
                      if UO.ContID~=konik_bag[1].ID then use_object(konik_bag[1].ID) repeat wait(500) until UO.ContID==konik_bag[1].ID end
                      local drivi = ScanItems(true,{Type=7133,ContID=UO.BackpackID})
                      if drivi[1]~=nil then dragndrop(drivi[1].ID,drivi[1].Stack,konik_bag[1].ID) end
                     end
local driky        = function ()
                      if UO.ContID~=konik_bag[1].ID then use_object(konik_bag[1].ID) repeat wait(500) until UO.ContID==konik_bag[1].ID end
                      local drivi = ScanItems(true,{Type=7133,ContID=UO.BackpackID})
                      if drivi[1]~=nil then dragndrop(drivi[1].ID,drivi[1].Stack,konik_bag[1].ID) end
                     end


local hledej_strom = function (dist)
                      for i1=-dist,dist do for i2=-dist,dist do local x=UO.CharPosX+i1 local y=UO.CharPosY+i2
                       for v=1,4 do local type,z,name,flags=UO.TileGet(x,y,v)
                        if name:find('tree') then if vykaceno_check(x,y) then
                         strom={['type']=type,['x']=UO.CharPosX+i1,['y']=UO.CharPosY+i2,['z']=z} return true end --end end
                       end end
                      end end return false
                     end
local rubej        = function ()
                      UO.LTargetTile=strom['type'] UO.LTargetX=strom['x'] UO.LTargetY=strom['y'] UO.LTargetZ=strom['z'] UO.LTargetKind=3
                      UO.Move(strom['x'],strom['y'],1)
                      use_object(sekyrka[1].ID)
                      WaitForTarCurs(3000)
                      UO.Macro(22,0)
                      j = journal:new()
                      local time_to_run = getticks() + 8000
                      repeat wait(50)
                       if UO.SysMsg=='Nezbylo tu nic k pok�cen�.' then table.insert(vykaceno,1,{strom['x'],strom['y']}) strom=nil return end
                       if UO.SysMsg=='Pokl�d� polena do ba�ohu.' then polena() return end
                      until j:next()=='|946|Lilien: Chvilku sek� do stromu, ale ��dn� pou�iteln� d�evo nez�sk�v�|' or time_to_run < getticks()
                     end


while not getatom('Escape') do wait(50)
 for i=1,10 do if hledej_strom(i) then break end end
  rubej()
end

